package Ti.app.porsche;

import Ti.DataProcessing.excel.porsche.PorscheExcelProcess1;
import Ti.DataProcessing.excel.porsche.PorscheExcelProcess2;
import Ti.model.porsche.PorscheSpec;
import Ti.model.porsche.PorscheSpecInvoice;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class PorscheFrame extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(PorscheFrame.class);

    private JPanel panel;
    private JLabel xls1;
    private JLabel xls2;
    private JLabel emptyLabel;
    private JButton xls1FolderOpenButton;
    private JButton xls2OpenButton;
    private JButton processButton;
    private List list;

    private FileNameExtensionFilter filter;
    private JFileChooser dirChooser;

    PorscheSpecInvoice porscheSpecInvoice = null;
    ArrayList<PorscheSpec> porscheSpecs;

    public PorscheFrame() throws HeadlessException {

        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        xls1 = new JLabel("");
        xls2 = new JLabel("");
        emptyLabel = new JLabel("");
        xls1FolderOpenButton = new JButton("Открыть папку с XLS первого типа");
        xls2OpenButton = new JButton("Открыть XLS файл второго типа");
        processButton = new JButton("Обработать");

        list = new List(20);
        dirChooser = new JFileChooser();

        xls1FolderOpenButton.addActionListener(this);
        xls2OpenButton.addActionListener(this);
        processButton.addActionListener(this);
        processButton.setEnabled(false);
        list.setVisible(true);

        panel.add(xls1);
        panel.add(xls1FolderOpenButton);
        panel.add(xls2);
        panel.add(xls2OpenButton);
        panel.add(emptyLabel);
        panel.add(processButton);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Porsche manifest app");
        addWindowListener(this);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        //---------------Открываем папку с файлами XLS первого типа
        if (e.getSource().equals(xls1FolderOpenButton)) {
            dirChooser.removeChoosableFileFilter(filter);
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                list.removeAll();
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                for (String S : Flist) {
                    if (FilenameUtils.getExtension(S).toLowerCase().equals("xls") || FilenameUtils.getExtension(S).toLowerCase().equals("xlsx")) {
                        list.add(S);
                    }
                }
                if (list.getItems().length > 0) {
                    processButton.setEnabled(true);
                } else
                    processButton.setEnabled(false);

                xls1.setText(dirChooser.getSelectedFile().getPath());
            }
        }
        //---------------Открываем файл XLS второго типа
        if (e.getSource().equals(xls2OpenButton)) {
            HashMap<String, String> checks = new HashMap<>();
            checks.put("A1", "INVOICE- SPECIFICATION");
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                porscheSpecInvoice = null;
                String file = dirChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                if (ext.toLowerCase().equals("xls") || ext.toLowerCase().equals("xlsx")) {
                    PorscheExcelProcess2 process2 = new PorscheExcelProcess2();
                    try {
                        if (process2.init(new FileInputStream(new File(file)), checks)) {
                            ArrayList<PorscheSpecInvoice> specInvoices = (ArrayList<PorscheSpecInvoice>) process2.processSheet();
                            if (specInvoices != null && specInvoices.size() > 0) {
                                porscheSpecInvoice = specInvoices.get(0);
                                if (porscheSpecInvoice != null) {
                                    xls2.setText(porscheSpecInvoice.getInvSpec());
                                }

                                if (list.getItems().length > 0 && porscheSpecInvoice != null) {
                                    processButton.setEnabled(true);
                                } else
                                    processButton.setEnabled(false);
                            } else {
                                JOptionPane.showMessageDialog(null, "Не найдено записей", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                                xls2.setText("");
                            }
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, process2.getErrors(), "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                            xls2.setText("");
                        }
                    } catch (FileNotFoundException e1) {
                        JOptionPane.showMessageDialog(null, process2.getErrors(), "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                        xls2.setText("");
                    }


                } else {
                    JOptionPane.showMessageDialog(null, "Выбран не Excel файл", "Информация", JOptionPane.INFORMATION_MESSAGE);
                    xls2.setText("");
                    processButton.setEnabled(false);
                }
            }
        }

        // Обрабатываем файлы первого типа и генерируем выходной файл
        if (e.getSource().equals(processButton)) {
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);

            HashMap<String, String> checks = new HashMap<>();
            checks.put("A1", "SPECIFICATION");
            PorscheExcelProcess1 process1 = new PorscheExcelProcess1();
            porscheSpecs = new ArrayList<>();
            try {
                for (String fname : list.getItems()) {

                    String fullName = xls1.getText() + File.separator + fname;

                    if (process1.init(new FileInputStream(new File(fullName)), checks)) {
                        porscheSpecs.addAll((Collection<? extends PorscheSpec>) process1.processSheet());
                        list.remove(fname);
                    } else {
                        JOptionPane.showMessageDialog(null, process1.getErrors(), "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                        xls1.setText("");
                        list.removeAll();
                        processButton.setEnabled(false);
                    }
                }
                Workbook wb = process1.generateXls(porscheSpecs, porscheSpecInvoice);
                if (dirChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {

                    String fileName = FilenameUtils.removeExtension(dirChooser.getSelectedFile().getName());
                    OutputStream outputStream = new FileOutputStream(new File(Paths.get(dirChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls"));
                    wb.write(outputStream);
                    outputStream.close();
                    xls1.setText("");
                    xls2.setText("");
                    porscheSpecInvoice=null;
                }
            } catch (FileNotFoundException e1) {
                JOptionPane.showMessageDialog(null, process1.getErrors(), "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }
}
