package Ti.DataProcessing.excel;

import Ti.model.Volvo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class VolvoExcelProcess extends ImportXLS {
    @Override
    public ArrayList<?> processSheet() {
        ArrayList<Volvo> volvos= new ArrayList<>();
        for(int i=2;i<=getDatatypeSheet().getLastRowNum();i++)
        {
            String tr= safeGetCell(i,1).getStringCellValue();
            String vin= safeGetCell(i,9).getStringCellValue();
            if (!vin.isEmpty()) {
                volvos.add(new Volvo(tr, vin));
            }
        }
        return volvos;
    }
    public void copyAndRename(String fname,String path,String cont) throws IOException {
        Files.copy(Paths.get(path + File.separator + fname),
                new FileOutputStream(new File(path+ File.separator+"out"+File.separator+cont+"-"+fname)));
    }
}
