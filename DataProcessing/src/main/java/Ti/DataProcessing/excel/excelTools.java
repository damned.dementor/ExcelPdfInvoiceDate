package Ti.DataProcessing.excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;

public class excelTools {

    public static HSSFCellStyle createWbStyle(BorderStyle style, HSSFWorkbook wb)
    {
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(style);
        cellStyle.setBorderRight(style);
        cellStyle.setBorderTop(style);
        cellStyle.setBorderLeft(style);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        return cellStyle;
    }
    public static void createCellRange(CellRangeAddress rangeAddress, HSSFSheet sheet, int row, Short heghtInPoints, BorderStyle style)
    {
        sheet.addMergedRegion(rangeAddress);
        sheet.getRow(row).setHeightInPoints(heghtInPoints);
        setRegionStyle(style,rangeAddress,sheet);
    }
    public static void setRegionStyle(BorderStyle style,CellRangeAddress range,HSSFSheet sheet)
    {
        RegionUtil.setBorderBottom(style, range, sheet);
        RegionUtil.setBorderTop(style, range, sheet);
        RegionUtil.setBorderLeft(style, range, sheet);
        RegionUtil.setBorderRight(style, range, sheet);
    }

    public static Cell createCellWtype(Row row,int column,HSSFCellStyle cellStyle, int cellType)
    {
       Cell cell= row.createCell(column);
        cell.setCellStyle(cellStyle);
        cell.setCellType(cellType);

        return cell;
    }
    public static Cell createCell(Row row,int column, Object data,boolean numeric,HSSFCellStyle style)
    {
        Cell cell = row.createCell(column);
        if(numeric) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((double)data);
        }
        else {
            cell.setCellType(CellType.STRING);
            cell.setCellValue((String) data);
        }
        cell.setCellStyle(style);
        return cell;
    }
    public static void insertAfterIndex(Row  row, int idx,String val)
    {
        for (int curIdx=row.getLastCellNum()-1;curIdx>idx+1;curIdx--)
        {
            Cell cell2move=row.getCell(curIdx);
            Cell newCell= row.createCell(curIdx+1,cell2move.getCellTypeEnum());
            newCell.setCellStyle(cell2move.getCellStyle());
            switch(cell2move.getCellTypeEnum())
            {
                case STRING:
                    newCell.setCellValue(cell2move.getStringCellValue());break;
                case NUMERIC:
                    newCell.setCellValue(cell2move.getNumericCellValue());break;
                case FORMULA:
                    newCell.setCellValue(cell2move.getCellFormula());break;
                    default:
                    {
                        cell2move.setCellType(CellType.STRING);
                        newCell.setCellType(CellType.STRING);
                        newCell.setCellValue(cell2move.getStringCellValue());
                    }
            }
            Cell cell= row.getCell(idx+1);
            cell.setCellType(CellType.STRING);
            cell.setCellValue(val);
        }
    }

}
