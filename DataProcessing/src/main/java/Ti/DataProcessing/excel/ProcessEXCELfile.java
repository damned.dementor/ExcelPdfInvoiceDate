package Ti.DataProcessing.excel;

import Ti.model.DataModel;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ProcessEXCELfile {
    private static Logger LOGGER = Logger.getLogger(ProcessEXCELfile.class);

    private String filePath;
    private Workbook workbook;
    private Map<String, DataModel> stringdataModelMap;
    int dateCount=0;

    public Map<String, DataModel> getStringdataModelMap() {
        return stringdataModelMap;
    }

    public int getDateCount() {
        return dateCount;
    }

    public ProcessEXCELfile(String filePath) {
        this.filePath = filePath;
    }

    /**
     * method init openes excel file and read necessary columns into the stringdataModelMap.
     * @return result of an operation
     */
    public boolean init() {
        try {
            FileInputStream excelFile = new FileInputStream(new File(filePath));
            LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"File:" + filePath + " successfully opened!");
            stringdataModelMap = new HashMap<>();
            workbook = WorkbookFactory.create(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            excelFile.close();
            Iterator<Row> iterator;
            iterator = datatypeSheet.iterator();
            int invoiceCount=0;
            while (iterator.hasNext()) {
                iterator.hasNext();
                Row row = iterator.next();
                Cell cell =  row.getCell(1);
                cell.setCellType(CellType.STRING);
                if (cell!=null&&!cell.getStringCellValue().isEmpty()) {
                    invoiceCount++;
                    DataModel dataModel = new DataModel(row.getRowNum());
                    stringdataModelMap.put(cell.getStringCellValue(), dataModel);
                    LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"Invoice N:"+invoiceCount+"--"+row.getRowNum() + ":" + cell.getStringCellValue());
                }
            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Ошибка открытия Excel файла!");
            LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Error opening excel file");
            return false;
        } catch (InvalidFormatException e) {
            JOptionPane.showMessageDialog(null, "Ошибка формата Excel файла!");
            LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Format excel file error");
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ошибка ввода вывода");
            LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"i/o error");
            e.printStackTrace();
            return false;
        }
        if(stringdataModelMap.size()<1)
        {
            JOptionPane.showMessageDialog(null, "Не найдено ни одного номера invoice в Excel файле");
            LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"There is no invoice records in Excel file");
            return false;
        }

        return true;
    }

    /**
     * method processPDF reads invoice number amd date from input file stream and addes it into stringdataModelMap.
     * @param inputStream input pdf file sream
     * @return result
     * @throws IOException file read exception
     */
    public boolean processPDF(InputStream inputStream) throws IOException {
        SimpleDateFormat df= new SimpleDateFormat("dd.MM.yyyy");
        Rectangle2D rect = new Rectangle
                 (300, 720, 150, 30);
        RenderFilter filter = new RegionTextRenderFilter(rect);
        PdfReader reader = new PdfReader(inputStream);
        TextExtractionStrategy strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);
        String[] text = PdfTextExtractor.getTextFromPage(reader, 1, strategy).split(" ");
        if(text.length>1)
        {
            if(StringUtils.isNumeric(text[0])) {
                LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"Invoice N:" + text[0]);
                LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"Date:" + text[1]);
                DataModel dataModel = stringdataModelMap.get(text[0]);
                if (dataModel != null) {
                    dataModel.setDate(makeDate(text[1], df));
                    stringdataModelMap.put(text[0], dataModel);
                    dateCount++;
                }
            }
            else
            {
                LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Error parsing invoice number:"+text[0]);
                return false;
            }
        }
        return true;
    }

    /**
     * method saveEXCEL saves result of an adding dates from pdf files to stringdataModelMap to an excel file.
     * @return result
     */
    public boolean saveEXCEL()
    {
        Sheet sheet= workbook.getSheetAt(0);

        CellStyle cellStyle = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        cellStyle.setDataFormat(
                createHelper.createDataFormat().getFormat("dd/mm/yyyy"));

        if(workbook!=null&&!filePath.isEmpty())
        {
            for (String s:stringdataModelMap.keySet() )
            {
                DataModel dataModel=stringdataModelMap.get(s);
                if(dataModel.getDate()!=null)
                {
                    Row row= sheet.getRow(dataModel.getRow());
                    Cell cell = row.getCell(16);
                    if(cell==null)
                    {
                        cell=row.createCell(16);
                    }
                    cell.setCellStyle(cellStyle);
                    cell.setCellValue(dataModel.getDate());
                }
            }
            sheet.autoSizeColumn(16);
            FileOutputStream out;
            try {
          //      String outPath = Paths.get(filePath).getParent().toString();
          //      String outName=Paths.get(filePath).getFileName().toString().toUpperCase();
          //      if(outName.contains("EB"))
            //        outName=outName.substring(outName.indexOf("EB")+2,outName.length());
           //     outPath=outPath+File.separator+outName;
                //out = new FileOutputStream(outPath);
                out = new FileOutputStream(filePath);
                workbook.write(out);
                out.close();
                LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"Output file:"+
                        filePath.substring(0,filePath.indexOf("."))+"out"+filePath.substring(filePath.indexOf("."))+"successfully created");
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Error saving excel file");
                return false;
            }

        }
        else
        {
            LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Wrong input excel file");
            return false;
        }
        LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Wrong input excel file");
        return true;
    }

    //--------------------------------------------------------------
    private static Date makeDate(String str, SimpleDateFormat format) {
        LOGGER.debug(Thread.currentThread().getStackTrace()[1].getMethodName() +"Converting to Date from String - " + str);
        Date check = null;
        if (str==null||str.isEmpty())
            return null;

        str = str.trim();

            try {
                check = format.parse(str);
            } catch (Exception ignore) {
            }
        if (check == null)
        {
            LOGGER.warn(Thread.currentThread().getStackTrace()[1].getMethodName() +"Not parsable date " + str);
        }
        return check;
    }
}
