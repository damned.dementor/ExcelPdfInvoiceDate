package Ti.DataProcessing.excel.porsche;

import Ti.DataProcessing.excel.ImportXLS;
import Ti.model.porsche.PorscheSpecInvoice;
import Ti.model.porsche.PorscheSpecInvoiceRecord;
import Ti.model.porsche.PorscheSpecInvoiceRecordElement;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PorscheExcelProcess2 extends ImportXLS {
    @Override
    public ArrayList<?> processSheet() {
        ArrayList<PorscheSpecInvoice> porscheExcel2 = new ArrayList<>();
        String invSpec = safeGetCellString(0, 5);
        PorscheSpecInvoice porscheSpecInvoice = new PorscheSpecInvoice(invSpec, new ArrayList<>());
        for (int i = 7; i <= getDatatypeSheet().getLastRowNum(); i++) {
            if ((safeGetCellString(i, 0).length() == 0 && safeGetCellString(i, 1).length() > 0)||
                    (safeGetCellString(i, 0).length() == 0 && safeGetCellString(i, 1).length() == 0&&safeGetCellString(i, 4).length() == 0))
                break;
            String contNo = safeGetCellString(i, 1);
            ArrayList<PorscheSpecInvoiceRecordElement> elements = new ArrayList<>();
            i++;
            while ((i <= getDatatypeSheet().getLastRowNum()) &&(safeGetCellString(i, 1).length() == 0)&&(safeGetCellString(i, 4).length() != 0)) {
                String hsCode=safeGetCellString(i, 2);
                String description=safeGetCellString(i, 3);
                int qtyPcs=Integer.parseInt(safeGetCellString(i, 4));
                double grossWeight= Double.parseDouble(safeGetCellString(i, 5).replaceAll("\\.","").replaceAll(",","\\."));
                BigDecimal pricePcsEur=new BigDecimal(safeGetCellString(i, 6).replaceAll("\\.","").replaceAll(",","\\."));
                PorscheSpecInvoiceRecordElement element = new PorscheSpecInvoiceRecordElement(hsCode,description,qtyPcs,grossWeight,pricePcsEur);
                elements.add(element);
                i++;
            }
            PorscheSpecInvoiceRecord record= new PorscheSpecInvoiceRecord(contNo,elements);
            porscheSpecInvoice.getRecords().add(record);
            i--;
        }
        porscheExcel2.add(porscheSpecInvoice);
        return porscheExcel2;
    }
}
