package Ti.DataProcessing.excel.porsche;

import Ti.DataProcessing.excel.ImportXLS;
import Ti.DataProcessing.excel.excelTools;
import Ti.model.porsche.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;

import java.math.BigDecimal;
import java.util.ArrayList;

import static Ti.DataProcessing.excel.excelTools.createCellRange;

public class PorscheExcelProcess1 extends ImportXLS {
    @Override
    public ArrayList<?> processSheet() {
        ArrayList<PorscheSpec> porscheExcel1 = new ArrayList<>();

        String invoice1 = safeGetCellString(0, 6);
        String invoice2 = safeGetCellString(0, 7);
        String contNo = safeGetCellString(3, 2);
        String sealNo = safeGetCellString(4, 2);
        ArrayList<PorscheSpecRecord> records = new ArrayList<>();
        for (int i = 8; i <= getDatatypeSheet().getLastRowNum(); i++) {
            String no = safeGetCellString(i, 0);
            if (!StringUtils.isNumeric(no.replaceAll("\\.", "").replaceAll(",", "")))
                break;

            String hsCode = safeGetCellString(i, 1);
            String description = safeGetCell(i, 2).getStringCellValue();
            int qtyPacks = Integer.parseInt(safeGetCellString(i, 3));
            int qtyPcs = Integer.parseInt(safeGetCellString(i, 4));
            String netW = safeGetCellString(i, 5).replaceAll("\\.", "").replaceAll(",", "\\.");
            double netWeight = Double.parseDouble(netW);
            netW = safeGetCellString(i, 6).replaceAll("\\.", "").replaceAll(",", "\\.");
            double grossWeight = Double.parseDouble(netW);
            BigDecimal pricePcsCny = new BigDecimal(safeGetCellString(i, 7).replaceAll("\\.", "").replaceAll(",", "\\."));
            String vinNo = safeGetCellString(i, 8);

            PorscheSpecRecord record = new PorscheSpecRecord(hsCode, description, qtyPacks, qtyPcs, netWeight, grossWeight, pricePcsCny, vinNo);
            records.add(record);
        }
        porscheExcel1.add(new PorscheSpec(invoice1, invoice2, contNo, sealNo, records));
        return porscheExcel1;
    }

    public Workbook generateXls(ArrayList<PorscheSpec> porscheSpecs, PorscheSpecInvoice porscheSpecInvoice) {
        int current_row = 0;
        Cell cell;

        String[] tableHeader = {"No.", "Container NO.", "HS Code", "description of goods", "Qty of packages", "Qty of pcs", "Net weight(kg)", "Gross weight(kg)", "Price/pcs CNY", "VIN No.",
                "INVOICE No.", "SEAL No.", "HS Code", "description of goods", "Qty of pcs", "Gross weight(kg)", "Price/pcs EUR", "INVOICE No."};
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        BorderStyle thinStyle = BorderStyle.THIN;
        BorderStyle mediumStyle = BorderStyle.MEDIUM;

        Font font12Calibri = wb.createFont();
        font12Calibri.setFontName("Calibri");
        font12Calibri.setFontHeightInPoints((short) 12);

        Font font10Calibri = wb.createFont();
        font10Calibri.setFontName("Calibri");
        font10Calibri.setFontHeightInPoints((short) 10);

        HSSFCellStyle calibriMedium12 = excelTools.createWbStyle(mediumStyle, wb);
        calibriMedium12.setFont(font12Calibri);

        HSSFCellStyle calibriThin10 = excelTools.createWbStyle(thinStyle, wb);
        calibriThin10.setFont(font10Calibri);

        HSSFCellStyle calibriThin10ButtonBorder = excelTools.createWbStyle(thinStyle, wb);
        calibriThin10ButtonBorder.setFont(font10Calibri);
        calibriThin10ButtonBorder.setBorderBottom(mediumStyle);

        HSSFCellStyle calibriThin10Red= excelTools.createWbStyle(thinStyle, wb);
        calibriThin10Red.setFont(font10Calibri);
        calibriThin10Red.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        calibriThin10Red.setFillForegroundColor(HSSFColor.RED.index);

        HSSFCellStyle calibriThin10RedButtonBorder= excelTools.createWbStyle(thinStyle, wb);
        calibriThin10RedButtonBorder.setFont(font10Calibri);
        calibriThin10RedButtonBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        calibriThin10RedButtonBorder.setFillForegroundColor(HSSFColor.RED.index);
        calibriThin10RedButtonBorder.setBorderBottom(mediumStyle);

        Row row = sheet.createRow(current_row);

        current_row++;

        for (int i = 0; i < tableHeader.length; i++) {
            CellUtil.createCell(row, i, tableHeader[i], calibriMedium12);
        }

        if (porscheSpecs != null) {
            int num = 1;
            for (PorscheSpec porscheSpec : porscheSpecs) {
                // пишем левую часть
                int cell_beg=current_row;
                boolean invoice1 = false, invoice2 = false,sealN=false,numUsed=false,paintRed=false;

                PorscheSpecRecord checkRed=porscheSpec.getRecords().get(0);
                for (PorscheSpecRecord porscheSpecRecord : porscheSpec.getRecords()) {
                    if(!checkRed.getHsCode().toLowerCase().equals(porscheSpecRecord.getHsCode().toLowerCase())) {
                        paintRed = true;
                        break;
                    }
                }

                for (PorscheSpecRecord porscheSpecRecord : porscheSpec.getRecords()) {
                    row = sheet.createRow(current_row);
                    current_row++;
                    if(!numUsed) {
                        CellUtil.createCell(row, 0, Integer.toString(num), calibriThin10);
                        numUsed=true;
                    }
                    CellUtil.createCell(row, 1, porscheSpec.getContNo(), calibriThin10);
                    CellUtil.createCell(row, 2, porscheSpecRecord.getHsCode(), paintRed?calibriThin10Red:calibriThin10);
                    CellUtil.createCell(row, 3, porscheSpecRecord.getDescription(), calibriThin10);

                    cell=excelTools.createCellWtype(row,4,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(porscheSpecRecord.getQtyPacks());
//                    CellUtil.createCell(row, 4, Integer.toString(porscheSpecRecord.getQtyPacks()), calibriThin10);

                    cell=excelTools.createCellWtype(row,5,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(porscheSpecRecord.getQtyPcs());
//                    CellUtil.createCell(row, 5, Integer.toString(porscheSpecRecord.getQtyPcs()), calibriThin10);

                    cell=excelTools.createCellWtype(row,6,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(porscheSpecRecord.getNetWeight());
//                    CellUtil.createCell (row, 6,porscheSpecRecord.getNetWeight(), calibriThin10);
                    cell=excelTools.createCellWtype(row,7,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                    cell.setCellValue(porscheSpecRecord.getGrossWeight());
//                    CellUtil.createCell(row, 7, Double.toString(porscheSpecRecord.getGrossWeight()), calibriThin10);
                    CellUtil.createCell(row, 8, porscheSpecRecord.getPricePcsCny().toString(), calibriThin10);
                    CellUtil.createCell(row, 9, porscheSpecRecord.getVinNo(), calibriThin10);

                    if (!invoice1) {
                        CellUtil.createCell(row, 10, porscheSpec.getInvoice1(), calibriThin10);
                        invoice1 = true;
                    } else {
                        if (!invoice2) {
                            CellUtil.createCell(row, 10, porscheSpec.getInvoice2(), calibriThin10);
                            invoice2 = true;
                        }
                        else
                        {
                            CellUtil.createCell(row, 10, "", calibriThin10);
                        }
                    }
                    if(!sealN) {
                        CellUtil.createCell(row, 11, porscheSpec.getSealNo(), calibriThin10);
                        sealN=true;
                    }
                    else
                        CellUtil.createCell(row, 11, "", calibriThin10);
                }
                // пишем правую часть
                if(porscheSpecInvoice!=null)
                {
                    boolean invoice=false;
                    PorscheSpecInvoiceRecord record2cont=null;
                    for (PorscheSpecInvoiceRecord record:porscheSpecInvoice.getRecords()) {
                        if(porscheSpec.getContNo().toLowerCase().equals(record.getContNo().toLowerCase()))
                        {
                            record2cont=record;
                            break;
                        }
                    }

                    if(record2cont!=null)
                    for (int i=0;i<record2cont.getElements().size();i++) {
                        if(sheet.getRow(cell_beg+i)==null) {
                            sheet.createRow(current_row++);
                            row=sheet.getRow(current_row-1);
                            CellUtil.createCell(row, 1, porscheSpec.getContNo(), calibriThin10);
                            for(int n=2;n<12;n++)
                            {
                                CellUtil.createCell(row, n, "", calibriThin10);
                            }
                        }

                        row=sheet.getRow(cell_beg+i);
                        PorscheSpecInvoiceRecordElement specInvoiceRecordElement =  record2cont.getElements().get(i);
                        CellUtil.createCell(row, 12, specInvoiceRecordElement.getHsCode(), calibriThin10);
                        CellUtil.createCell(row, 13, specInvoiceRecordElement.getDescription(), calibriThin10);
                        cell=excelTools.createCellWtype(row,14,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(specInvoiceRecordElement.getQtyPcs());
//                        CellUtil.createCell(row, 14, Integer.toString(specInvoiceRecordElement.getQtyPcs()), calibriThin10);
                        cell=excelTools.createCellWtype(row,15,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(specInvoiceRecordElement.getGrossWeight());
//                        CellUtil.createCell(row, 15, Double.toString(specInvoiceRecordElement.getGrossWeight()), calibriThin10);
                        cell=excelTools.createCellWtype(row,16,calibriThin10,Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(specInvoiceRecordElement.getPricePcsEur().doubleValue());
//                        CellUtil.createCell(row, 16, specInvoiceRecordElement.getPricePcsEur().toString(), calibriThin10);
                        if (!invoice) {
                            CellUtil.createCell(row, 17, porscheSpecInvoice.getInvSpec(), calibriThin10);
                            invoice=true;
                        }
                        else
                            CellUtil.createCell(row, 17, "", calibriThin10);
                    }
                }
                else
                {
                    for(int rw=cell_beg;rw<current_row;rw++)
                    for (int i=12;i<18;i++)
                    {
                        row=sheet.getRow(rw);
                        CellUtil.createCell(row, i, "", calibriThin10);
                    }
                }


                // объяединяем ячейки и пишем номер
                CellRangeAddress rangeAddress = new CellRangeAddress(cell_beg,current_row-1,0,0);
                createCellRange(rangeAddress,sheet,1,(short)30,thinStyle);
                num++;

                // подчеркиваем жирным запись
                for(int i=0;i<tableHeader.length;i++)
                {
                    row= sheet.getRow(current_row-1);
                    cell=row.getCell(i);

                    if(cell.getCellStyle().equals(calibriThin10Red))
                        cell.setCellStyle(calibriThin10RedButtonBorder);
                    else
                        cell.setCellStyle(calibriThin10ButtonBorder);

                }
            }
        }
        for (int i = 0; i < tableHeader.length; i++) {
            sheet.autoSizeColumn(i);
        }

        return wb;
    }
}
