package Ti.DataProcessing.excel.ura;

import Ti.DataProcessing.Utils;
import Ti.DataProcessing.excel.ImportXLS;
import Ti.DataProcessing.excel.excelTools;
import Ti.model.ura.ClientWorks;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellUtil;

import java.text.DecimalFormat;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class UraExcel extends ImportXLS {
    @Override
    public ArrayList<?> processSheet() {

        ArrayList<ClientWorks> clients = new ArrayList<>();

        Sheet sheet = getDatatypeSheet();
        for (int i = 0; i <= getDatatypeSheet().getLastRowNum(); i++) {
            ClientWorks clientWorks = new ClientWorks();

            String client = safeGetCellString(i, 0);
            if (!client.trim().isEmpty()) {
                clientWorks.setClientName(client);
                Row row = sheet.getRow(i);

                for (int n = 1; n < row.getPhysicalNumberOfCells(); n++) {
                    String work = safeGetCellString(i, n);
                    if (!work.trim().isEmpty())
                        clientWorks.getWorks().add(work);
                }
                clients.add(clientWorks);
            }
        }

        return clients;

    }

        public Workbook generateXls(ArrayList<ClientWorks> clientWorks,int year,String additional_text,String additional_text_sub) {

        HSSFWorkbook wb = new HSSFWorkbook();
        BorderStyle thinStyle = BorderStyle.THIN;
        DecimalFormat decimalFormat = new DecimalFormat("00");

        Font font12Calibri = wb.createFont();
        font12Calibri.setFontName("Calibri");
        font12Calibri.setFontHeightInPoints((short) 12);

        HSSFCellStyle calibriMedium12 = excelTools.createWbStyle(thinStyle, wb);
        calibriMedium12.setFont(font12Calibri);
        calibriMedium12.setWrapText(true);

        String[] tableHeader = {"Дата", "Клиент", "Работы"};

       // int year = Year.now().getValue();
        Calendar calendar = new GregorianCalendar();


        for (int month = 0; month < 12; month++) {
            String monthName = Month.of(month + 1).getDisplayName(TextStyle.FULL_STANDALONE, new Locale("ru"));
            HSSFSheet sheet = wb.createSheet(monthName);
            Row row = sheet.createRow(1);
            CellUtil.createCell(row, 1, "Отчет о проделанной работе за " + monthName.toLowerCase()+","+year+" "+additional_text);

            int actionsMonthQuantity = Utils.generateRandomIntIntRange(10, 20);
            ArrayList<Integer> daysOfWork = new ArrayList<>();
            ThreadLocalRandom.current().ints(1, calendar.getActualMaximum(Calendar.DAY_OF_MONTH)).distinct().limit(actionsMonthQuantity).forEach(daysOfWork::add);
            daysOfWork.sort(Integer::compareTo);

            int current_row = 2;
            row = sheet.createRow(current_row);

            for (int i = 0; i < tableHeader.length; i++) {
                CellUtil.createCell(row, i, tableHeader[i], calibriMedium12);
            }

            calendar.set(year, month, 1);

            for (Integer day : daysOfWork) {

                StringBuilder date = new StringBuilder();
                date.append(decimalFormat.format(day + 1)).append(".")
                        .append(decimalFormat.format(month + 1)).append(".")
                        .append(year);

                row = sheet.createRow(++current_row);
                CellUtil.createCell(row, 0, date.toString(), calibriMedium12);

                int clientNum=Utils.generateRandomIntIntRange(0, clientWorks.size()-1);
                String clientName=clientWorks.get(clientNum).getClientName();
                List<String> works=clientWorks.get(clientNum).getWorks();
                CellUtil.createCell(row, 1, clientWorks.get(clientNum).getClientName(), calibriMedium12);

                StringJoiner actionsDoneString = new StringJoiner("\r\n");
                ArrayList<Integer> actionsDone = new ArrayList<>();
                ThreadLocalRandom.current().ints(0, works.size()).distinct().limit(Utils.generateRandomIntIntRange(1, Math.min(works.size(), 3)))
                        .forEach(actionsDone::add);
                actionsDone.sort(Integer::compareTo);
                for (Integer i : actionsDone) {
                    actionsDoneString.add(works.get(i));
                }
                CellUtil.createCell(row, 2, actionsDoneString.toString(), calibriMedium12);
            }
            if(!additional_text_sub.isEmpty())
            {
                row = sheet.createRow(current_row);
                CellUtil.createCell(row, 1, additional_text_sub, calibriMedium12);
            }

            for (int i = 0; i < tableHeader.length; i++) {
                sheet.autoSizeColumn(i);
            }
        }
        return wb;
    }
}
