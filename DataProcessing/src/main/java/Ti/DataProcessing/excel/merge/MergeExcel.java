package Ti.DataProcessing.excel.merge;

import Ti.DataProcessing.excel.ImportXLS;
import Ti.model.ExcelCell;
import Ti.model.mergeApp.MergeAppReport;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MergeExcel extends ImportXLS {
    private CellStyle cs10ArialBoldBorderThinWrap;
    private List<MergeAppReport> excelMergeReport;

    public Sheet readSheet(FileInputStream xlsStream) throws IOException {
        Workbook workbook = new XSSFWorkbook(xlsStream);
        return workbook.getSheetAt(0);
    }

    private void initStyles(Workbook wb) {
        Font font10Arial = wb.createFont();
        font10Arial.setFontName("Arial");
        font10Arial.setFontHeightInPoints((short) 10);
        cs10ArialBoldBorderThinWrap = createWbStyle(BorderStyle.THIN, wb, true, null);
        cs10ArialBoldBorderThinWrap.setFont(font10Arial);
        cs10ArialBoldBorderThinWrap.setWrapText(true);
    }

    public Workbook mergeSheets(List<Sheet> sheets,List<String> fnames) throws Exception {
        excelMergeReport= new ArrayList<>();
//        URL resource = getClass().getClassLoader().getResource("out.xlsx");

        // Считываем файл шаблона выходного документа
        File dictFile = new File(Paths.get(".").toAbsolutePath().normalize().toString()+File.separator+"out.xlsx");

        int rowNum = 3;
        Workbook workbook = new XSSFWorkbook(new FileInputStream(dictFile));
        initStyles(workbook);
        Sheet sheetOut = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();
        for (int num=0;num<sheets.size();num++) {
            try {
                MergeAppReport report = new MergeAppReport();
                excelMergeReport.add(report);
                report.setFileName(fnames.get(num));

                Sheet sheet = sheets.get(num);
                String kontNum = "";
                setDatatypeSheet(sheet);
                for (int i = 1; i <= sheet.getLastRowNum(); i++) {
//                    если нет номера по порядку и МРН, то конец документа
                    String npp=safeGetCellString(i, 0);
                    String mrn = safeGetCellString(i, 3);
                    if(npp.isEmpty()&&mrn.isEmpty())
                        break;

//                    Если есть МРН тол создаем запись в выходном документе
                    if (mrn != null && !mrn.isEmpty()) {
                        Row rowOut = sheetOut.createRow(rowNum++);

                        // номер контейнера
                        Cell cell = rowOut.createCell(0);
                        cell.setCellStyle(cs10ArialBoldBorderThinWrap);
                        String knum = safeGetCellString(i, 1);
                        if (!knum.isEmpty())
                            kontNum = knum;
                        else if(kontNum.isEmpty())
                            report.getContNumReadErrors().add(new ExcelCell(String.valueOf(i+1),CellReference.convertNumToColString(1),""));

                        cell.setCellValue(kontNum);

                        // номер мрн
                        cell = rowOut.createCell(3);
                        cell.setCellStyle(cs10ArialBoldBorderThinWrap);
                        cell.setCellValue(mrn);

                        cell = rowOut.createCell(2);
                        cell.setCellStyle(cs10ArialBoldBorderThinWrap);
                        cell.setCellValue("");

                        // брутто
                        cell = rowOut.createCell(1, CellType.NUMERIC);
                        cell.setCellStyle(cs10ArialBoldBorderThinWrap);
                        String val = dataFormatter.formatCellValue(sheet.getRow(i).getCell(4));
                        if (val != null && !val.isEmpty())
                            if(val.contains(".")&&val.contains(","))
                                cell.setCellValue(new Double(val.replaceAll(" +","").replaceAll(" +","").replaceAll("\\.", "").replaceAll(",", ".")).toString());
                            else
                                cell.setCellValue(new Double(val.replaceAll(" +","").replaceAll(" +","").replaceAll(",", ".")).toString());

                        cell = rowOut.createCell(4);
                        cell.setCellStyle(cs10ArialBoldBorderThinWrap);
                        cell.setCellValue("");
                    }else{
                        report.getMrnReadErrors().add(new ExcelCell(String.valueOf(i+1),CellReference.convertNumToColString(3),""));
                    }
                }
            }catch (Exception e)
            {
                throw new Exception("File processing error :"+fnames.get(num)+"  "+e.getMessage());
            }
        }

        return workbook;
    }

    private CellStyle createWbStyle(BorderStyle style, Workbook wb, boolean wrap, Short BackgroundColor) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(style);
        cellStyle.setBorderRight(style);
        cellStyle.setBorderTop(style);
        cellStyle.setBorderLeft(style);
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setVerticalAlignment(VerticalAlignment.TOP);
        cellStyle.setWrapText(wrap);
        if (BackgroundColor != null) {
            cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }

        return cellStyle;
    }

    @Override
    public ArrayList<?> processSheet() {
        return null;
    }

    public List<MergeAppReport> getExcelMergeReport() {
        return excelMergeReport;
    }
}
