package Ti.DataProcessing.excel.phoenix;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static Ti.DataProcessing.excel.excelTools.insertAfterIndex;

public class PhoenixExcel {

    private Map<String,String> dict = new HashMap<>();
    private Workbook workbook;
    private int startRow;

    public boolean initDict(InputStream inputStream)
    {
        try {
            Workbook workbook = WorkbookFactory.create(inputStream);
            Sheet sheet = workbook.getSheetAt(0);
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row=sheet.getRow(i);

                String descr=row.getCell(0).getStringCellValue();
                String tDescr=row.getCell(1).getStringCellValue();
                System.out.println(descr+"------------"+tDescr);

                if(!descr.isEmpty())
                {
                    dict.putIfAbsent(descr, tDescr);
                }
                else {
                    System.out.println(i);
                    break;
                }
            }
        } catch (IOException | InvalidFormatException e) {
            return false;
        }
        return true;
    }

    public boolean readWb(InputStream inputStream)
    {
        try {
            workbook = WorkbookFactory.create(inputStream);

        } catch (IOException | InvalidFormatException e) {
            return false;
        }
        return true;
    }
    public void findStart(Sheet sheet)
    {
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            Row row=sheet.getRow(i);
            Row rowPrev=sheet.getRow(i-1);
            Cell cell = row.getCell(0);
            Cell cellPrev = rowPrev.getCell(0);
            if(cell!=null&&cellPrev!=null) {
                cell.setCellType(CellType.STRING);
                cellPrev.setCellType(CellType.STRING);
                if (StringUtils.isNumeric(cell.getStringCellValue()) && Integer.parseInt(cell.getStringCellValue()) == 1 &&
                        cellPrev.getStringCellValue().toLowerCase().equals("position")) {
                    insertAfterIndex(rowPrev, 2, "name");
                    startRow = i;
                    break;
                }
            }
        }
    }

    public Workbook processSheet()
    {
        for(int sheetNum=0;sheetNum<workbook.getNumberOfSheets();sheetNum++) {
            Sheet sheet = workbook.getSheetAt(sheetNum);
            findStart(sheet);

            for (int i = startRow; i < sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                if (row == null) {
                    row = sheet.getRow(i+2);
                    if(row!=null)
                    {
                        Cell cell= row.getCell(9);
                        Cell newCell= row.createCell(10,CellType.STRING);
                        newCell.setCellStyle(cell.getCellStyle());
                        newCell.setCellValue(cell.getStringCellValue());

                        row.removeCell(cell);
                        newCell= row.createCell(9,CellType.FORMULA);
                        newCell.setCellFormula("sum(J"+startRow+":J"+(i-1)+")");

                        row.removeCell(row.getCell(8));

                         cell= row.getCell(5);
                         newCell= row.createCell(6,CellType.STRING);
                        newCell.setCellStyle(cell.getCellStyle());
                        newCell.setCellValue(cell.getStringCellValue());

                        row.removeCell(cell);
                        newCell= row.createCell(5,CellType.FORMULA);
                        newCell.setCellFormula("sum(F"+startRow+":F"+(i-1)+")");
                        row.removeCell(row.getCell(4));

                    }
                    break;
                }

                Cell cell = row.getCell(0);
                cell.setCellType(CellType.STRING);
                System.out.println(cell.getStringCellValue());
                if (StringUtils.isNumeric(cell.getStringCellValue())) {
                    cell = row.getCell(2);
                    String descr = cell.getStringCellValue();
                    String name = dict.get(descr) != null ? dict.get(descr) : "";
                    insertAfterIndex(row, 2, name);
                } else
                    break;
            }

            sheet.autoSizeColumn(4);
        }

        return workbook;
    }


    public Map<String, String> getDict() {
        return dict;
    }

    public void setDict(Map<String, String> dict) {
        this.dict = dict;
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public int getStartRow() {
        return startRow;
    }
}
