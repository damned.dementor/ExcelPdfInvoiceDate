package Ti.DataProcessing.excel.stuffingMerge;

import Ti.DataProcessing.excel.ImportXLS;
import Ti.model.excel.stuffing.ItogoCurModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class MergeStuffingXLS extends ImportXLS {
    @Override
    public ArrayList<?> processSheet() {
        return null;
    }

    public ItogoCurModel processItogoXls(String xlsFile) throws Exception {

        ItogoCurModel model = new ItogoCurModel();
        String utinKey = "Specification for Container".toLowerCase();
        String utiKye2="Container/Контейнер".toLowerCase();
        FileInputStream stream = new FileInputStream(new File(xlsFile));
        Workbook workbook = WorkbookFactory.create(stream);
        Sheet sheet = workbook.getSheetAt(0);
        setDatatypeSheet(sheet);
        String contString = safeGetCell(1, 1).getStringCellValue().trim().toLowerCase();
        String contString2 = safeGetCell(2, 2).getStringCellValue().trim().toLowerCase();
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        Map<String,BigDecimal> curItogoMap = new TreeMap<>();

        if (contString.contains(utinKey)||contString2.contains(utiKye2)) {
            String utin = null;
            if(contString.contains(utinKey))
                utin= contString.replaceAll(utinKey, "").trim().replaceAll(" +","");
            if(contString2.contains(utiKye2))
                utin= contString2.replaceAll(utiKye2, "").trim().replaceAll(" +","");
            if(utin==null)
                throw new Exception("Не могу найти номер контейнера");

            model.setUtiN(utin);
            for (int i = 8; i <= sheet.getLastRowNum(); i++) {
                String nzgr = safeGetCellString(i, 2);
                if (!nzgr.isEmpty()) {

                    String itogo = String.valueOf(evaluator.evaluate(sheet.getRow(i).getCell(CellReference.convertColStringToIndex("J"))).getNumberValue());
                    if (!StringUtils.isNumeric(itogo.replaceAll(",", "").replaceAll("\\.", "")))
                        throw new Exception("Неверная сумма итого:" + itogo);

                    BigDecimal itogoDec= new BigDecimal(itogo);

                    String cur = safeGetCellString(i,CellReference.convertColStringToIndex("K")).toLowerCase();
                    if (cur.trim().isEmpty())
                        throw new Exception("Пустое значение валюты:" + cur);

                    BigDecimal itigoSum=curItogoMap.get(cur);
                    if(itigoSum!=null)
                    {
                        itigoSum=itigoSum.add(itogoDec);
                        curItogoMap.put(cur,itigoSum);
                    }
                    else
                    {
                        curItogoMap.put(cur,itogoDec);
                    }
                }
                else
                {

                    model.setCur(String.join("/",curItogoMap.keySet()));
                    model.setItogo(curItogoMap.values().stream().map(BigDecimal::toPlainString).collect(Collectors.joining("/")));

                    return model;
                }
            }
        } else {
            throw new Exception("Не верный файл. Не могу прочитать Валюту и сумму");
        }


        throw new Exception("Не могу найти номер контейнера");
    }

    public List<String> processStuffingFile(String xlsFile, Map<String,ItogoCurModel> itogoCurModelMap) throws IOException, InvalidFormatException {
        FileInputStream stream = new FileInputStream(new File(xlsFile));
        Workbook workbook = WorkbookFactory.create(stream);
        stream.close();
        Sheet sheet = workbook.getSheetAt(0);
        setDatatypeSheet(sheet);
        List<String> missingUtins = new ArrayList<>();

        int currentRow = 2;

        for (int i = currentRow; i <=sheet.getLastRowNum(); i++) {
            String numVal = safeGetCellString(i,0);
            if(StringUtils.isNumeric(numVal))
            {
                String utin = safeGetCellString(i,2).trim().toLowerCase().replaceAll(" +","");
                if(itogoCurModelMap.get(utin)!=null)
                {
                    ItogoCurModel model=itogoCurModelMap.get(utin);
                    model.setUsed(true);
                    Row row = sheet.getRow(i);
                    Cell cell = row.createCell(CellReference.convertColStringToIndex("P"));
                    cell.setCellType(CellType.STRING);
                    cell.setCellValue(itogoCurModelMap.get(utin).getItogo());

                    cell = row.createCell(CellReference.convertColStringToIndex("Q"));
                    cell.setCellType(CellType.STRING);
                    cell.setCellValue(itogoCurModelMap.get(utin).getCur());
                }
                else{
                    missingUtins.add(utin);
                }
            }
        }
        FileOutputStream  outputStream = new FileOutputStream(xlsFile);
        workbook.write(outputStream);
        outputStream.close();
        return missingUtins;
    }
}
