package Ti.DataProcessing.pdf.custom.mrn;

import Ti.DataProcessing.excel.excelTools;
import Ti.DataProcessing.pdf.AbstractProcessPDFfile;
import Ti.model.mrn.CimSmgsInvoiceGruz;
import Ti.model.mrn.CustomMrnModel;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellUtil;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class ProcessCustomMrn extends AbstractProcessPDFfile {
    private CustomMrnModel invoiceModel;
    private int currentPosition;
    private int currentPage=1;

//    private boolean multiPagePosition = false;

    private String docType;
    private static Logger LOGGER = Logger.getLogger(ProcessCustomMrn.class);
    // A4 595x842; 2.83
    private final Rectangle2D bruttoRectType1 = new Rectangle(141, 370, 90, 20);
    private final Rectangle2D bruttoRectType2 = new Rectangle(438, 571, 90, 20);
    private final Rectangle2D bruttoRectType3 = new Rectangle(438, 560, 90, 10);
    private final Rectangle2D bruttoRectType4 = new Rectangle(438, 537, 90, 10);
    private final Rectangle2D[] bruttoRecs = {bruttoRectType1, bruttoRectType2,bruttoRectType3,bruttoRectType4};

    private final Rectangle2D positionsRectType1 = new Rectangle(297, 620, 40, 20);
    private final Rectangle2D positionsRectType2 = new Rectangle(303, 720, 70, 40);
    private final Rectangle2D positionsRectType3 = new Rectangle(300, 720, 40, 40);
    private final Rectangle2D positionsRectType4 = new Rectangle(300, 700, 40, 20);
    private final Rectangle2D[] pisotionRects = {positionsRectType1, positionsRectType2,positionsRectType3,positionsRectType4};

    private final Rectangle2D pageRect = new Rectangle(28, 28, 560, 820);
    private final Rectangle2D pageRectType2FirstPage = new Rectangle(28, 161, 560, 367);

    private final Rectangle2D pageRectType4 = new Rectangle(21, 28, 567, 820);
    private final Rectangle2D pageRectType2FirstPageType4  = new Rectangle(21, 161, 567, 367);


    /**
     * Считываем брутто всех позиций в файле
     *
     * @return вес брутто всех позиций
     * @throws IOException
     */
    public BigDecimal readBrutto() throws Exception {
        LOGGER.info("readBruttoString");

        for (int i = 0; i < bruttoRecs.length; i++) {
            try {
                String bruttoStr = getTextFromPageRect(bruttoRecs[i], 1).toLowerCase().trim().replaceAll(",", ".");
                LOGGER.info(bruttoStr);
                getInvoiceModel().setGrossWeight(new BigDecimal(bruttoStr));
                docType = "type" + (i + 1);
                LOGGER.info("Doc type:" + docType);
                LOGGER.info(getInvoiceModel().getGrossWeight());
                break;

            } catch (Exception ignored) {
            }
        }
        if (getInvoiceModel().getGrossWeight() == null)
            throw new Exception("Cant find brutto");

        return getInvoiceModel().getGrossWeight();
    }

    /**
     * Считываем количнство позиций из файла
     *
     * @return количество позиций
     * @throws IOException
     */
    public Integer readPositionsCount() throws Exception {
        LOGGER.info("readPositionsString");
        int i = Integer.parseInt(docType.replaceAll("type", "")) - 1;

        switch (i) {
            case 0: {
                String positionsStr = getTextFromPageRect(pisotionRects[i], 1).toLowerCase().trim().replaceAll(",", ".");
                LOGGER.info(positionsStr);
                positionsStr = positionsStr.split(" ")[0].split("\n")[0];
                LOGGER.info(positionsStr);
                getInvoiceModel().setPositionsCount(Integer.parseInt(positionsStr));
            }
            break;
            case 1: {
                String positionsStr = getTextFromPageRect(pisotionRects[i], 1).toLowerCase().trim().replaceAll(",", ".");
                String[] posLines = positionsStr.split("\n");
                for (int j = 0; j < posLines.length; j++) {
                    if (posLines[j].contains("totaal colli") || posLines[j].contains("totale dei colli")||posLines[j].contains("total des colis (6)")) {
                        positionsStr = posLines[j + 1];
                        LOGGER.info(positionsStr);
                        getInvoiceModel().setPositionsCount(Integer.parseInt(positionsStr.split(" ")[0]));
                        break;
                    }
                }
            }break;
            case 2: {
                String positionsStr = getTextFromPageRect(pisotionRects[i], 1).toLowerCase().trim().replaceAll(",", ".");
                String[] posLines = positionsStr.split("\n");
                for (int j = 0; j < posLines.length; j++) {
                    if (posLines[j].contains("varuposter")) {
                        positionsStr = posLines[j + 1];
                        LOGGER.info(positionsStr);
                        getInvoiceModel().setPositionsCount(Integer.parseInt(positionsStr));
                        break;
                    }
                }
            }break;
            case 3: {
                String positionsStr = getTextFromPageRect(pisotionRects[i], 1).toLowerCase().trim().replaceAll(",", ".");
                String[] posLines = positionsStr.split("\n");
                for (int j = 0; j < posLines.length; j++) {
                    if (posLines[j].contains("pozycje")) {
                        positionsStr = posLines[j + 1];
                        LOGGER.info(positionsStr);
                        getInvoiceModel().setPositionsCount(Integer.parseInt(positionsStr));
                        break;
                    }
                }
            }break;
        }


        return getInvoiceModel().getPositionsCount();
    }

    @Override
    public boolean init(InputStream inputStream) {
        invoiceModel = new CustomMrnModel();
        currentPosition = 0;
        return super.init(inputStream);
    }

    /**
     * Считывает все позиции из файла
     *
     * @return спписок считанных позиций
     * @throws Exception
     */
    public List<CimSmgsInvoiceGruz> processPagePositions() throws Exception {
        LOGGER.info("processPagePositions");

        int type = Integer.parseInt(docType.replaceAll("type", "")) - 1;

        switch (type) {
            case 0: {
                processPositionsType1();
            }
            break;
            case 1:
            case 2:
            case 3:
                {
                processPositionsType2();
            }
            break;
        }

        return getInvoiceModel().getPositions();
    }
    public Workbook generateXLS(){
        HSSFWorkbook wb = new HSSFWorkbook();
        BorderStyle thinStyle = BorderStyle.THIN;

        Font font12Calibri = wb.createFont();
        font12Calibri.setFontName("Calibri");
        font12Calibri.setFontHeightInPoints((short) 12);

        HSSFCellStyle calibriMedium12 = excelTools.createWbStyle(thinStyle, wb);
        calibriMedium12.setFont(font12Calibri);
        calibriMedium12.setWrapText(true);

        String[] tableHeader = {"ТНВЕД", "Нетто", "Тара","Брутто"};
        HSSFSheet sheet = wb.createSheet();
        int current_row = 2;
        Row row;

        for (CimSmgsInvoiceGruz gruz:getInvoiceModel().getPositionsMap().values()) {
            row = sheet.createRow(current_row++);
            CellUtil.createCell(row, 0, gruz.getTnved(), calibriMedium12);

            Cell cell = row.createCell(1);
            cell.setCellStyle(calibriMedium12);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(gruz.getMnet().doubleValue());

            cell = row.createCell(2);
            cell.setCellStyle(calibriMedium12);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(gruz.getMtara().doubleValue());

            cell = row.createCell(3);
            cell.setCellStyle(calibriMedium12);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(gruz.getMbrt().doubleValue());

        }

        row = sheet.createRow(1);
        for (int i = 0; i < tableHeader.length; i++) {
            sheet.autoSizeColumn(i);
            CellUtil.createCell(row, i, tableHeader[i], calibriMedium12);
        }

        return wb;
    }

    public void processPositionsType2() throws Exception {
        for (int pageNum = 1; pageNum <= getReader().getNumberOfPages(); pageNum++) {
            List<Integer> positions = new ArrayList<>();
            String pageText = null;
            // разбиваем текст стринцы на строки
            if(getDocType().equals("type4"))
            {
                pageText=getTextFromPageRect(pageNum == 1 ? pageRectType2FirstPageType4 : pageRectType4, pageNum).toLowerCase();
            }
            else
            {
                getTextFromPageRect(pageNum == 1 ? pageRectType2FirstPage : pageRect, pageNum).toLowerCase();
            }


            String[] pageLines = pageText.split("\n");
            // ищем позиции на странице
            for (int i = 0; i < pageLines.length; i++) {
                if (pageLines[i].trim().split(" ")[0].equals(String.valueOf(currentPosition + 1))) {
                    currentPosition++;
                    positions.add(i);
                }
            }
            // обрабатываем позиции
            for (int i = 0; i < positions.size(); i++) {
                CimSmgsInvoiceGruz gruz = new CimSmgsInvoiceGruz();
                int positionEnd = i < positions.size() - 1 ?
                        positions.get(i + 1) :
                        pageLines.length;

                for (int j = positions.get(i); j < positionEnd; j++) {
                    if (gruz.getTnved() == null) {
                        String[] tnvedWords = pageLines[j].replaceAll("[^\\d ]", "").replaceAll(" +", " ").split(" ");
                        for (String tnvedWord : tnvedWords) {
                            if (tnvedWord.length() >= 8) {
                                LOGGER.info("TNVED:" + tnvedWord);
                                gruz.setTnved(tnvedWord);
                                j = positionEnd;
                                break;
                            }
                        }
                    }
                }
                for (int j = positions.get(i); j < positionEnd; j++) {
                    String line=pageLines[j].replaceAll(" +"," ");
                    if(line.contains("ex a")||line.contains("ex c")||line.contains(" ex")){
                        String[] mnetWords = pageLines[j].replaceAll("[^\\d., ]", "").replaceAll(",",".").replaceAll(" +", " ").split(" ");
                        LOGGER.info("MNET:" + mnetWords[mnetWords.length - 1]);
                        gruz.setMnet(new BigDecimal(mnetWords[mnetWords.length - 1]));
                    }
                }
                if(gruz.getMnet()==null)
                {
                 String[] mnetWords = pageLines[positionEnd - 1].replaceAll("[^\\d., ]", "").replaceAll(",",".").replaceAll(" +", " ").split(" ");
                LOGGER.info("MNET:" + mnetWords[mnetWords.length - 1]);
                gruz.setMnet(new BigDecimal(mnetWords[mnetWords.length - 1]));
                }

                if(gruz.getMnet()==null)
                    throw new Exception("Cant find mnet");


                getInvoiceModel().getPositions().add(gruz);
            }
        }
    }

    public void processPositionsType1() throws Exception {

        int pageFirstPositionIndex = -1;
        int pageSecondPosrionIndex = -1;
        int pageThirdPosrionIndex = -1;

        for (int pageNum = 2; pageNum <= getReader().getNumberOfPages(); pageNum++) {
            currentPage=pageNum;
            String pageText = getTextFromPageRect(pageRect, pageNum).toLowerCase();
            String[] pageLines = pageText.split("\n");

            for (int i = 0; i < pageLines.length; i++) {
                if (pageLines[i].trim().indexOf((currentPosition + 1) + " ") == 0 || pageLines[i].trim().equals(String.valueOf(currentPosition + 1))) {
                    if (pageFirstPositionIndex == -1) {
                        pageFirstPositionIndex = i;
                        currentPosition++;
                        boolean ohneFound = false;
                        for (String str : pageLines) {
                            if (str.trim().equals("ohne")) {
                                ohneFound = true;
                                break;
                            }
                        }
                        if (!ohneFound) {
                            for (pageNum = pageNum + 1; pageNum <= getReader().getNumberOfPages(); pageNum++) {
                                if (ohneFound) {
                                    pageNum--;
                                    break;
                                }
                                pageText = getTextFromPageRect(pageRect, pageNum).toLowerCase();
                                pageLines = pageText.split("\n");
                                for (i = 0; i < pageLines.length; i++) {
                                    if (pageLines[i].trim().equals("ohne")) {
                                        pageFirstPositionIndex = 0;
                                        ohneFound = true;
                                        break;
                                    }
                                }
                            }
                        }

                        continue;
                    }
                    if (pageSecondPosrionIndex == -1) {
                        pageSecondPosrionIndex = i;
                        currentPosition++;
                        continue;
                    }
                    pageThirdPosrionIndex = i;
                    currentPosition++;
                    break;
                }
            }

            if (pageFirstPositionIndex != -1) {

                int searchFinish = pageLines.length;
                if (pageSecondPosrionIndex != -1)
                    searchFinish = pageSecondPosrionIndex;

                CimSmgsInvoiceGruz gruz = parsePosition(pageLines, pageFirstPositionIndex, searchFinish);
                getInvoiceModel().getPositions().add(gruz);
                pageFirstPositionIndex = -1;

            }
            if (pageSecondPosrionIndex != -1) {
                int searchFinish = pageLines.length;
                if (pageThirdPosrionIndex != -1)
                    searchFinish = Math.min(pageThirdPosrionIndex,pageLines.length);

                CimSmgsInvoiceGruz gruz = parsePosition(pageLines, pageSecondPosrionIndex, searchFinish);
                getInvoiceModel().getPositions().add(gruz);
                pageSecondPosrionIndex = -1;
            }
            if (pageThirdPosrionIndex != -1&&pageLines.length-pageThirdPosrionIndex>10) {
                CimSmgsInvoiceGruz gruz = parsePosition(pageLines, pageThirdPosrionIndex, pageLines.length);
                getInvoiceModel().getPositions().add(gruz);
                pageThirdPosrionIndex = -1;
            }
        }
    }

    /**
     * ОБрабатывает одну позицию из выбранног интервала
     *
     * @param pageLines       список строк
     * @param positionStarIdx индекс начала позиции
     * @param positionEndIdx  индекс конца позиции
     * @return позицю
     * @throws Exception
     */
    public CimSmgsInvoiceGruz parsePosition(String[] pageLines, int positionStarIdx, int positionEndIdx) throws Exception {
        LOGGER.info("parsePosition N"+currentPosition);
        CimSmgsInvoiceGruz gruz = new CimSmgsInvoiceGruz();

        for (int i = positionStarIdx; i < positionEndIdx; i++) {
            if (pageLines[i].trim().equals("ohne")) {
                gruz.setTnved(pageLines[i - 1].trim());
                break;
            }
        }
        if (gruz.getTnved() == null)
            throw new Exception("Cant find TNVED for position N:" + currentPosition);

        String[] mnetStrWords = pageLines[positionEndIdx - 1].replaceAll(" +", " ").split(" ");
        String mnetString = mnetStrWords[mnetStrWords.length - 1].replaceAll(",", ".");
        LOGGER.debug(mnetString);
        try {
            gruz.setMnet(new BigDecimal(mnetString));
        }
        catch (Exception e)
        {
            if(currentPage<getReader().getNumberOfPages())
            {
                int idx=-1;
                currentPage++;
                for (int i = currentPage; i <getReader().getNumberOfPages() ; i++) {
                    idx=findNextPosition(currentPage);
                    if(idx!=-1)
                    {
                        break;
                    }
                }
                String pageText = getTextFromPageRect(pageRect, currentPage).toLowerCase();
                pageLines = pageText.split("\n");
                positionEndIdx=idx!=-1?idx:pageLines.length;
                mnetStrWords = pageLines[positionEndIdx - 1].replaceAll(" +", " ").split(" ");
                mnetString = mnetStrWords[mnetStrWords.length - 1].replaceAll(",", ".");
                LOGGER.debug(mnetString);
                gruz.setMnet(new BigDecimal(mnetString));
            }
        }

        return gruz;
    }

    public int findNextPosition(int pageNum) throws IOException {
        int idx=-1;
        String pageText = getTextFromPageRect(pageRect, pageNum).toLowerCase();
        String[] pageLines = pageText.split("\n");

        for (int i = 0; i < pageLines.length; i++) {
            if (pageLines[i].trim().indexOf((currentPosition + 1) + " ") == 0 || pageLines[i].trim().equals(String.valueOf(currentPosition + 1))) {
                return i;
            }
        }

        return idx;
    }

    public CustomMrnModel processFile() throws Exception {
        readBrutto();
        if (getInvoiceModel().getGrossWeight() == null || getInvoiceModel().getGrossWeight().equals(new BigDecimal(0)))
            throw new Exception("Total brutto read Error!");

        readPositionsCount();
        if (getInvoiceModel().getPositionsCount() == null || getInvoiceModel().getPositionsCount() == 0)
            throw new Exception("Positions count read Error");

        processPagePositions();
        if (getInvoiceModel().getPositions().size() != getInvoiceModel().getPositionsCount())
            throw new Exception("Positions count read from file not equal count of actual read positions. Position count from first page:" + getInvoiceModel().getPositionsCount() +
                    ". Positions count:" + getInvoiceModel().getPositions().size());

        calcWeight();
        positions2map();

        return getInvoiceModel();
    }

    public void positions2map() {
        getInvoiceModel().setPositionsMap(new TreeMap<>());

        for (CimSmgsInvoiceGruz gruz : getInvoiceModel().getPositions()) {
            if (getInvoiceModel().getPositionsMap().get(gruz.getTnved()) == null) {
                getInvoiceModel().getPositionsMap().put(gruz.getTnved(), gruz);
            } else {
                CimSmgsInvoiceGruz gruzFromMap = getInvoiceModel().getPositionsMap().get(gruz.getTnved());
                gruzFromMap.setMnet(gruz.getMnet().add(gruz.getMnet()));
            }
        }
    }

    /**
     * подсчитываем нетто для каждой позиции
     */
    public void calcWeight()
    {
        LOGGER.debug("calcWeight");
        BigDecimal totalNetWeight = new BigDecimal("0");
        BigDecimal allPosBrutto= new BigDecimal("0");

        // подсчитываем сумарное нетто
        for (CimSmgsInvoiceGruz position :getInvoiceModel().getPositions()) {
            totalNetWeight= totalNetWeight.add(position.getMnet());
        }
        getInvoiceModel().setNetWeight(totalNetWeight);

        // сумарная тара
        BigDecimal totalTara= getInvoiceModel().getGrossWeight().subtract(getInvoiceModel().getNetWeight());
        // коэффициент тары на единицу веса нетто
        BigDecimal coof=totalTara.divide(totalNetWeight, RoundingMode.CEILING).stripTrailingZeros();

        // считаем брутто для каждо позиции
        for (CimSmgsInvoiceGruz position :getInvoiceModel().getPositions()) {
            BigDecimal posTara=position.getMnet().multiply(coof).setScale(3,RoundingMode.CEILING);

            position.setMtara(posTara);
            position.setMbrt(position.getMnet().add(posTara));
            allPosBrutto=allPosBrutto.add(position.getMbrt());
        }
        // считаем ошибку округления
        BigDecimal errorBrutto=getInvoiceModel().getGrossWeight().subtract(allPosBrutto);
        // добавляем ошибку округления в позицию с максимальным значением
        for(int mult=10;mult>=1;mult--) {
            for (CimSmgsInvoiceGruz position : getInvoiceModel().getPositions()) {
                if (position.getMbrt() != null && position.getMbrt().doubleValue() > (Math.abs(errorBrutto.doubleValue()) * mult)) {
                    position.setMbrt(position.getMbrt().add(errorBrutto));
                    position.setMnet(position.getMnet().add(errorBrutto));
                    return;
                }
            }
        }
    }



    public CustomMrnModel getInvoiceModel() {
        return invoiceModel;
    }

    public void setInvoiceModel(CustomMrnModel invoiceModel) {
        this.invoiceModel = invoiceModel;
    }

    public String getDocType() {
        return docType;
    }
}
