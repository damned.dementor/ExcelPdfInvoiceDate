package Ti.DataProcessing.pdf;

import Ti.model.MrnModel;
import Ti.model.PDFmodelMrn;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.pdf.parser.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * ProcessMrnPDFfile class processing SCHNEIDER ELECTRIC (CHINA) CO. LTD mrn file.
 */
public class ProcessMrnPDFfile extends AbstractProcessPDFfile {

    private static Logger LOGGER = Logger.getLogger(ProcessMrnPDFfile.class);
    private PDFmodelMrn pdFmodelMrn = new PDFmodelMrn();
    private List<String> positions=new ArrayList<>();
    private ArrayList<String> arrayListStrings= new ArrayList<>();

    public boolean readFirstPage() throws IOException {
        LOGGER.debug("ProcessMrnPDFfile.readFirstPage");
        if (inited) {
            Rectangle2D rect6 = new Rectangle(336, 725, 25, 10);
            Rectangle2D rect6_2 = new Rectangle(331, 675, 25, 10);
            //
            Rectangle2D rectBrutto = new Rectangle(430, 570, 70, 10);
            Rectangle2D rectBrutto2 = new Rectangle(430, 520, 70, 10);
            Rectangle2D rectBrutto3 = new Rectangle(435, 480, 70, 10);
            RenderFilter filter = new RegionTextRenderFilter(rect6);
            RenderFilter filter2 = new RegionTextRenderFilter(rect6_2);
            TextExtractionStrategy strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
            TextExtractionStrategy strategy2 = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter2);
            String total = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase();

            if (!StringUtils.isNumeric(total)||Integer.parseInt(total)<1) {
                total = PdfTextExtractor.getTextFromPage(reader, 1, strategy2).toLowerCase();
                if (!StringUtils.isNumeric(total)||Integer.parseInt(total)<1)
                    throw new IllegalArgumentException("Количество мест считалось с ошибкой!");
            }

            pdFmodelMrn.setTotalDes(Integer.parseInt(total));

            filter = new RegionTextRenderFilter(rectBrutto);
            strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);

            String brutto = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase();
            if (StringUtils.isNumeric(brutto.replaceAll("\\.", "").replaceAll(",", ""))) {
                pdFmodelMrn.setMasBrutto(new BigDecimal(brutto.replaceAll(",", ".")));
            }
            if(pdFmodelMrn.getMasBrutto()==null)
            {
                filter = new RegionTextRenderFilter(rectBrutto2);
                strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
                brutto = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase();
                if (StringUtils.isNumeric(brutto.replaceAll("\\.", "").replaceAll(",", ""))) {
                    pdFmodelMrn.setMasBrutto(new BigDecimal(brutto.replaceAll(",", ".")));
                }
            }
            if(pdFmodelMrn.getMasBrutto()==null)
            {
                filter = new RegionTextRenderFilter(rectBrutto3);
                strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
                brutto = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase();
                if (StringUtils.isNumeric(brutto.replaceAll("\\.", "").replaceAll(",", ""))) {
                    pdFmodelMrn.setMasBrutto(new BigDecimal(brutto.replaceAll(",", ".")));
                }
            }


            LOGGER.debug(pdFmodelMrn);
            return true;
        } else {
            LOGGER.debug("not inited");
        }
        return false;
    }

    public void readTable() throws IOException {
        LOGGER.debug("ProcessMrnPDFfile.readTable");
        if (inited) {
            if (reader.getNumberOfPages() > 1) {

                for (int i = 2; i <= reader.getNumberOfPages(); i++) {
                    Rectangle2D rectdocN = new Rectangle(0, 0, 595, 660);
                    RenderFilter filter = new RegionTextRenderFilter(rectdocN);
                    TextExtractionStrategy strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);
                    String page = PdfTextExtractor.getTextFromPage(reader, i, strategy).toLowerCase().trim().replaceAll(" +", " ");
                    wholeText.append(page.trim().replaceAll(" +", " "));
                    wholeText.append("\n");
                }
            } else {
                Rectangle2D rectdocN = new Rectangle(0, 135, 595, 200);
                RenderFilter filter = new RegionTextRenderFilter(rectdocN);
                TextExtractionStrategy strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
                String page = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase().trim().replaceAll(" +", " ");
                wholeText.append(page.trim().replaceAll(" +", " "));
            }
        } else {
            LOGGER.debug("not inited");
        }
        LOGGER.debug(wholeText.toString());
    }

    public void cleantext()
    {

        String[] delStr= {"groupage",
//                "schneider electric industries",
                "grd rail",
                "fabriques a partir de feuilles",
                "ensembles disjoncteurs",
        "non utilises pour telecommunications"};
//        arrayListStrings.addAll(Arrays.asList(wholeText.toString().split("\n")));
        for(String str:wholeText.toString().split("\n"))
        {
            boolean accepted=true;
            for (String delmark:delStr) {
                if(str.contains(delmark))
                    accepted=false;
            }
            if (accepted)
                arrayListStrings.add(str);

        }
    }

    public void processText() {
        LOGGER.debug("ProcessMrnPDFfile.processText");
        if (inited) {
            int position=1;

//            arrayStrings=wholeText.toString().split("\n");
            for (int indx=0;indx<arrayListStrings.size();indx++)
            {
                int res_start=findStrInArrayList(arrayListStrings,indx,Integer.toString(position));
                if(res_start!=-1)
                {

                    position++;
                    int finish=findStrInArrayList(arrayListStrings,indx,Integer.toString(position));
                    if(finish==-1)
                        finish=arrayListStrings.size();

                    StringBuilder builder = new StringBuilder();
                    for(int cancat_idx=res_start;cancat_idx<finish;cancat_idx++)
                    {
                        builder.append(arrayListStrings.get(cancat_idx));
                        builder.append("\n");
                    }
                    indx=finish-1;
                    positions.add(builder.toString());
                }
            }
            LOGGER.debug(positions);

        } else {
            LOGGER.debug("not inited");
        }
    }

    public void parseTable()
    {
        LOGGER.debug("ProcessMrnPDFfile.parseTable");
        if(inited)
        {
            for (String position:positions) {
                String[] positionStrings=position.split("\n");
                MrnModel mrnModel= new MrnModel();
                mrnModel.setPosition(Integer.parseInt(positionStrings[0]));

                int numCommodity=findStringWithText(positionStrings,1,"schneider electric industries");
                String commodityCode_2=positionStrings[numCommodity+1].split(" ")[0];
                if(commodityCode_2.length()>5) {
                    mrnModel.setCommodityCode_2(commodityCode_2);
                    mrnModel.setInvoiceNumber(positionStrings[numCommodity + 3].split(" ")[1]);
                }
                else
                {
                    commodityCode_2=positionStrings[numCommodity+5].split(" ")[0];
                    if(commodityCode_2.length()>5)
                    {
                        mrnModel.setCommodityCode_2(commodityCode_2);
                        mrnModel.setInvoiceNumber(positionStrings[numCommodity + 4].split(" ")[1]);
                    }
                    else
                    {
                        commodityCode_2=positionStrings[numCommodity+4].split(" ")[1];
                        mrnModel.setCommodityCode_2(commodityCode_2);
                        mrnModel.setInvoiceNumber(positionStrings[numCommodity + 3].split(" ")[1]);
                    }

                }

                int brutto_ind = findStringWithText(positionStrings, 4, " cn ");
                if(brutto_ind==-1)
                    brutto_ind = findStringWithText(positionStrings, 4, " hk ");

                String[] bruttoMas = positionStrings[brutto_ind].split(" ");
                mrnModel.setBrutto(new BigDecimal(bruttoMas[bruttoMas.length - 1]));

                int netto_ind=findStringWithText(positionStrings,4,"ex c");
                String[] nettoMas=positionStrings[netto_ind].split(" ");
                String netto=nettoMas[nettoMas.length-1];

                if (StringUtils.isNumeric(netto.replaceAll("\\.", "").replaceAll(",", ""))) {
                    mrnModel.setNetto(new BigDecimal(nettoMas[nettoMas.length-1]));
                }
                else
                    mrnModel.setNetto(new BigDecimal(0));

                pdFmodelMrn.getMrnList().add(mrnModel);
                LOGGER.debug(mrnModel);
            }
        }
    }

    /**
     * cancatCargoRecords cancatinate
     */
    public void cancatMRNRecords() {
        LOGGER.debug("ProcessInvoicePDFfile.cancatCargoRecords");
        ArrayList<MrnModel> mrnList = pdFmodelMrn.getMrnList();
        ArrayList<MrnModel> newmrnList = new ArrayList<>();

        while (mrnList.size() > 0) {

            MrnModel model = mrnList.get(0);
            ArrayList<Integer> indexList = new ArrayList<>();
            for (int n = 0; n < mrnList.size(); n++) {
                if (model.getCommodityCode_2().equals(mrnList.get(n).getCommodityCode_2()) && model.getInvoiceNumber().equals(mrnList.get(n).getInvoiceNumber())) {
                    indexList.add(n);
                }
            }
            for (Integer idx : indexList) {
                if(idx!=0) {
                    model.setBrutto(model.getBrutto().add(mrnList.get(idx).getBrutto()));
                    model.setNetto(model.getNetto().add(mrnList.get(idx).getNetto()));
                }
            }

            for (int idx=indexList.size()-1;idx>=0;idx--) {
                mrnList.remove(indexList.get(idx).intValue());
            }
            newmrnList.add(model);
        }
//        Collections.sort(newCargoList,byCommodityCode);

        pdFmodelMrn.setMrnList(newmrnList);
    }

    public PDFmodelMrn getPdFmodelMrn() {

        return pdFmodelMrn;
    }

    public List<String> getPositions() {
        return positions;
    }

}
