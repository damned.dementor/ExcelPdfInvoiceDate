package Ti.DataProcessing.pdf;

import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class to most common PDF reading actions
 */
public abstract class  AbstractProcessPDFfile {
    private static Logger LOGGER = Logger.getLogger(AbstractProcessPDFfile.class);
    PdfReader reader;
    StringBuilder wholeText = new StringBuilder();
    protected boolean inited = false;

    /**
     * Opens PDF file and set init parameter to true if file is valid
     * @param inputStream file input stream
     * @return success flag
     */
    public boolean init(InputStream inputStream) {
        LOGGER.debug("----------------------------------------------------------------ProcessPDFfile.init");
        try {
            reader = new PdfReader(inputStream);
        } catch (IOException e) {
            LOGGER.error("Wrong PDF");
            inited = false;
            return false;
        }
        LOGGER.debug("PDF inited");
        inited = true;
        return true;
    }
    /**
     * Считывает текст из прямоугольника
     * @param rect прямоугольник
     * @param num номер страницы
     * @return
     * @throws IOException
     */
    public String getTextFromPageRect(Rectangle2D rect, int num) throws IOException {
        RenderFilter filter = new RegionTextRenderFilter(rect);
        TextExtractionStrategy strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);
        return  PdfTextExtractor.getTextFromPage(getReader(), num, strategy).toLowerCase().trim();
    }

    /**
     * findStringWithText service method to find string in String Arraylist.
     * @param list Arraylist
     * @param start start position
     * @param search word to find
     * @return posiotion of string
     */
    public int findStringWithPosInText(List<String> list, int start, String search, int pos) {
        int kurpos = start;
        while (kurpos < list.size()) {
            if (list.get(kurpos).toLowerCase().contains(search.toLowerCase())&&list.get(kurpos).toLowerCase().indexOf(search.toLowerCase())==pos)
                return kurpos;
            kurpos++;
        }
        return -1;
    }

    /**
     * findStringWithText service method to find string in String Arraylist.
     * @param list Arraylist
     * @param start start position
     * @param search word to find
     * @return
     */
    public int findStringWithText(List<String> list, int start, String search) {
        int kurpos = start;
        while (kurpos < list.size()) {
            if (list.get(kurpos).toLowerCase().contains(search.toLowerCase()))
                return kurpos;
            kurpos++;
        }
        return -1;
    }
    /**
     * findStringWithText service method to find string in String Array.
     * @param list Arraylist
     * @param start start position
     * @param search word to find
     * @return
     */
    public int findStringWithText(String[] list, int start, String search) {
        int kurpos = start;
        while (kurpos < list.length) {
            if (list[kurpos].contains(search))
                return kurpos;
            kurpos++;
        }
        return -1;
    }

    /**
     * findNotempty service method to find non empty string in String Array.
     * @param str array
     * @param start start position
     * @return result index
     */
    public int findNotempty(String[] str, int start) {
        for (int i = start; i < str.length; i++) {
            if (!str[i].isEmpty()) {
                return i;
            }
        }
        return -1;
    }
    /**
     * findNotempty service method to find exact string in String Array.
     * @param str array
     * @param start start position
     * @return result index
     */
    public int findStrInArrayList(ArrayList<String>str, int start,String search) {
        for (int i = start; i < str.size(); i++) {
            if (str.get(i).equals(search)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * returns initiation state flag
     * @return
     */
    public boolean isInited() {
        return inited;
    }

    public void setInited(boolean inited) {
        this.inited = inited;
    }

    public void setReader(PdfReader reader) {
        this.reader = reader;
    }

    public PdfReader getReader() {
        return reader;
    }

    public StringBuilder getWholeText() {
        return wholeText;
    }
}
