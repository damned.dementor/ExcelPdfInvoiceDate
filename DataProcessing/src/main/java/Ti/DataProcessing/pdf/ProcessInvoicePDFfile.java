package Ti.DataProcessing.pdf;

import Ti.model.CargoModel;
import Ti.model.PDFmodelInvoice;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.pdf.parser.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * ProcessInvoicePDFfile class processing SCHNEIDER ELECTRIC (CHINA) CO. LTD invoice file.
 */
public class ProcessInvoicePDFfile extends AbstractProcessPDFfile {
    private static Logger LOGGER = Logger.getLogger(ProcessInvoicePDFfile.class);
    private int parseType = -1;

    private ArrayList<String> dataTable = new ArrayList<>();

    /**
     * parsed PDF object
     */
    private PDFmodelInvoice pdFmodel = new PDFmodelInvoice();

    public ProcessInvoicePDFfile() {
    }

    /**
     * readFirstPage reads first page of pdf file.
     *
     * @return success result
     * @throws IOException fileread exception
     */
    public boolean readFirstPage() throws IOException {
        LOGGER.debug("ProcessPDFfile.readFirstPage");
        String docN, docD, totalNA;

        if (inited) {
            TextExtractionStrategy strategy = new MyLocationTextExtractionStrategy();
            String text = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase().replaceAll(" +", " ");
            text = text.replaceAll("total amount of invoice", "");
            setParseType(text);
            for (String s : text.split("\n")) {
                if (s.contains("document number:")) {
                    docN = s.substring("document number:".length()).trim();
                    pdFmodel.setDocNumber(docN);
                    LOGGER.debug("document number:" + docN);
                }
                if (s.contains("document date:")) {
                    docD = s.substring("document date:".length()).trim().replaceAll(" +", " ");
                    LOGGER.debug("document date:" + docD);

                    DateFormat dfEn = new SimpleDateFormat(
                            "dd MMM yyyy", Locale.ENGLISH);
                    try {

                        pdFmodel.setDocDate(dfEn.parse(docD));
                    } catch (ParseException e) {
                        pdFmodel.setDocDate(null);
                        LOGGER.error("Error date parsing");
                        return false;
                    }
                }
                if (s.contains("total net amount")) {
                    totalNA = s.substring("total net amount".length()).trim();
                    LOGGER.debug(totalNA);
                    if(totalNA.contains(".") && totalNA.contains(",") &&totalNA.indexOf(".")>totalNA.indexOf(",")||
                            totalNA.contains(".") && !totalNA.contains(",")
                    ){
                        pdFmodel.setTotalNetAmount(new BigDecimal(totalNA.replaceAll(",", "").replaceAll(" ", "")));
                    }
                    else {
                        pdFmodel.setTotalNetAmount(new BigDecimal(totalNA.replaceAll("\\.", "").replaceAll(",", ".").replaceAll(" ", "")));
                    }
                    LOGGER.debug("total net amount:" + pdFmodel.getTotalNetAmount());
                }
                if (s.contains("total amount")) {
                    return extractTotalAmounCurrency(s);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * setParseType - service method that sets parsing type by PDF first page.
     *
     * @param text text for analyse
     */
    private void setParseType(String text) {
        LOGGER.debug("setParseType");
        if (text.replaceAll(" +", " ").contains("item material description"))
            parseType = 2;
        else
            parseType = 1;
//        if (isInited()) {
//
//            if (pdFmodel.getTotalAmount() == null)
//                parseType = 2;
//            else {
//                    parseType = 1;
//            }
//        }
        LOGGER.debug("parseType:" + parseType);
    }


    /**
     * extractTotalAmounCurrency - service method that extract total amount and currency values.
     *
     * @param s string for extraction
     * @return success result
     */
    private boolean extractTotalAmounCurrency(String s) {
        String totalAmark = "total amount";
        String totalA = "";
        String cur = "";
        String[] mas = s.substring(totalAmark.length()).trim().replaceAll(" +", " ").split(" ");
        if (mas.length > 1) {
            for (int n=0;n<mas.length-1;n++) {
                totalA += mas[n];
            }

            cur = mas[mas.length-1];

            try {
                pdFmodel.setTotalAmount(new BigDecimal(totalA.replaceAll("\\.", "").replaceAll(",", ".")));
            } catch (Exception e) {
                LOGGER.error("total Amount parsing error");
                return false;
            }
            pdFmodel.setCurency(cur);
        }
        LOGGER.debug(s);
        LOGGER.debug(totalAmark + ":" + totalA);
        LOGGER.debug("currency:" + cur);
        return true;
    }

    /**
     * readText reads data table from PDF in dependency of chosen parsing type.
     *
     * @throws IOException file read exception
     */
    public void readText() throws IOException {

        if (parseType == 2) {
            readFirstPageTable();
        }
        readTable();
    }

    /**
     * readFirstPageTable reads table data from first page.
     *
     * @throws IOException throws file read exception
     */
    public void readFirstPageTable() throws IOException {
        if (inited) {
            Rectangle2D rectdocN = new Rectangle(0, 110, 595, 400);
            RenderFilter filter = new RegionTextRenderFilter(rectdocN);
            TextExtractionStrategy strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
            String page = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase();
            wholeText.append(page.replaceAll(" +", " "));
            wholeText.append("\n");
//            processPage(page);
        }
    }

    /**
     * readTable reads table data from  page 2 till end.
     *
     * @throws IOException throws file read exception
     */
    public void readTable() throws IOException {
        LOGGER.debug("ProcessInvoicePDFfile.readTable");
        if (inited) {
            for (int i = 2; i <= reader.getNumberOfPages(); i++) {
//                LOGGER.debug("--------------"+i+"-------------------");
                Rectangle2D rectdocN = new Rectangle(0, 108, 595, 600);
                RenderFilter filter = new RegionTextRenderFilter(rectdocN);
                TextExtractionStrategy strategy = new FilteredTextRenderListener(new MyLocationTextExtractionStrategy(), filter);
                String page = PdfTextExtractor.getTextFromPage(reader, i, strategy).toLowerCase();

                wholeText.append(page.replaceAll(" +", " "));
                wholeText.append("\n");
            }
            processText(wholeText.toString());
            LOGGER.debug("-----------dataTable-----------------");
            for (String s : dataTable) {
                LOGGER.debug(s);
            }
        }
        reader.close();
    }

    /**
     * processText delete unnecessary data from table data.
     *
     * @param page text for clearing
     */
    private void processText(String page) throws NullPointerException {
        LOGGER.debug("ProcessInvoicePDFfile.processText");
        String[] delMark = {"shipment document",
                "customer order number",
                "schneider order number",
                "your po item", "item material description",
                "item material description",
                "item reference:",
                "customer material nr.",
                "document sent through edi",
                "delivery notes list"};
        String totalNA;
//        page = page.replaceAll(" +", " ");
        String[] pageMas = page.split("\n");
        for (int n = 0; n < pageMas.length; n++) {
            String str = pageMas[n];
            boolean addString = true;
            for (String delStr : delMark) {
                str = str.replaceAll(" +", " ");
                if (str.indexOf(delStr) != -1)
                    addString = false;
            }
            if (str.indexOf("countries of origin manufacturer city") != -1)
                break;

            if (str.contains("total net amount")) {
                totalNA = str.substring("total net amount".length()).trim();
                if (pdFmodel.getTotalNetAmount() == null) {
                    pdFmodel.setTotalNetAmount(new BigDecimal(totalNA.replaceAll("\\.", "").replaceAll(",", ".")));
                    LOGGER.debug("total net amount:" + pdFmodel.getTotalNetAmount());
                }
                for (n = n + 1; n < pageMas.length; n++) {
                    str = pageMas[n];

                    if (!str.contains("total amount of invoice") && str.contains("total amount")) {
                        extractTotalAmounCurrency(str);
                        break;
                    }
                }
                break;
            }
            if (addString)
                dataTable.add(str.trim());
        }
    }

    /**
     * parseTable parses data table in dependency of chosen parsing type.
     */
    public void parseTable() {
        switch (parseType) {
            case 1:
                parseTableType1();
                break;
            case 2:
                parseTableType2();
                break;
        }
    }

    /**
     * parseTableType1 parse data table for PDF type 1.
     */
    private void parseTableType1() {
        if (inited) {
            LOGGER.debug("-------------parseTableType1------------------");
            for (int i = 0; i < dataTable.size() - 1; i++) {
                StringBuilder name_mixin = new StringBuilder();
                CargoModel cargoModel = new CargoModel();

                int idx_str1 = findStringWithText(dataTable, i, " " + getPdFmodel().getCurency());
                if(idx_str1==-1)
                {
                    idx_str1 = findStringWithText(dataTable, i, getPdFmodel().getCurency());
                    if(idx_str1!=-1)
                    {
                        dataTable.set(idx_str1,dataTable.get(idx_str1).replaceAll(getPdFmodel().getCurency()," " + getPdFmodel().getCurency()));
                    }
                }
                if (idx_str1 != -1) {
                    String str1 = dataTable.get(idx_str1);
                    int idx_str2 = findStringWithText(dataTable, i, "pce");
                    String str2 = dataTable.get(idx_str2);
                    if (idx_str2 != idx_str1 + 1) {
                        for (int num = idx_str1 + 1; num < idx_str2; num++) {
                            name_mixin.append(dataTable.get(num));
                        }
                    }
                    i = idx_str2;

                    cargoModel.setPOItem(str1.split(" ")[0]);
                    str1 = str1.replaceFirst("\\w+ \\w+ ", "");
                    str2 = str2.replaceFirst("\\w+ \\w+ ", "");

                    String[] str1splited = str1.split(" ");
                    if(str1splited[str1splited.length - 2].contains(".") && str1splited[str1splited.length - 2].contains(",")
                            &&str1splited[str1splited.length - 2].indexOf(".")>str1splited[str1splited.length - 2].indexOf(",")||
                            str1splited[str1splited.length - 2].contains(".") && !str1splited[str1splited.length - 2].contains(",")
                    ){
                        cargoModel.setTotalPrice(new BigDecimal(str1splited[str1splited.length - 2].replaceAll(",", "")));
                    }
                    else {
                        cargoModel.setTotalPrice(new BigDecimal(str1splited[str1splited.length - 2].replaceAll("\\.", "").replaceAll(",", ".")));
                    }
                    cargoModel.setQty(Long.parseLong(str1splited[str1splited.length - 3]));
                    cargoModel.setCurrency(pdFmodel.getCurency());
                    StringBuilder descr = new StringBuilder();
                    for (int some_n = 0; some_n < str1splited.length - 4; some_n++) {
                        if (descr.length() > 0)
                            descr.append(" ");
                        descr.append(str1splited[some_n]);
                    }
                    if (name_mixin.length() != 0)
                        descr.append(" ").append(name_mixin);
                    cargoModel.setDescription(descr.toString());
//                    for (int strNum = 0; strNum < str1splited.length; strNum++) {
//                        if (StringUtils.isNumeric(str1splited[strNum].replaceAll("\\.", "").replaceAll(",", ""))) {
//                            cargoModel.setDescription(str1.substring(0, str1.indexOf(str1splited[strNum])).trim());
//
//                            int nextIndx = findNotempty(str1splited, strNum + 1);
//                            if (nextIndx != -1)
//                                cargoModel.setQty(Long.parseLong(str1splited[nextIndx]));
//
//                            nextIndx = findNotempty(str1splited, nextIndx + 1);
//                            if (nextIndx != -1)
//                                cargoModel.setTotalPrice(new BigDecimal(str1splited[nextIndx].replaceAll("\\.", "").replaceAll(",", ".")));
//
//                            nextIndx = findNotempty(str1splited, nextIndx + 1);
//                            if (nextIndx != -1)
//                                cargoModel.setCurrency(str1splited[nextIndx]);
//
//                            break;
//                        }
//                    }
                    String[] str2splited = str2.split(" ");
                    for (int strNum = 0; strNum < str2splited.length; strNum++) {
                        String clean=str2splited[strNum].replaceAll("\\.", "").replaceAll(",", "");
                        if (StringUtils.isNumeric(clean)&&(clean.length()>5)) {
                            cargoModel.setCommodityCode(str2splited[strNum]);
                            break;
                        }
                    }
                    pdFmodel.getCargoList().add(cargoModel);
                    LOGGER.debug(cargoModel);
                }
            }
        }
    }

    /**
     * parseTableType1 parse data table for PDF type 2.
     */
    private void parseTableType2() {
        LOGGER.debug("-------------parseTableType2------------------");
        if (inited) {
            for (int i = 0; i < dataTable.size() - 1; i++) {
                StringBuilder description = new StringBuilder();

                CargoModel cargoModel = new CargoModel();
                i = findStringWithText(dataTable, i, "pce");

                String[] strsplited = dataTable.get(i).split(" ");
                cargoModel.setPOItem(strsplited[0]);
                int pcePos = Collections.indexOfSubList(Arrays.asList(strsplited), Arrays.asList("pce"));
                if (pcePos != -1) {
                    cargoModel.setPOItem(strsplited[0]);
                    cargoModel.setQty(Long.parseLong(strsplited[pcePos - 1].replaceAll("\\.", "").replaceAll(",", "")));

                    strsplited[0] = "";
                    strsplited[1] = "";
                    strsplited[pcePos] = "";
                    strsplited[pcePos - 1] = "";
                    for (String s : strsplited) {
                        if (!s.isEmpty()) {
                            if (description.length() > 0)
                                description.append(" ");
                            description.append(s);
                        }
                    }
                }
                i++;
                int start = i;
                i = findStringWithText(dataTable, i, "pce");
                int stop = i;
                for (int m = start; m < stop; m++) {
                    if (description.length() > 0)
                        description.append(" ");
                    description.append(dataTable.get(m));
                }
                cargoModel.setDescription(description.toString());
                String totalPrice = dataTable.get(i).substring(dataTable.get(i).indexOf("pce") + "pce".length()).trim();
                cargoModel.setTotalPrice(new BigDecimal(totalPrice.split(" ")[0].replaceAll("\\.", "").replaceAll(",", ".")));

                i = findStringWithText(dataTable, i, "commodity code:");
                String commodotyCode = dataTable.get(i).substring(dataTable.get(i).indexOf("commodity code:") + "commodity code:".length());

                cargoModel.setCommodityCode(commodotyCode.trim().split(" ")[0]);

                cargoModel.setCurrency(getPdFmodel().getCurency());
                pdFmodel.getCargoList().add(cargoModel);
                LOGGER.debug(cargoModel);
            }
        }
    }

    /**
     * cancatCargoRecords cancatinate
     */
    public void cancatCargoRecords() {
        LOGGER.debug("ProcessInvoicePDFfile.cancatCargoRecords");
        ArrayList<CargoModel> cargoList = pdFmodel.getCargoList();
        ArrayList<CargoModel> newCargoList = new ArrayList<>();

        while (cargoList.size() > 0) {

            CargoModel model = cargoList.get(0);
            ArrayList<Integer> indexList = new ArrayList<>();
            for (int n = 0; n < cargoList.size(); n++) {
                if (model.getDescription().equals(cargoList.get(n).getDescription()) && model.getCommodityCode().equals(cargoList.get(n).getCommodityCode())) {
                    indexList.add(n);
                }
            }
            for (Integer idx : indexList) {
                if(idx!=0) {
                    model.setQty(model.getQty() + cargoList.get(idx).getQty());
                    model.setTotalPrice(model.getTotalPrice().add(cargoList.get(idx).getTotalPrice()));
                }
            }

            for (int idx=indexList.size()-1;idx>=0;idx--) {
                cargoList.remove(indexList.get(idx).intValue());
            }
            newCargoList.add(model);
        }
        Comparator<CargoModel> byCommodityCode= Comparator.comparing(CargoModel::getCommodityCode);

        newCargoList.sort(byCommodityCode);
//        Collections.sort(newCargoList,byCommodityCode);

        pdFmodel.setCargoList(newCargoList);
    }


    public PDFmodelInvoice getPdFmodel() {
        return pdFmodel;
    }

    public int getParseType() {
        return parseType;
    }

    public StringBuilder getWholeText() {
        return wholeText;
    }
}
