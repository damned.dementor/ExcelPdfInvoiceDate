package Ti.DataProcessing.pdf.wago;

import Ti.DataProcessing.pdf.AbstractProcessPDFfile;
import Ti.model.Wago.WagoInvoiceModel;
import Ti.model.Wago.WagoPosition;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.pdf.parser.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static Ti.DataProcessing.excel.excelTools.createCell;
import static Ti.DataProcessing.excel.excelTools.createWbStyle;

public class ProcessWagoInvoicePDF extends AbstractProcessPDFfile {

    private static Logger LOGGER = Logger.getLogger(ProcessWagoInvoicePDF.class);

    private WagoInvoiceModel model = new WagoInvoiceModel();
    // весь текст позиций
    private StringBuilder wholeText = new StringBuilder();
    // весь текст стоимостей (Position value)
    private StringBuilder wholePositionValueText = new StringBuilder();
    private StringBuilder finalTableCustomsTarif = new StringBuilder();
    private StringBuilder finalTableNetWeight= new StringBuilder();
    private StringBuilder finalTablePrice= new StringBuilder();
    //список текстов позиций
    private List<String> positionStrings= new ArrayList<>();
    //список текстов стоимостей позиций
    private List<String> positionVlaueStrings= new ArrayList<>();
    private List<WagoPosition> positions = new ArrayList<>();
    private Map<String,BigDecimal> lastpageMap= new HashMap<>();
    private BigDecimal totalPrice;
    private BigDecimal totalNetWeight;
    private String error="";
    private long pos=10;
    private int lastPageNum=-1;

    /**
     * Считываемпервую страницу и распознавем необходимые данные
     * Invoice:
     * Gross weight:
     * Number of collis:
     * @throws IOException
     */
    public boolean readFirstPage() throws IOException {
        LOGGER.debug("ProcessWagoInvoicePDF.readFirstPage");
        try {
            if (inited) {
                // A4 595x842; 2.83
                Rectangle2D customsSumRect = new Rectangle(365, 693, 70, 15);
                Rectangle2D dated = new Rectangle(500, 693, 70, 15);
                Rectangle2D grossWeightRect = new Rectangle(365, 550, 70, 15);
                Rectangle2D grossWeightRect2 = new Rectangle(365, 560, 70, 15);
                Rectangle2D numCoilsRect = new Rectangle(365, 540, 70, 15);
                Rectangle2D numCoilsRect2 = new Rectangle(365, 550, 70, 15);

                RenderFilter customsSumRectfilter = new RegionTextRenderFilter(customsSumRect);
                RenderFilter grossWeighfilter = new RegionTextRenderFilter(grossWeightRect);
                RenderFilter grossWeighfilter2 = new RegionTextRenderFilter(grossWeightRect2);
                RenderFilter numCoilsfilter = new RegionTextRenderFilter(numCoilsRect);
                RenderFilter numCoilsfilter2 = new RegionTextRenderFilter(numCoilsRect2);
                RenderFilter datedfilter = new RegionTextRenderFilter(dated);

                TextExtractionStrategy customsSumStrategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), customsSumRectfilter);
                String invoice = PdfTextExtractor.getTextFromPage(getReader(), 1, customsSumStrategy).toLowerCase().trim();
                model.setInvoice(invoice);

                TextExtractionStrategy grossWeightStrategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), grossWeighfilter);
                String grossWeight = PdfTextExtractor.getTextFromPage(getReader(), 1, grossWeightStrategy).toLowerCase().trim();
                model.setGrossWeight(totalNetWeight = new BigDecimal(grossWeight.replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "")).stripTrailingZeros());

                TextExtractionStrategy numCoilsStrategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), numCoilsfilter);
                String numCoils = PdfTextExtractor.getTextFromPage(getReader(), 1, numCoilsStrategy).toLowerCase().trim();
                model.setNumCoils(numCoils);

                if(numCoils.isEmpty()||grossWeight.length()<3)
                {
                    TextExtractionStrategy grossWeightStrategy2 = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), grossWeighfilter2);
                    grossWeight = PdfTextExtractor.getTextFromPage(getReader(), 1, grossWeightStrategy2).toLowerCase().trim();
                    model.setGrossWeight(totalNetWeight = new BigDecimal(grossWeight.replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "")).stripTrailingZeros());

                    TextExtractionStrategy numCoilsStrategy2 = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), numCoilsfilter2);
                    numCoils = PdfTextExtractor.getTextFromPage(getReader(), 1, numCoilsStrategy2).toLowerCase().trim();
                    model.setNumCoils(numCoils);
                }

                TextExtractionStrategy datedStrategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), datedfilter);
                String datedStr = PdfTextExtractor.getTextFromPage(getReader(), 1, datedStrategy).toLowerCase().trim();
                if(datedStr!=null&&!datedStr.isEmpty()&&datedStr.trim().length()==10)
                {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    model.setDated(simpleDateFormat.parse(datedStr));
                }
                return true;
            }
        }catch (Exception e)
        {
            error="Ошибка чтения первой страници";
            LOGGER.error("First page reading error");
        }

        return false;
    }

    /**
     * Считывает таблицу позиций Invoice
     * @throws IOException
     */
    public void readTable() throws IOException {
        LOGGER.debug("readTable.readFirstPage");
        // x 5.5-205.5  y 270-120
        Rectangle2D firstPageRect = new Rectangle(15, 76, 382, 424);
        Rectangle2D firstPageRectValue = new Rectangle(500, 76, 60, 424);
        Rectangle2D otherPageRect = new Rectangle(15, 76, 382, 664);
        Rectangle2D otherPageRectValue = new Rectangle(500, 76, 60, 665);

        for(int pn=1;pn<lastPageNum;pn++)
        {
            if(pn==1)
            {
                wholeText.append(getTextFromPageRect(firstPageRect,pn));
                wholePositionValueText.append(getTextFromPageRect(firstPageRectValue,pn));
            }
            else
            {
                wholeText.append("\n");
                wholeText.append(getTextFromPageRect(otherPageRect,pn));
                wholePositionValueText.append(getTextFromPageRect(otherPageRectValue,pn));
            }
        }
    }


    /**
     * ОБработка считаной таблицы позиций Invoice
     */
    public boolean parseTable()
    {
        LOGGER.debug("parseTable");
        DecimalFormat decimalFormat = new DecimalFormat("000000");
        String endOfPosition="Customs tariff no.:";

        positionStrings= Arrays.asList(getWholeText().toString().split("\n"));
        positionVlaueStrings=Arrays.asList(getWholePositionValueText().toString().split("\n"));
        positionVlaueStrings= positionVlaueStrings.stream()
                .map(s->s.replaceAll("\\.",""))
                .map(s -> s.replaceAll(",","."))
                .map(s -> s.replaceAll("[^\\d.]", ""))
                .collect(Collectors.toList());
        for(int i=0;i<positionStrings.size();i++)
        {
            int posStartFound=-1;
            int posEndFound=-1;
            String toFind=decimalFormat.format(pos);

            // Ищем позицию
            posStartFound= findStringWithPosInText(positionStrings,i,toFind,0);
            if(posStartFound!=-1) {
//                System.out.println(positionStrings.get(posStartFound));

                // Ищем конец позиции
                posEndFound = findStringWithText(positionStrings, posStartFound, endOfPosition);
                if (posEndFound != -1) {
//                    System.out.println(positionStrings.get(posEndFound));
                    WagoPosition position= parsePosition(posStartFound,posEndFound,toFind);
                    if (!error.isEmpty()) {
                        return false;
                    }

                    position.setPos(toFind);
                    positions.add(position);
                }
                else
                {
                    LOGGER.error("Cant find record for Position:"+toFind);
                    error="Не могу найти данные для позиции:"+toFind;
                    return false;
                }
            }
            else {
                LOGGER.debug("finished searching last position:"+toFind);
                break;
            }
            pos+=100;
        }
        return true;
    }
    /**
     * Обработка одной позиции Invoice
     * @param posStartFound номер строки начала позиции
     * @param posEndFound номер строки конца позиции
     * @param positionNum номер позиции
     * @return позиция
     */
    public WagoPosition parsePosition(int posStartFound, int posEndFound, String positionNum)
    {
        LOGGER.debug("parseTable.parsePosition:"+positionNum);
        WagoPosition position = new WagoPosition();
        StringBuilder descrStringBuilder = new StringBuilder();
        StringBuilder nameBuilder= new StringBuilder();

        for(int i=(posStartFound+2==posEndFound?posStartFound+1:posStartFound+2);i<posEndFound;i++)
        {
            descrStringBuilder.append(positionStrings.get(i)).append(" ");
        }
//        System.out.println(positionNum);
        String[] descrString =descrStringBuilder.toString().replaceAll(" +"," ").split(" ");
        position.setUnits(descrString[descrString.length-3]);
//        position.setPosValue(new BigDecimal(descrString[descrString.length-1].replaceAll("\\.","").replaceAll(",",".")));

//        position.setQuntity(new BigDecimal(descrString[descrString.length-4].replaceAll("\\.","").replaceAll(",",".")));
        if(StringUtils.isNumeric(descrString[descrString.length-2].replaceAll("\\.","").replaceAll(",",""))) {
            position.setQuntity(new BigDecimal(descrString[descrString.length - 2].replaceAll("\\.", "").replaceAll(",", ".")));
            position.setUnits(descrString[descrString.length-1]);
            for (int i=0; i<descrString.length-2;i++) {
                nameBuilder.append(descrString[i]).append(" ");
            }
        }
        else
        {
            if(StringUtils.isNumeric(descrString[0].replaceAll("\\.","").replaceAll(",","."))) {
                position.setQuntity(new BigDecimal(descrString[0].replaceAll("\\.", "").replaceAll(",", ".")));
                position.setUnits(descrString[1]);
                for (int i=2; i<descrString.length-1;i++) {
                    nameBuilder.append(descrString[i]).append(" ");
                }
            }
            else
            {
                String[] quanUnits=positionStrings.get(posStartFound+1).split(" ");
                position.setQuntity(new BigDecimal(quanUnits[0].replaceAll("\\.", "").replaceAll(",", ".")));
                position.setUnits(quanUnits[1]);
                for (int i=0; i<descrString.length-1;i++) {
                    nameBuilder.append(descrString[i]).append(" ");
                }
            }
        }
        position.setPosValue(new BigDecimal( positionVlaueStrings.get(getPositions().size())));



        position.setName(nameBuilder.toString().trim());

        String tarifString=positionStrings.get(posEndFound).toLowerCase();
        int tarifStartPos=tarifString.indexOf("tariff no.:")+"tariff no.:".length();
        int tarifEndPos=tarifString.indexOf("country of");
        if(tarifEndPos==-1)
            tarifEndPos=tarifString.length()+1;

        if(((tarifStartPos>-1))&&(tarifEndPos>tarifStartPos))
        {
            position.setTarifNo(tarifString.substring(tarifStartPos,tarifEndPos-1).trim().split(" ")[0]);
        }
        else
        {
            LOGGER.error("Cant find tarif for position:"+ positionNum);
            error="Не могу найти тариф для позиции"+ positionNum;
        }

        return position;
    }

    /**
     * Ищем начало сумарной информации
     * @throws IOException
     */
    public boolean findLastPageNum() throws IOException {
        LOGGER.debug("findLastPageNum");
        Rectangle2D lastPageRect = new Rectangle(15, 76, 560, 800);
        if(isInited()) {
            for (int pn = 1; pn <=getReader().getNumberOfPages(); pn++) {
                String lastPage = getTextFromPageRect(lastPageRect, pn);
                if (lastPage.toLowerCase().contains("preferential goods")) {
                    lastPageNum = pn;
                }
            }
        }
        return false;
    }

    public void readLastTable() throws IOException {
        LOGGER.debug("readLastTable");
        Rectangle2D lastPageRectTarif = new Rectangle(220, 76, 60, 630);
        Rectangle2D lastPageRectTarif2 = new Rectangle(220, 76, 60, 660);

        Rectangle2D lastPageRectNetWieght = new Rectangle(360, 76, 82, 630);
        Rectangle2D lastPageRectNetWieght2 = new Rectangle(360, 76, 82, 660);

        Rectangle2D lastPageRectPrice= new Rectangle(480, 76, 80, 630);
        Rectangle2D lastPageRectPrice2 = new Rectangle(480, 76, 80, 660);
        int count=1;
        for (int pn = lastPageNum; pn <=getReader().getNumberOfPages(); pn++)
        {
            if(count==1) {
                finalTableCustomsTarif.append(getTextFromPageRect(lastPageRectTarif, pn));
                finalTableNetWeight.append(getTextFromPageRect(lastPageRectNetWieght, pn));
                finalTablePrice.append(getTextFromPageRect(lastPageRectPrice, pn));
            }
            else {
                finalTableCustomsTarif.append("\n");
                finalTableCustomsTarif.append(getTextFromPageRect(lastPageRectTarif2, pn));
                finalTableNetWeight.append("\n");
                finalTableNetWeight.append(getTextFromPageRect(lastPageRectNetWieght2, pn));
                finalTablePrice.append("\n");
                finalTablePrice.append(getTextFromPageRect(lastPageRectPrice2, pn));
            }

            count++;
        }
    }
    public boolean parseLastPage()
    {
        LOGGER.debug("parseLastPage");
        String[] customsTarifStrings=   finalTableCustomsTarif.toString().split("\n");
        String[] netWeighStrings= Arrays.stream(finalTableNetWeight.toString().split("\n")).map(String::trim)
                .filter(entity -> !entity.isEmpty()).toArray(String[]::new);
        int correction=0;


        String[] priceStrings=   finalTablePrice.toString().split("\n");

        if((customsTarifStrings.length!=netWeighStrings.length-2)&&(customsTarifStrings.length!=netWeighStrings.length-1)) {
            error="Количество записей о тарифах и о весе не совпадают";
            LOGGER.error("Last page parsing error");
            return false;
        }

        try {

            totalNetWeight = new BigDecimal(netWeighStrings[netWeighStrings.length - 1].split(" ")[0].trim().replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", ""));

            totalPrice = new BigDecimal(priceStrings[priceStrings.length - 1].split(" ")[0].trim().replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", ""));

            for (int i = 0; i < customsTarifStrings.length; i++) {
                String tarifNum = customsTarifStrings[i];
                BigDecimal netWeight = new BigDecimal(netWeighStrings[i+correction].replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "")).stripTrailingZeros();
                BigDecimal netWeightSum = new BigDecimal("0");
                if(i>1&&correction==0) {
                    for (int j = 0; j < i; j++) {
                        netWeightSum = netWeightSum.add(new BigDecimal(netWeighStrings[j].replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "")).stripTrailingZeros());
                    }
                    if(netWeightSum.equals(netWeight))
                    {
                        correction=1;
                        netWeight=new BigDecimal(netWeighStrings[i+correction].replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "")).stripTrailingZeros();
                    }
                }

                if (lastpageMap.get(tarifNum) != null) {
                    BigDecimal val = lastpageMap.get(tarifNum);
                    val = val.add(netWeight);
                    lastpageMap.put(tarifNum, val);
                } else {
                    lastpageMap.put(tarifNum, netWeight);
                }
            }
        }catch (Exception e)
        {
            error="ошибка обработки последней страницы";
            LOGGER.error("Last page parsing error");
            return false;
        }
        return true;
    }
    /**
     * Обработка последней страници документа Invoice
     * @throws IOException
     */
//    public boolean parseLastPage() throws IOException {
//        LOGGER.debug("parseLastPage");
//        Rectangle2D lastPageRect = new Rectangle(15, 76, 560, 630);
//        String lastPage= getTextFromPageRect(lastPageRect,getReader().getNumberOfPages());
//        String[] lastPageStrings=lastPage.split("\n");
//
//        try {
//
//            for (int i = 0; i < lastPageStrings.length - 1; i += 2) {
//                String tarifNumberstr = lastPageStrings[i];
//                String weightstr = lastPageStrings[i + 1];
//                if (tarifNumberstr.toLowerCase().contains("subtotal")) {
//                    String[] totalPriceArr = weightstr.split(" ");
//                    totalPrice = new BigDecimal(totalPriceArr[totalPriceArr.length - 1].trim().replaceAll("\\.", "").replaceAll(",", "."));
//                    if (totalPriceArr.length > 1)
//                        totalNetWeight = new BigDecimal(totalPriceArr[0].replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", ""));
//                    else {
//                        totalNetWeight = new BigDecimal(lastPageStrings[i + 2].split(" ")[0].trim().replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", ""));
//                    }
//                    break;
//                }
//                String tarifNumber = tarifNumberstr.split(" ")[1].trim().toLowerCase();
//                String[] weghtSplit = weightstr.split(" ");
//                String weightStr = weghtSplit[weghtSplit.length - 2].replaceAll("\\.", "").replaceAll(",", ".").replaceAll("[^\\d.]", "");
//                BigDecimal weight = new BigDecimal(weightStr).stripTrailingZeros();
//                ;
//                System.out.println(tarifNumber + "-" + weight);
//
//                if (lastpageMap.get(tarifNumber) != null) {
//                    BigDecimal val = lastpageMap.get(tarifNumber);
//                    val = val.add(weight);
//                    lastpageMap.put(tarifNumber, val);
//                } else {
//                    lastpageMap.put(tarifNumber, weight);
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            error="ошибка обработки последней страницы";
//            LOGGER.error("Last page parsing error");
//            return false;
//        }
//        return true;
//    }

    /**
     * Всталяем нетта и брутто в позиции
     */
    public void calcWeight()
    {
        BigDecimal totalTara= getModel().getGrossWeight().subtract(totalNetWeight);
        BigDecimal coof=totalTara.divide(totalNetWeight,RoundingMode.CEILING).stripTrailingZeros();
        BigDecimal allPosBrutto= new BigDecimal("0");

        Collections.sort(getPositions());

//        System.out.println("Position wout netWeight:");
        for (WagoPosition position:getPositions()) {
            if(getLastpageMap().get(position.getTarifNo())!=null)
            {
                BigDecimal posNetWeight=getLastpageMap().get(position.getTarifNo());
                BigDecimal posTara=posNetWeight.multiply(coof).setScale(3,RoundingMode.CEILING);
                position.setNetto(getLastpageMap().get(position.getTarifNo()));
                position.setBrutto(posTara.add(position.getNetto()));
                allPosBrutto=allPosBrutto.add(position.getBrutto());
                getLastpageMap().remove(position.getTarifNo());

            }
//            else
//                System.out.println(position);
        }

        BigDecimal errorBrutto=getModel().getGrossWeight().subtract(allPosBrutto);
        for(int mult=10;mult>1;mult--)
            for (WagoPosition position:getPositions()) {
                if(position.getBrutto()!=null&&position.getBrutto().doubleValue()>(Math.abs(errorBrutto.doubleValue())*mult))
                {
                    position.setBrutto(position.getBrutto().add(errorBrutto));
                    errorBrutto= new BigDecimal("0");
                    break;
                }
            }
    }

    /**
     * провверяем данные на валидность
     * @return
     */
    public boolean check()
    {
        BigDecimal totalPriceSum= new BigDecimal(0);
        BigDecimal totalNetWeighSum= new BigDecimal(0);
        for (WagoPosition position:positions) {
            totalPriceSum=totalPriceSum.add(position.getPosValue());
            if(position.getNetto()!=null)
                totalNetWeighSum=totalNetWeighSum.add(position.getNetto());
//            else
//                System.out.println(position);
        }
        return totalPrice.stripTrailingZeros().equals(totalPriceSum.stripTrailingZeros())&&totalNetWeight.stripTrailingZeros().equals(totalNetWeighSum.stripTrailingZeros());
    }

    /**
     * Полная обработка документа
     * @param pdfFile
     * @return
     * @throws IOException
     */
    public boolean processPDF(InputStream pdfFile) throws IOException {

        if(!init(pdfFile))
            return false;

        if(!readFirstPage())
            return false;
        findLastPageNum();
        readTable();
        if(! parseTable())
            return false;

        readLastTable();
        if(!parseLastPage())
            return false;
        calcWeight();

        return check();
    }

    public static HSSFWorkbook generateExcel(List<ProcessWagoInvoicePDF> invoicePDFS)
    {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();

        String[] tableHeader ={"Код ТНВЭД","Артикул","Наименование товара","Наименование товара, Eng","Кол-во","Ед. измерения",
                "Вес нетто, кг","Вес брутто,кг","Стоимость","Кол-во мест","Код упаковки","№ инвойса"};

        BorderStyle mediumStyle = BorderStyle.MEDIUM;
        BorderStyle thinStyle = BorderStyle.THIN;
        Font font14CalibriBold = wb.createFont();
        font14CalibriBold.setFontName("Calibri");
        font14CalibriBold.setFontHeightInPoints((short)14);

        Font font12CalibriBold = wb.createFont();
        font12CalibriBold.setFontName("Calibri");
        font12CalibriBold.setFontHeightInPoints((short)14);

        Font font11ArialCyrBold = wb.createFont();
        font11ArialCyrBold.setFontName("Arial Cyr");
        font11ArialCyrBold.setFontHeightInPoints((short)11);

        HSSFCellStyle style1 = createWbStyle(mediumStyle,wb);
        style1.setFont(font14CalibriBold);

        HSSFCellStyle style2 = createWbStyle(mediumStyle,wb);
        style2.setFont(font11ArialCyrBold);

        createCell(sheet.createRow(1),1,"Container",false,style1);
        Row headerRow=sheet.createRow(4);
        for(int i=0;i<tableHeader.length;i++)
        {
            createCell(headerRow,i+1,tableHeader[i],false,style1);
        }

        int currRowNum=5;

        for (ProcessWagoInvoicePDF pdf:invoicePDFS) {

            List<WagoPosition> wagoPositions=pdf.getPositions();

//            Collections.sort(wagoPositions);

            for (int i=0;i<wagoPositions.size();i++) {

                WagoPosition position=wagoPositions.get(i);


                Row row = sheet.createRow(currRowNum);

                createCell(row,1,position.getTarifNo(),false,style1);
                createCell(row,2,"",false,style1);
                createCell(row,3,"",false,style1);
                createCell(row,4,position.getName(),false,style1);
                createCell(row,5,position.getQuntity().doubleValue(),true,style1);
                createCell(row,6,position.getUnits(),false,style1);

                if(position.getNetto()!=null) {
                    createCell(row, 7, position.getNetto().doubleValue(), true, style1);
                    createCell(row,8,position.getBrutto().doubleValue(),true,style1);
                }

                else {
                    createCell(row, 7, "", false, style1);
                    createCell(row,8,"",false,style1);
                }


                createCell(row,9,position.getPosValue().doubleValue(),true,style1);
                if(i==0)
                    createCell(row,10,Double.valueOf(pdf.getModel().getNumCoils()),true,style1);
                else
                    createCell(row,10,"",false,style1);

                createCell(row,11,"",false,style1);
                createCell(row,12,pdf.getModel().getInvoice(),false,style1);
                currRowNum++;
            }

        }


        for(int i=1;i<tableHeader.length+1;i++)
        {
            sheet.autoSizeColumn(i);
        }
        return wb;
    }


    public WagoInvoiceModel getModel() {
        return model;
    }

    @Override
    public StringBuilder getWholeText() {
        return wholeText;
    }

    public long getPos() {
        return pos;
    }

    public List<WagoPosition> getPositions() {
        return positions;
    }

    public Map<String, BigDecimal> getLastpageMap() {
        return lastpageMap;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public BigDecimal getTotalNetWeight() {
        return totalNetWeight;
    }

    public String getError() {
        return error;
    }

    public int getLastPageNum() {
        return lastPageNum;
    }

    public StringBuilder getFinalTableCustomsTarif() {
        return finalTableCustomsTarif;
    }

    public StringBuilder getFinalTableNetWeight() {
        return finalTableNetWeight;
    }

    public StringBuilder getFinalTablePrice() {
        return finalTablePrice;
    }

    public StringBuilder getWholePositionValueText() {
        return wholePositionValueText;
    }
}
