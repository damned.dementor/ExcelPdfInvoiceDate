package Ti.DataProcessing.pdf;

import Ti.model.CargoModel;
import Ti.model.MrnModel;
import Ti.model.PDFmodelInvoice;
import Ti.model.PDFmodelMrn;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator.evaluateAllFormulaCells;

/**
 * MrnInvoiceProcess is createdd for checking pdf file and process then in a case of pdf type mrn or invoice
 * and then generates excel file  from mrn and invoice data
 */
public class MrnInvoiceProcess {
    private static Logger LOGGER = Logger.getLogger(MrnInvoiceProcess.class);
    private ArrayList<PDFmodelInvoice> invoices = new ArrayList<>();
    private ArrayList<PDFmodelMrn> mrns = new ArrayList<>();
    private PdfReader reader;

    /**
     * processPDF is using to extract cargo list or mrn list from pdf file
     * @param inputStream file stream
     * @return result
     */
    public boolean processPDF(FileInputStream inputStream) {
        boolean res = false;

        switch (findPDFtype(inputStream)) {
            case "invoice": {
                LOGGER.debug("----------------------------------------------------------------Processing invoice");
                ProcessInvoicePDFfile invoicePDFfile = new ProcessInvoicePDFfile();
                try {
                    invoicePDFfile.setInited(true);
                    invoicePDFfile.setReader(reader);
                    invoicePDFfile.readFirstPage();
                    invoicePDFfile.readText();
                    invoicePDFfile.parseTable();
                    invoicePDFfile.cancatCargoRecords();
                    if (invoicePDFfile.getPdFmodel().checkControlSum()) {
                        invoices.add(invoicePDFfile.getPdFmodel());
                        return true;
                    }
                    LOGGER.error("Control sum check error!");
                    return false;

                } catch (Exception e) {
                    LOGGER.error("Exception during mrn PDF processing"+e.getMessage());
                    return false;
                }
            }
            case "mrn": {
                LOGGER.debug("----------------------------------------------------------------Processing mrn");
                ProcessMrnPDFfile mrnPDFfile = new ProcessMrnPDFfile();
                try {
                    mrnPDFfile.setInited(true);
                    mrnPDFfile.setReader(reader);
                    mrnPDFfile.readFirstPage();
                    mrnPDFfile.readTable();
                    mrnPDFfile.cleantext();
                    mrnPDFfile.processText();
                    mrnPDFfile.parseTable();
                    if (mrnPDFfile.getPdFmodelMrn().checkControlSum()) {
                        mrns.add(mrnPDFfile.getPdFmodelMrn());
                        return true;
                    }
                    LOGGER.error("Control sum check error!");
                    return false;


                } catch (Exception e) {
                    LOGGER.error("Exception during invoice PDF processing");
                    LOGGER.error(e.getMessage());
                    LOGGER.error(e.getCause().getMessage());
                    return false;
                }
            }
        }

        return res;
    }

    /**
     * findPDFtype is using to find out what pdf type file is processing
     * @param inputStream file stream
     * @return pdf type
     */
    public String findPDFtype(FileInputStream inputStream) {
        String result = "";

        try {
            reader = new PdfReader(inputStream);
            TextExtractionStrategy strategy = new MyLocationTextExtractionStrategy();
            String text = PdfTextExtractor.getTextFromPage(reader, 1, strategy).toLowerCase().replaceAll(" +", " ");
            if (text.contains("invoice"))
                return "invoice";
            else
                return "mrn";

        } catch (Exception e) {
            LOGGER.error("Wrong PDF");
            return result;
        }
    }
    private ArrayList<MrnModel> cancatMrnModelLists()
    {
        ArrayList<MrnModel> mrnModels = new ArrayList<>();
        for (PDFmodelMrn mrn:mrns) {
            mrnModels.addAll(mrn.getMrnList());
        }
        return mrnModels;
    }

    /**
     * generateExcel generating excel report
     * @throws IOException file write exception
     */
    public HSSFWorkbook generateExcel() throws IOException {

        //---------------------LOG info-------------------------
        BigDecimal totalBrutto= new BigDecimal("0");
        for (PDFmodelMrn pdFmodelMrn:mrns) {
            totalBrutto=totalBrutto.add(pdFmodelMrn.getMasBrutto());
            LOGGER.debug(pdFmodelMrn);
        }
//        for(PDFmodelInvoice pdFmodelInvoice:invoices)
//        {
//            LOGGER.debug(pdFmodelInvoice);
//        }
        LOGGER.debug("totalBrutto:"+totalBrutto);
        //----------------------------------------------

        ArrayList<MrnModel> mrnModels=cancatMrnModelLists();
        int current_row;
        Cell cell;

        String[] tableHeader ={"Код ТНВЭД","Наименование товара","Наименование товара, Eng","Кол-во","Ед. измерения",
                "Вес нетто, кг","Тара, кг","Вес брутто,кг","Стоимость","Валюта","Инвойс №","места"};
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();

        BorderStyle mediumStyle = BorderStyle.MEDIUM;
        BorderStyle thinStyle = BorderStyle.THIN;
        Font font14CalibriBold = wb.createFont();
        font14CalibriBold.setFontName("Calibri");
        font14CalibriBold.setFontHeightInPoints((short)14);

        Font font12CalibriBold = wb.createFont();
        font12CalibriBold.setFontName("Calibri");
        font12CalibriBold.setFontHeightInPoints((short)14);

        Font font11ArialCyrBold = wb.createFont();
        font11ArialCyrBold.setFontName("Arial Cyr");
        font11ArialCyrBold.setFontHeightInPoints((short)11);

        HSSFCellStyle style1 = createWbStyle(mediumStyle,wb);
        style1.setFont(font14CalibriBold);

        HSSFCellStyle style2 = createWbStyle(mediumStyle,wb);
        style2.setFont(font11ArialCyrBold);

        HSSFCellStyle styleThin = createWbStyle(thinStyle,wb);
        styleThin.setFont(font11ArialCyrBold);

        createCell(sheet.createRow(1),1,"Packing list for Container ",false,style1);
        createCell(sheet.createRow(3),1,"Отправитель Consignor:",false,style2);
        createCell(sheet.createRow(4),1,"Получатель Consignee:",false,style2);
        createCell(sheet.createRow(5),1,"Количество мест:",false,style2);
        sheet.getRow(5).setHeightInPoints((short)11);

        CellRangeAddress rangeAddress = new CellRangeAddress(1,1,1,14);
        createCellRange(rangeAddress,sheet,1,(short)30,mediumStyle);

        rangeAddress=new CellRangeAddress(3,3,2,14);
        createCellRange(rangeAddress,sheet,3,(short)50,mediumStyle);

        rangeAddress=new CellRangeAddress(4,4,2,14);
        createCellRange(rangeAddress,sheet,4,(short)50,mediumStyle);


        current_row=7;
        Row headerRow= sheet.createRow(current_row);
        current_row++;
        for(int i=0;i<tableHeader.length;i++)
        {
            cell=headerRow.createCell(i+1);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(tableHeader[i]);
            cell.setCellStyle(styleThin);
        }
        //----------------------------- filling up table
        for( PDFmodelInvoice invoice:invoices){
            ArrayList<CargoModel> cargoList =invoice.getCargoList();
            while (cargoList.size() > 0) {
            MrnModel searched_mrn=null;
                CargoModel model = cargoList.get(0);
                ArrayList<Integer> indexList = new ArrayList<>();
                for (int n = 0; n < cargoList.size(); n++) {
                    if (model.getCommodityCode().equals(cargoList.get(n).getCommodityCode())) {
                            indexList.add(n);
                    }
                }
                // getting mrn code from Cargo
                String code= model.getCommodityCode().substring(0,model.getCommodityCode().length()-2);
                for (PDFmodelMrn pdFmodelMrn:mrns) {
                    boolean stop=false;

                    for (MrnModel mrnModel:pdFmodelMrn.getMrnList())
                    if(mrnModel.getCommodityCode_2().equals(code)&&mrnModel.getInvoiceNumber().equals(invoice.getDocNumber())&&!mrnModel.isUsed())
                    {
                            searched_mrn = mrnModel;
                            mrnModel.setUsed(true);
//                            pdFmodelMrn.setUsed(true);
                            stop=true;
                            break;
                    }
                    if (stop)
                        break;
                }
                for (int n=0;n<indexList.size();n++)
                {
                    int idx=indexList.get(n);
                    CargoModel cargoModel=cargoList.get(idx);
                    Row row =sheet.createRow(current_row);

                    createCell(row,1,cargoModel.getCommodityCode(),false,styleThin);
                    createCell(row,2,cargoModel.getDescription(),false,styleThin);
                    createCell(row,3,"",false,styleThin);
                    createCell(row,4,cargoModel.getQty().doubleValue(),true,styleThin);
                    createCell(row,5,"шт.",false,styleThin);

                    if(n==0&&searched_mrn!=null)
                    {
                        createCell(row,6,searched_mrn.getNetto().doubleValue(),true,styleThin);
                        rangeAddress = new CellRangeAddress(row.getRowNum(),row.getRowNum()-1+indexList.size(),6,6);
                        createCellRange(rangeAddress,sheet,row.getRowNum(),(short)14,thinStyle);

                        createCell(row,7,searched_mrn.getBrutto().subtract(searched_mrn.getNetto()).doubleValue(),true,styleThin);
                        rangeAddress = new CellRangeAddress(row.getRowNum(),row.getRowNum()-1+indexList.size(),7,7);
                        createCellRange(rangeAddress,sheet,row.getRowNum(),(short)14,thinStyle);

                        createCell(row,8,searched_mrn.getBrutto().doubleValue(),true,styleThin);
                        rangeAddress = new CellRangeAddress(row.getRowNum(),row.getRowNum()-1+indexList.size(),8,8);
                        createCellRange(rangeAddress,sheet,row.getRowNum(),(short)14,thinStyle);
                    }
                    else
                    {
                        createCell(row,6,"",false,styleThin);
                        createCell(row,7,"",false,styleThin);
                        createCell(row,8,"",false,styleThin);
                    }
                    createCell(row,9,cargoModel.getTotalPrice().doubleValue(),true,styleThin);
                    createCell(row,10,cargoModel.getCurrency(),false,styleThin);

                    String pattern = "dd-MM-yyyy";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                    createCell(row,11,invoice.getDocNumber()+"("+simpleDateFormat.format(invoice.getDocDate())+")",false,styleThin);

                    createCell(row,12,"",false,styleThin);
                    current_row++;
                }
                for (int idx=indexList.size()-1;idx>=0;idx--) {
                    cargoList.remove(indexList.get(idx).intValue());
                }
            }
            int total_places=0;
            for (PDFmodelMrn pdFmodelMrn:mrns) {
//                if(pdFmodelMrn.isUsed())
                    total_places+=pdFmodelMrn.getTotalDes();
            }

            createCell(sheet.getRow(5),2,(double)total_places,true,style2);
        }


        Row row =sheet.createRow(current_row);
        createCell(sheet.createRow(current_row),1,"ИТОГО",false,style1);
        rangeAddress = new CellRangeAddress(current_row,current_row,1,7);
        createCellRange(rangeAddress,sheet,current_row,(short)30,mediumStyle);

        current_row++;
        cell=createCell(row,8,"",false,style1);
        cell.setCellFormula("sum(I"+9+":I"+--current_row+")");
        evaluateAllFormulaCells( wb);


        for(int i=1;i<20;i++)
        {
            sheet.autoSizeColumn(i);
        }
        LOGGER.debug("--------UnUsedMrn-------");
        for (PDFmodelMrn pdFmodelMrn:mrns) {
            totalBrutto=totalBrutto.add(pdFmodelMrn.getMasBrutto());
            if(!pdFmodelMrn.isUsed())
//            LOGGER.debug(pdFmodelMrn);

            for (MrnModel mrnModel:pdFmodelMrn.getMrnList()) {
                if(!mrnModel.isUsed())
                    LOGGER.debug(mrnModel);
            }
        }
        return wb;
    }
    private HSSFCellStyle createWbStyle(BorderStyle style, HSSFWorkbook wb)
    {
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(style);
        cellStyle.setBorderRight(style);
        cellStyle.setBorderTop(style);
        cellStyle.setBorderLeft(style);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        return cellStyle;

    }
    private void setRegionStyle(BorderStyle style,CellRangeAddress range,HSSFSheet sheet)
    {
        RegionUtil.setBorderBottom(style, range, sheet);
        RegionUtil.setBorderTop(style, range, sheet);
        RegionUtil.setBorderLeft(style, range, sheet);
        RegionUtil.setBorderRight(style, range, sheet);
    }
    private Cell createCell(Row row,int column, Object data,boolean numeric,HSSFCellStyle style)
    {
        Cell cell = row.createCell(column);
        if(numeric) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue((double)data);
        }
        else {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue((String) data);
        }
        cell.setCellStyle(style);
        return cell;
    }

    private void createCellRange(CellRangeAddress rangeAddress, HSSFSheet sheet,int row,Short heghtInPoints,BorderStyle style)
    {
        if(rangeAddress.getNumberOfCells()>1) {
            sheet.addMergedRegion(rangeAddress);
            sheet.getRow(row).setHeightInPoints(heghtInPoints);
            setRegionStyle(style, rangeAddress, sheet);
        }
    }

    public ArrayList<PDFmodelMrn> getMrns() {
        return mrns;
    }

    public ArrayList<PDFmodelInvoice> getInvoices() {
        return invoices;
    }
}
