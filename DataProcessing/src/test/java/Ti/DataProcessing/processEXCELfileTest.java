package Ti.DataProcessing;

import Ti.DataProcessing.excel.ProcessEXCELfile;
import Ti.model.DataModel;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;


public class processEXCELfileTest {
    //-----------------------test1-------------------------
    @Test
    public void excelReadTest1() throws IOException, ParseException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("1").getPath().substring(1);

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180713_Zollliste_EB901.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String, DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();

        Assert.assertNotNull(stringDataModelMap);
        Assert.assertEquals(32,stringDataModelMap.size());

        Assert.assertNotNull(stringDataModelMap.get("7511095402"));
        Assert.assertNotNull(stringDataModelMap.get("7511095344"));
        Assert.assertNotNull(stringDataModelMap.get("7511095343"));
        Assert.assertNotNull(stringDataModelMap.get("7511095342"));
        Assert.assertNotNull(stringDataModelMap.get("7511095283"));
        Assert.assertNotNull(stringDataModelMap.get("7511095261"));
        Assert.assertNotNull(stringDataModelMap.get("7511095238"));
        Assert.assertNotNull(stringDataModelMap.get("7511095237"));
        Assert.assertNotNull(stringDataModelMap.get("7511095152"));
        Assert.assertNotNull(stringDataModelMap.get("7511095126"));
        Assert.assertNotNull(stringDataModelMap.get("7511095125"));
        Assert.assertNotNull(stringDataModelMap.get("7511095124"));
        Assert.assertNotNull(stringDataModelMap.get("7511095123"));
        Assert.assertNotNull(stringDataModelMap.get("7511095073"));
        Assert.assertNotNull(stringDataModelMap.get("7511095072"));
        Assert.assertNotNull(stringDataModelMap.get("7511095071"));
        Assert.assertNotNull(stringDataModelMap.get("7511095057"));
        Assert.assertNotNull(stringDataModelMap.get("7511095056"));
        Assert.assertNotNull(stringDataModelMap.get("7511095025"));
        Assert.assertNotNull(stringDataModelMap.get("7511095010"));
        Assert.assertNotNull(stringDataModelMap.get("7511094953"));
        Assert.assertNotNull(stringDataModelMap.get("7511094901"));
        Assert.assertNotNull(stringDataModelMap.get("7511094900"));
        Assert.assertNotNull(stringDataModelMap.get("7511094799"));
        Assert.assertNotNull(stringDataModelMap.get("7511094798"));
        Assert.assertNotNull(stringDataModelMap.get("7511094797"));
        Assert.assertNotNull(stringDataModelMap.get("7511094286"));
        Assert.assertNotNull(stringDataModelMap.get("7511093891"));
        Assert.assertNotNull(stringDataModelMap.get("7511093816"));
        Assert.assertNotNull(stringDataModelMap.get("7511092915"));
        Assert.assertNotNull(stringDataModelMap.get("7511089874"));


        File choosenDir = new File(rootDir+File.separator+"1");
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir+File.separator+"1",processEXCELfile);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095025").getDate());

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(1,dateCount);
    }
    @Test
    public void excelReadTest2() throws IOException, ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("1").getPath();

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180713_Zollliste_EB901.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();


        File choosenDir = new File(rootDir+File.separator+"2");
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir+File.separator+"2",processEXCELfile);


        Assert.assertEquals(simpleDateFormat.parse("13.07.2018"),stringDataModelMap.get("7511095402").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095344").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095343").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095342").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095283").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095261").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095238").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095237").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095152").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095126").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095125").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095124").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095123").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095073").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095072").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095071").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095057").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511095056").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511095010").getDate());
        Assert.assertEquals(simpleDateFormat.parse("10.07.2018"),stringDataModelMap.get("7511094953").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511094901").getDate());
        Assert.assertEquals(simpleDateFormat.parse("11.07.2018"),stringDataModelMap.get("7511094900").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511094799").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511094797").getDate());
        Assert.assertEquals(simpleDateFormat.parse("13.07.2018"),stringDataModelMap.get("7511094286").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511093891").getDate());
        Assert.assertEquals(simpleDateFormat.parse("12.07.2018"),stringDataModelMap.get("7511093816").getDate());
        Assert.assertEquals(simpleDateFormat.parse("13.07.2018"),stringDataModelMap.get("7511092915").getDate());

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(28,dateCount);
    }
    @Test
    public void excelReadTest3() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("1").getPath();

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180713_Zollliste_EB901.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();


        File choosenDir = new File(rootDir+File.separator+"1+2");
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir+File.separator+"1+2",processEXCELfile);

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(29,dateCount);
    }
    //-----------------------test2-------------------------
    @Test
    public void excelReadTest1_2() throws IOException, ParseException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("2").getPath();

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180613_Zollliste_EB888.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();

        Assert.assertNotNull(stringDataModelMap);
        Assert.assertEquals(22,stringDataModelMap.size());

        Assert.assertNotNull(stringDataModelMap.get("7511092446"));
        Assert.assertNotNull(stringDataModelMap.get("7511092327"));
        Assert.assertNotNull(stringDataModelMap.get("7511092322"));
        Assert.assertNotNull(stringDataModelMap.get("7511092251"));
        Assert.assertNotNull(stringDataModelMap.get("7511092247"));
        Assert.assertNotNull(stringDataModelMap.get("7511092119"));
        Assert.assertNotNull(stringDataModelMap.get("7511092072"));
        Assert.assertNotNull(stringDataModelMap.get("7511092048"));
        Assert.assertNotNull(stringDataModelMap.get("7511092047"));
        Assert.assertNotNull(stringDataModelMap.get("7511092003"));
        Assert.assertNotNull(stringDataModelMap.get("7511091966"));
        Assert.assertNotNull(stringDataModelMap.get("7511091965"));
        Assert.assertNotNull(stringDataModelMap.get("7511091964"));
        Assert.assertNotNull(stringDataModelMap.get("7511091961"));
        Assert.assertNotNull(stringDataModelMap.get("7511091960"));
        Assert.assertNotNull(stringDataModelMap.get("7511091865"));
        Assert.assertNotNull(stringDataModelMap.get("7511091789"));
        Assert.assertNotNull(stringDataModelMap.get("7511090953"));
        Assert.assertNotNull(stringDataModelMap.get("7511090489"));
        Assert.assertNotNull(stringDataModelMap.get("7511089540"));
        Assert.assertNotNull(stringDataModelMap.get("7511088777"));

        File choosenDir = new File(rootDir);
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir,processEXCELfile);

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(21,dateCount);
    }
    //-----------------------test3-------------------------
    @Test
    public void excelReadTest1_3() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("3").getPath().substring(1);

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180530_Zollliste_EB882.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();

        Assert.assertNotNull(stringDataModelMap);
        Assert.assertEquals(21,stringDataModelMap.size());

        Assert.assertNotNull(stringDataModelMap.get("7511091022"));
        Assert.assertNotNull(stringDataModelMap.get("7511090962"));
        Assert.assertNotNull(stringDataModelMap.get("7511090883"));
        Assert.assertNotNull(stringDataModelMap.get("7511090745"));
        Assert.assertNotNull(stringDataModelMap.get("7511090725"));
        Assert.assertNotNull(stringDataModelMap.get("7511090683"));
        Assert.assertNotNull(stringDataModelMap.get("7511090623"));
        Assert.assertNotNull(stringDataModelMap.get("7511090609"));
        Assert.assertNotNull(stringDataModelMap.get("7511090478"));
        Assert.assertNotNull(stringDataModelMap.get("7511090459"));
        Assert.assertNotNull(stringDataModelMap.get("7511090309"));
        Assert.assertNotNull(stringDataModelMap.get("7511090225"));
        Assert.assertNotNull(stringDataModelMap.get("7511090189"));
        Assert.assertNotNull(stringDataModelMap.get("7511090085"));
        Assert.assertNotNull(stringDataModelMap.get("7511089884"));
        Assert.assertNotNull(stringDataModelMap.get("7511089334"));
        Assert.assertNotNull(stringDataModelMap.get("7511088649"));
        Assert.assertNotNull(stringDataModelMap.get("7511088081"));
        Assert.assertNotNull(stringDataModelMap.get("7511088080"));
        Assert.assertNotNull(stringDataModelMap.get("7511088079"));

        File choosenDir = new File(rootDir);
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir,processEXCELfile);

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(20,dateCount);
        processEXCELfile.saveEXCEL();
    }
    @Test
    public void excelReadTest1_33() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("1").getPath().substring(1);

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_200821_Zollliste_EB1231.XLSX");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();
    }
    //-----------------------test4-------------------------
    @Test
    public void excelReadTest1_4() throws IOException, ParseException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("4").getPath().substring(1);

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_180316_Zollliste_EB851.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();

        Assert.assertNotNull(stringDataModelMap);
        Assert.assertEquals(35,stringDataModelMap.size());

        File choosenDir = new File(rootDir);
        String[] Flist = choosenDir.list();

        fileListProcess(Flist,rootDir,processEXCELfile);

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(34,dateCount);
        processEXCELfile.saveEXCEL();
    }
    //-----------------------test5-------------------------
    @Test
    public void excelReadTest1_5() throws IOException, ParseException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("5").getPath().substring(1);

        ProcessEXCELfile processEXCELfile = new ProcessEXCELfile(rootDir+File.separator+"LIST_171229_Zollliste_EB817.xlsx");

        Assert.assertTrue(processEXCELfile.init());
        Map<String,DataModel> stringDataModelMap=processEXCELfile.getStringdataModelMap();

        Assert.assertNotNull(stringDataModelMap);
        Assert.assertEquals(32,stringDataModelMap.size());

        File choosenDir = new File(rootDir);
        String[] Flist = choosenDir.list();
        if(Flist!=null)
        fileListProcess(Flist,rootDir,processEXCELfile);

        int dateCount=0;
        for (String S: stringDataModelMap.keySet()) {
            if(stringDataModelMap.get(S).getDate()!=null)
                dateCount++;
        }
        Assert.assertEquals(31,dateCount);
        processEXCELfile.saveEXCEL();
    }
    //-----------------------------------
//    @Test
//    public void test() throws IOException {
//        FileInputStream pdfFile = new FileInputStream(new File("D:/1.pdf"));
//        PdfReader reader = new PdfReader(pdfFile);
//        Rectangle rect=reader.getCropBox(1);
//    }
    //------------------- service method
    private void fileListProcess(String[] Flist, String rootDir, ProcessEXCELfile processEXCELfile) throws IOException {

        for (String S : Flist) {
            int index = S.lastIndexOf('.');
            if (index != -1) {
                String ext = S.substring(index);
                if (ext.equals(".pdf")) {
                    FileInputStream pdfFile = new FileInputStream(new File(rootDir+File.separator+S));
                    processEXCELfile.processPDF(pdfFile);
                }
            }
        }
    }

    @Test
    public void test2()
    {

        String str="414.623,42";
        System.out.println(str.replaceAll("\\.","").replaceAll(",","."));
        System.out.println(new BigDecimal(str.replaceAll("\\.","").replaceAll(",",".")));
        System.out.println(Long.parseLong("100"));
    }
}
