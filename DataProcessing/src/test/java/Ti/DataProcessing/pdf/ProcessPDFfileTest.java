package Ti.DataProcessing.pdf;

import Ti.model.CargoModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ProcessPDFfileTest {

    private static String FILE1="/pdf/invoice_type/TCNU5169663_26-03-2019-155754-17.PDF";
    private static String FILE2= "/pdf/invoice_type/INVOICE 9142190025.pdf";
    private static String FILE3= "/pdf/invoice/inv 9142159628.pdf";
    private static String FILE4= "/pdf/invoice/9142159523 INVOICE--CS00604509.pdf";

    private ProcessInvoicePDFfile processPDFfile;
    private DateFormat dfEn;

    @Before
    public void testSetUp()
    {
        processPDFfile = new ProcessInvoicePDFfile();
        dfEn = new SimpleDateFormat(
                "dd MMM yyyy", Locale.ENGLISH);
    }

    @Test
    public void initTest1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath()));
        Assert.assertTrue(processPDFfile.isInited());
    }

    @Test
    public void initTest2() throws FileNotFoundException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        Assert.assertTrue(processPDFfile.isInited());
    }

    @Test
    public void readFirstPage1() throws IOException, ParseException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath()));
        processPDFfile.readFirstPage();
        Assert.assertNotNull(processPDFfile.getPdFmodel().getDocNumber());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getDocDate());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getTotalAmount());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getCurency());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getTotalNetAmount());

        Assert.assertEquals("9142176535",processPDFfile.getPdFmodel().getDocNumber());
        Assert.assertEquals(dfEn.parse("14 mar 2019"),processPDFfile.getPdFmodel().getDocDate());
        Assert.assertEquals(new BigDecimal("414623.42"),processPDFfile.getPdFmodel().getTotalAmount());
        Assert.assertEquals(new BigDecimal("403271.31"),processPDFfile.getPdFmodel().getTotalNetAmount());
        Assert.assertEquals("cny",processPDFfile.getPdFmodel().getCurency());
    }

    @Test
    public void readFirstPage2() throws IOException, ParseException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertNotNull(processPDFfile.getPdFmodel().getDocNumber());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getDocDate());
        Assert.assertNull(processPDFfile.getPdFmodel().getTotalAmount());
        Assert.assertNull(processPDFfile.getPdFmodel().getCurency());
        Assert.assertNull(processPDFfile.getPdFmodel().getTotalNetAmount());

        Assert.assertEquals("9142190025",processPDFfile.getPdFmodel().getDocNumber());
        Assert.assertEquals(dfEn.parse("20 mar 2019"),processPDFfile.getPdFmodel().getDocDate());
    }
    @Test
    public void setParseType1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath()));
        processPDFfile.readFirstPage();

        Assert.assertNotEquals( -1,processPDFfile.getParseType());
        Assert.assertEquals( 1,processPDFfile.getParseType());
    }
    @Test
    public void setParseType2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertNotEquals( -1,processPDFfile.getParseType());
        Assert.assertEquals( 2,processPDFfile.getParseType());
    }

    @Test
    public void readText1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath()));
        processPDFfile.readFirstPage();
        processPDFfile.readText();

        Assert.assertNotNull(processPDFfile.getWholeText());
        Assert.assertFalse(processPDFfile.getWholeText().toString().isEmpty());
    }

    @Test
    public void readText2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();

        Assert.assertNotNull(processPDFfile.getWholeText());
        Assert.assertNotNull(processPDFfile.getWholeText());
        Assert.assertFalse(processPDFfile.getWholeText().toString().isEmpty());

        Assert.assertNotNull(processPDFfile.getPdFmodel().getTotalAmount());
        Assert.assertNotNull(processPDFfile.getPdFmodel().getCurency());

        Assert.assertEquals(new BigDecimal("1370843.73"),processPDFfile.getPdFmodel().getTotalAmount());
        Assert.assertEquals(new BigDecimal("1326557.61"),processPDFfile.getPdFmodel().getTotalNetAmount());
        Assert.assertEquals("cny",processPDFfile.getPdFmodel().getCurency());
    }

    @Test
    public void parseTable1() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath()));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();

        Assert.assertTrue(processPDFfile.getPdFmodel().getCargoList().size()>0);
        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            Assert.assertNotNull(model.getTotalPrice());
            Assert.assertNotNull(model.getQty());
            amount=amount.add(model.getTotalPrice());

            Assert.assertTrue(!model.getCommodityCode().isEmpty());
            Assert.assertTrue(!model.getCurrency().isEmpty());
            Assert.assertTrue(!model.getDescription().isEmpty());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
    }
    @Test
    public void parseTable2() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();

        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            amount=amount.add(model.getTotalPrice());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
    }

    @Test
    public void parseTable3() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE4).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();

        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            amount=amount.add(model.getTotalPrice());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
    }



    @Test
    public void cancatCargoRecordsTest1() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();
        processPDFfile.cancatCargoRecords();

        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            amount=amount.add(model.getTotalPrice());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
    }
    @Test
    public void cancatCargoRecordsTest2() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();
        processPDFfile.cancatCargoRecords();

        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            amount=amount.add(model.getTotalPrice());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
    }
    @Test
    public void cancatCargoRecordsTest3() throws IOException {
        BigDecimal amount= new BigDecimal(0);
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readText();
        processPDFfile.parseTable();
        processPDFfile.cancatCargoRecords();

        for (CargoModel model:processPDFfile.getPdFmodel().getCargoList()) {
            amount=amount.add(model.getTotalPrice());
        }
        Assert.assertEquals(processPDFfile.getPdFmodel().getTotalNetAmount(),amount);
        Assert.assertEquals(processPDFfile.getPdFmodel().getCargoList().get(1).getQty(),new Long("8000"));
        Assert.assertEquals(processPDFfile.getPdFmodel().getCargoList().get(0).getQty(),new Long("4800"));
    }


    @Test
    public void generalTestWholeMass() throws IOException {
        BigDecimal amount;
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/invoice").getPath().substring(1);
        File fileDir= new File(getClass().getClassLoader().getResource("pdf/invoice").getPath().substring(1));
        String[] list=fileDir.list();
        for (String file:list) {

            ProcessInvoicePDFfile pdFfile = new ProcessInvoicePDFfile();
            pdFfile.init(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
            pdFfile.readFirstPage();
            pdFfile.readText();
            pdFfile.parseTable();

            Assert.assertTrue(pdFfile.getPdFmodel().checkControlSum());
            Assert.assertTrue(pdFfile.getPdFmodel().getCargoList().size()>0);
            amount= new BigDecimal(0);
            for (CargoModel model:pdFfile.getPdFmodel().getCargoList()) {
                Assert.assertNotNull(model.getTotalPrice());
                Assert.assertNotNull(model.getQty());
                Assert.assertTrue(!model.getCommodityCode().isEmpty());
                Assert.assertTrue(!model.getCurrency().isEmpty());
                Assert.assertTrue(!model.getDescription().isEmpty());
                amount=amount.add(model.getTotalPrice());
            }
            Assert.assertEquals(pdFfile.getPdFmodel().getTotalNetAmount(),amount);

            amount= new BigDecimal(0);
            pdFfile.cancatCargoRecords();
            for (CargoModel model:pdFfile.getPdFmodel().getCargoList()) {
                amount=amount.add(model.getTotalPrice());
            }
            Assert.assertEquals(pdFfile.getPdFmodel().getTotalNetAmount(),amount);
            System.out.println(file);
        }
    }

}
