package Ti.DataProcessing.pdf;

import Ti.model.MrnModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;

public class ProcessMrnPDFfileTest {

    private static String FILE1="/pdf/mrn/TCNU5169663_26-03-2019-155754-9.PDF";
    private static String FILE2="/pdf/mrn/CV00327865 DSE1902312646.pdf";
    private static String FILE3="/pdf/mrn_error/TCNU2672099_11-04-2019-130601-12.PDF";
    private static String FILE4="/pdf/SCHNEIDERmrn/CS00814372-Douane1.PDF";
    private static String FILE5="/pdf/SCHNEIDERmrn/CX00071568-Douane1.PDF";
    private static String FILE6="/pdf/SCHNEIDERmrn2/1.PDF";

    private ProcessMrnPDFfile processPDFfile;

    @Before
    public void setUp()
    {
        processPDFfile= new ProcessMrnPDFfile();
    }

    @Test
    public void readFirstPage1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().getTotalDes()!=0);
        Assert.assertNotNull(processPDFfile.getPdFmodelMrn().getMasBrutto());

        Assert.assertEquals(20,processPDFfile.getPdFmodelMrn().getTotalDes());
        Assert.assertEquals(new BigDecimal("1481.894"),processPDFfile.getPdFmodelMrn().getMasBrutto());

    }

    @Test
    public void readFirstPage2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().getTotalDes()!=0);
        Assert.assertNotNull(processPDFfile.getPdFmodelMrn().getMasBrutto());

        Assert.assertEquals(45,processPDFfile.getPdFmodelMrn().getTotalDes());
        Assert.assertEquals(new BigDecimal("4817.591"),processPDFfile.getPdFmodelMrn().getMasBrutto());
    }

    @Test
    public void readTableTest1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();

        Assert.assertTrue(!processPDFfile.wholeText.toString().isEmpty());
    }

    @Test
    public void readTableTest2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();

        Assert.assertTrue(!processPDFfile.wholeText.toString().isEmpty());
    }
    @Test
    public void processTexttest1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();

        Assert.assertTrue(!processPDFfile.wholeText.toString().isEmpty());
    }
    @Test
    public void parseTabletest1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.parseTable();
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);

    }
    @Test
    public void parseTabletest2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.parseTable();
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);
    }

    @Test
    public void parseTabletestError() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.parseTable();
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);
    }
    @Test
    public void cancatMRNRecordstest1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.parseTable();
        processPDFfile.cancatMRNRecords();
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);

    }


    @Test
    public void generalTestWholeMassSeparationQuality() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/mrn").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            ProcessMrnPDFfile pdFfile = new ProcessMrnPDFfile();
            pdFfile.init(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
            pdFfile.readFirstPage();
            pdFfile.readTable();
            pdFfile.cleantext();
            pdFfile.processText();
            processPDFfile.cancatMRNRecords();
            int position=1;
            for (String s:pdFfile.getPositions()) {
                Assert.assertEquals(Integer.toString(position),s.split("\n",2)[0]);
                position++;
            }
            pdFfile.parseTable();
            Assert.assertTrue(pdFfile.getPdFmodelMrn().checkControlSum());
            BigDecimal brutto= new BigDecimal(0);
            for (MrnModel model:pdFfile.getPdFmodelMrn().getMrnList()) {
                brutto=brutto.add(model.getBrutto());
            }
            Assert.assertEquals(pdFfile.getPdFmodelMrn().getMasBrutto(),brutto);
        }
    }
    @Test
    public void SCHNEIDERerrorMRN1() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE4).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().getTotalDes()!=0);
        Assert.assertNotNull(processPDFfile.getPdFmodelMrn().getMasBrutto());

        Assert.assertEquals(9,processPDFfile.getPdFmodelMrn().getTotalDes());
        Assert.assertEquals(new BigDecimal("980.272"),processPDFfile.getPdFmodelMrn().getMasBrutto());

        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.cancatMRNRecords();

        int position=1;
        for (String s:processPDFfile.getPositions()) {
            Assert.assertEquals(Integer.toString(position),s.split("\n",2)[0]);
            position++;
        }
        processPDFfile.parseTable();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().checkControlSum());
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);
    }
    @Test
    public void SCHNEIDERerrorMRN2() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE5).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().getTotalDes()!=0);
        Assert.assertNotNull(processPDFfile.getPdFmodelMrn().getMasBrutto());

        Assert.assertEquals(10,processPDFfile.getPdFmodelMrn().getTotalDes());
        Assert.assertEquals(new BigDecimal("1555.996"),processPDFfile.getPdFmodelMrn().getMasBrutto());

        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.cancatMRNRecords();

        int position=1;
        for (String s:processPDFfile.getPositions()) {
            Assert.assertEquals(Integer.toString(position),s.split("\n",2)[0]);
            position++;
        }
        processPDFfile.parseTable();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().checkControlSum());
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);
    }
    @Test
    public void SCHNEIDERerrorMRN3() throws IOException {
        processPDFfile.init(new FileInputStream(getClass().getResource(FILE6).getPath().replaceAll("%20"," ")));
        processPDFfile.readFirstPage();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().getTotalDes()!=0);
        Assert.assertNotNull(processPDFfile.getPdFmodelMrn().getMasBrutto());

        Assert.assertEquals(33,processPDFfile.getPdFmodelMrn().getTotalDes());
        Assert.assertEquals(new BigDecimal("2748.146"),processPDFfile.getPdFmodelMrn().getMasBrutto());

        processPDFfile.readTable();
        processPDFfile.cleantext();
        processPDFfile.processText();
        processPDFfile.cancatMRNRecords();

        int position=1;
        for (String s:processPDFfile.getPositions()) {
            Assert.assertEquals(Integer.toString(position),s.split("\n",2)[0]);
            position++;
        }
        processPDFfile.parseTable();
        Assert.assertTrue(processPDFfile.getPdFmodelMrn().checkControlSum());
        BigDecimal brutto= new BigDecimal(0);
        for (MrnModel model:processPDFfile.getPdFmodelMrn().getMrnList()) {
            brutto=brutto.add(model.getBrutto());
        }
        Assert.assertEquals(processPDFfile.getPdFmodelMrn().getMasBrutto(),brutto);
    }
}
