package Ti.DataProcessing.pdf.wago;

import Ti.model.Wago.WagoPosition;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;

public class ProcessWagoInvoicePDFTest {

    private static String FILE1="/pdf/Wago/CN_310328854_RG.pdf";
    private static String FILE2="/pdf/Wago/CN_310328859_RG.pdf";
    private static String FILE3="/pdf/Wago/CN_310332206_RG.pdf";
    private static String FILE4="/pdf/Wago/2/CN_310328761_RG.pdf";
    private static String FILE5="/pdf/Wago/2/CN_310332198_RG.pdf";
    private static String FILE6="/pdf/Wago/1/CN_310334814_RG.pdf";
    private static String FILE7="/pdf/Wago/3/CN_310348875_RG.pdf";
    private static String FILE8="/pdf/Wago/4/CN_310951431_RG.pdf";
    private static String FILE9= "/pdf/Wago/5/Invoice_9075608HNKU5132696(1).pdf";
    private static String FILE10= "/pdf/Wago/5/Invoice_9075610HNKU5132696(1).pdf";
    private static String FILE11= "/pdf/Wago/6/Invoice1.pdf";
    private static String FILE12= "/pdf/Wago/6/Invoice2.pdf";
    private static String FILE13= "/pdf/Wago/7/CN_313308180_RG.pdf";
    private static String FILE14= "/pdf/Wago/8/HNKU6042581_S2100150878_Invoice.pdf";
    private static String FILE15= "/pdf/Wago/111.PDF";

    private ProcessWagoInvoicePDF processWagoInvoicePDF;

    @Before
    public void testSetUp()
    {
        processWagoInvoicePDF = new ProcessWagoInvoicePDF();
    }
    @Test
    public void fulltest1() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.readFirstPage();
        processWagoInvoicePDF.findLastPageNum();
        processWagoInvoicePDF.readTable();
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9070796");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("414.307"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"2");
        processWagoInvoicePDF.readTable();
        System.out.println(processWagoInvoicePDF.getWholeText().toString());
        processWagoInvoicePDF.parseTable();
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {

            System.out.println(position);
        }

        processWagoInvoicePDF.readLastTable();
        processWagoInvoicePDF.parseLastPage();
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().size(),4);
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("73202081"));
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("85369010"));
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("74199990"));
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("73209090"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("73202081"),new BigDecimal("39.6"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("85369010"),new BigDecimal("33"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("74199990"),new BigDecimal("64.452"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("73209090"),new BigDecimal("159"));
        processWagoInvoicePDF.calcWeight();
        Assert.assertTrue(processWagoInvoicePDF.check());
        Collections.sort(processWagoInvoicePDF.getPositions());

    }
    @Test
    public void fulltest2() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9070799");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("10066.468"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"20");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
    @Test
    public void fulltest3() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.readFirstPage();
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9070797");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("417.888"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"1");
        processWagoInvoicePDF.findLastPageNum();
        processWagoInvoicePDF.readTable();
        System.out.println(processWagoInvoicePDF.getWholeText().toString());
        processWagoInvoicePDF.parseTable();
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {

            System.out.println(position);
        }
        processWagoInvoicePDF.readLastTable();
        processWagoInvoicePDF.parseLastPage();
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().size(),2);
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("85414010"));
        Assert.assertNotNull(processWagoInvoicePDF.getLastpageMap().get("73209090"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("85414010"),new BigDecimal("1.515"));
        Assert.assertEquals(processWagoInvoicePDF.getLastpageMap().get("73209090"),new BigDecimal("333.36"));
        processWagoInvoicePDF.calcWeight();
        Assert.assertTrue(processWagoInvoicePDF.check());
        Collections.sort(processWagoInvoicePDF.getPositions());

    }

    @Test
    public void fulltest4() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE4).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9070673");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("3552.334"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"22");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
    @Test
    public void fulltest5() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE5).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9070755");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("1592.033"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"11");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
    @Test
    public void fulltest6() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE6).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9071014");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("5967.209"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"41");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }

    @Test
    public void fulltest7() throws IOException {
        Assert.assertTrue( processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE7).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9071272");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("139.181"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"1");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }

    @Test
    public void test() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," "))));
        processWagoInvoicePDF= new ProcessWagoInvoicePDF();
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," "))));
        processWagoInvoicePDF= new ProcessWagoInvoicePDF();
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," "))));

    }

    @Test
    public void findLastPageTest1() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE1).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(2,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }

    @Test
    public void findLastPageTest2() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE2).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(9,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }

    @Test
    public void findLastPageTest3() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE3).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(2,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }

    @Test
    public void findLastPageTest4() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE4).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(17,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }
    @Test
    public void findLastPageTest5() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE5).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(3,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }
    @Test
    public void findLastPageTest6() throws IOException {
        processWagoInvoicePDF.init(new FileInputStream(getClass().getResource(FILE6).getPath().replaceAll("%20"," ")));
        processWagoInvoicePDF.findLastPageNum();
        Assert.assertEquals(32,processWagoInvoicePDF.getLastPageNum());
        processWagoInvoicePDF.readLastTable();
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTableNetWeight().toString().split("\n").length - 2));
        Assert.assertEquals(processWagoInvoicePDF.getFinalTableCustomsTarif().toString().split("\n").length, (processWagoInvoicePDF.getFinalTablePrice().toString().split("\n").length - 2));
        System.out.println(processWagoInvoicePDF.getFinalTableCustomsTarif().toString());
        System.out.println(processWagoInvoicePDF.getFinalTableNetWeight().toString());
        System.out.println(processWagoInvoicePDF.getFinalTablePrice().toString());
    }
    @Test
    public void testPDFerror() throws IOException {
        Assert.assertTrue( processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE8).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9074091");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("5756.888"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"16");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
    @Test
    public void testPDFerror2() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE9).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9075608");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("8613.039"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"24");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("m"));
        }
    }
    @Test
    public void testPDFerror3() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE10).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9075610");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("164.543"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"1");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc"));
        }
    }
    @Test
    public void fulltest12() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE12).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9077763");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("50.163"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"1");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }

    @Test
    public void fulltest13() throws IOException {
        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE13).getPath().replaceAll("%20"," "))));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9085701");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("3721.042"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"37");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
//    @Test
//    public void fulltest14() throws IOException {
//        Assert.assertTrue(processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE14).getPath().replaceAll("%20"," "))));
//        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"70130001");
//        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("8766.906"));
//        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"24");
//        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
//            Assert.assertNotNull(position.getTarifNo());
//            Assert.assertNotNull(position.getQuntity());
//            Assert.assertNotNull(position.getName());
//            Assert.assertNotNull(position.getQuntity());
//            Assert.assertNotNull(position.getUnits());
//            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
//        }
//    }
    @Test
    public void fulltest15() throws IOException {
        processWagoInvoicePDF.processPDF(new FileInputStream(getClass().getResource(FILE15).getPath().replaceAll("%20"," ")));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getInvoice(),"9101377");
        Assert.assertEquals(processWagoInvoicePDF.getModel().getGrossWeight(),new BigDecimal("7373.636"));
        Assert.assertEquals(processWagoInvoicePDF.getModel().getNumCoils(),"24");
        for (WagoPosition position:processWagoInvoicePDF.getPositions()) {
            Assert.assertNotNull(position.getTarifNo());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getName());
            Assert.assertNotNull(position.getQuntity());
            Assert.assertNotNull(position.getUnits());
//            Assert.assertTrue(position.getUnits().equals("pc")||position.getUnits().equals("kg"));
        }
    }
}
