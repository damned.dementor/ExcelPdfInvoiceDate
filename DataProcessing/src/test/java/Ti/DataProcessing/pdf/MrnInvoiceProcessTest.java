package Ti.DataProcessing.pdf;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class MrnInvoiceProcessTest {
    private static Logger LOGGER = Logger.getLogger(MrnInvoiceProcessTest.class);
    private static String PAIR1_MRN="/pdf/pairs/1/CS00611383.pdf";
    private static String MRN="/pdf/mrn/CV00922199-Douane1.PDF";
    private static String PAIR1_INVOICE="/pdf/pairs/1/CS00611383 - FACT RAIL 9142192894.pdf";

    private MrnInvoiceProcess process;


    @Before
    public void testSetUp()
    {
        process= new MrnInvoiceProcess();
    }


    @Test
    public void findPDFtypeTest1() throws FileNotFoundException {
        Assert.assertEquals("mrn",process.findPDFtype(new FileInputStream(getClass().getResource(PAIR1_MRN).getPath().replaceAll("%20"," "))));
    }

    @Test
    public void findPDFtypeTest3() throws FileNotFoundException {
        String type=process.findPDFtype(new FileInputStream(getClass().getResource(PAIR1_MRN).getPath().replaceAll("%20"," ")));

        Assert.assertEquals("mrn",type);
    }

    @Test
    public void findPDFtypeTest2() throws FileNotFoundException {
        Assert.assertEquals("invoice",process.findPDFtype(new FileInputStream(getClass().getResource(PAIR1_INVOICE).getPath().replaceAll("%20"," "))));
    }

    @Test
    public void findPDFtypeTestWholeMassMRN() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/mrn").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            System.out.println(file);
            Assert.assertEquals("mrn",process.findPDFtype(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," "))));
        }
    }
    @Test
    public void findPDFtypeTestWholeMassInvoice() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/invoice").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            Assert.assertEquals("invoice",process.findPDFtype(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," "))));
        }
    }

    @Test
    public void processPDFTestWholeMassMRN_INVOICE() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/mrn").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        int mrn_count=list.length;
        for (String file:list) {
           process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }

        rootDir = classLoader.getResource("pdf/invoice").getPath().substring(1);
        fileDir= new File(rootDir);
        list=fileDir.list();
        int invoice_count=list.length;
        for (String file:list) {
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }
        Assert.assertEquals(mrn_count,process.getMrns().size());
        Assert.assertEquals(invoice_count,process.getInvoices().size());
    }

    @Test
    public void generateExcelPair1Test() throws IOException {
        process.processPDF(new FileInputStream(getClass().getResource(PAIR1_MRN).getPath().replaceAll("%20"," ")));
        process.processPDF(new FileInputStream(getClass().getResource(PAIR1_INVOICE).getPath().replaceAll("%20"," ")));
        process.generateExcel();
    }

    @Test
    public void processPDFTestWholeMassMRN_INVOICEexcelGeneration() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/mrn").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        int mrn_count=list.length;
        for (String file:list) {
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }

        rootDir = classLoader.getResource("pdf/invoice").getPath().substring(1);
        fileDir= new File(rootDir);
        list=fileDir.list();
        int invoice_count=list.length;
        for (String file:list) {
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }
        FileOutputStream out = new FileOutputStream(classLoader.getResource("pdf").getPath().substring(1)+File.separator+"out.xls");
        process.generateExcel().write(out);
        out.close();
    }
    @Test
    public void errorTest() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/error").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            LOGGER.debug("--------Precess file:"+file);
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }

        FileOutputStream outputStream =new FileOutputStream(rootDir+File.separator+"out.xls");
        process.generateExcel().write(outputStream);
        outputStream.close();
    }
    @Test
    public void errorTest2() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/error2").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            if(file.substring(file.lastIndexOf(".")).toLowerCase().equals(".pdf")) {
                LOGGER.debug("--------Precess file:" + file);
                process.processPDF(new FileInputStream(rootDir + File.separator + file.replaceAll("%20", " ")));
            }
        }

        FileOutputStream outputStream =new FileOutputStream(rootDir+File.separator+"out.xls");
        process.generateExcel().write(outputStream);
        outputStream.close();
    }
    @Test
    public void errorTest3() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/error3").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            if(file.substring(file.lastIndexOf(".")).toLowerCase().equals(".pdf")) {
                LOGGER.debug("--------Precess file:" + file);
                process.processPDF(new FileInputStream(rootDir + File.separator + file.replaceAll("%20", " ")));
            }
        }

        FileOutputStream outputStream =new FileOutputStream(rootDir+File.separator+"out.xls");
        process.generateExcel().write(outputStream);
        outputStream.close();
    }
    @Test
    public void errorTest4() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/error4").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        for (String file:list) {
            if(file.substring(file.lastIndexOf(".")).toLowerCase().equals(".pdf")) {
                LOGGER.debug("--------Precess file:" + file);
                process.processPDF(new FileInputStream(rootDir + File.separator + file.replaceAll("%20", " ")));
            }
        }
    }
    @Test
    public void processPDFTestWholeMassMRN_INVOICEexcelGeneration2() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String rootDir = classLoader.getResource("pdf/SCHNEIDERm+i/1").getPath().substring(1);
        File fileDir= new File(rootDir);
        String[] list=fileDir.list();
        int mrn_count=list.length;
        for (String file:list) {
            System.out.println("!!!FILE:"+file);
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }

        rootDir = classLoader.getResource("pdf/SCHNEIDERm+i/1").getPath().substring(1);
        fileDir= new File(rootDir);
        list=fileDir.list();
        int invoice_count=list.length;
        for (String file:list) {
            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
        }
        FileOutputStream out = new FileOutputStream(classLoader.getResource("pdf").getPath().substring(1)+File.separator+"out.xls");
        process.generateExcel().write(out);
        out.close();
    }
//    @Test
//    public void processPDFTestWholeMassMRN_INVOICEexcelGeneration3() throws IOException {
//        ClassLoader classLoader = getClass().getClassLoader();
//        String rootDir = classLoader.getResource("pdf/SCHNEIDERm+i/2").getPath().substring(1);
//        File fileDir= new File(rootDir);
//        String[] list=fileDir.list();
//        int mrn_count=list.length;
//        for (String file:list) {
//            System.out.println("!!!FILE:"+file);
//            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
//        }
//
//        rootDir = classLoader.getResource("pdf/SCHNEIDERm+i/2").getPath().substring(1);
//        fileDir= new File(rootDir);
//        list=fileDir.list();
//        int invoice_count=list.length;
//        for (String file:list) {
//            System.out.println("!!!FILE:"+file);
//            process.processPDF(new FileInputStream(rootDir+File.separator+file.replaceAll("%20"," ")));
//        }
//        FileOutputStream out = new FileOutputStream(classLoader.getResource("pdf").getPath().substring(1)+File.separator+"out.xls");
//        process.generateExcel().write(out);
//        out.close();
//    }
}

