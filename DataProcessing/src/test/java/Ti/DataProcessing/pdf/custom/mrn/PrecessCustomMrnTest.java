package Ti.DataProcessing.pdf.custom.mrn;

import Ti.model.mrn.CimSmgsInvoiceGruz;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PrecessCustomMrnTest {
    private List<String> files;
    private List<String> filesType2;
//    private List<String> filesNew;
    private List<String> filesAll;
    private ProcessCustomMrn precessCustomMrn;

    @Before
    public void testSetUp() throws IOException, URISyntaxException {
        String PDF_DIR_TYPE1 = "pdf/CustomMrn/type1";
        String PDF_DIR_TYPE2 = "pdf/CustomMrn/type2";
//        String PDF_DIR_NEW = "pdf/CustomMrn/new";
        String PDF_DIR_ALL = "pdf/CustomMrn/all";
        URI uri= getClass().getClassLoader().getResource(PDF_DIR_TYPE1).toURI();
        URI uriType2= getClass().getClassLoader().getResource(PDF_DIR_TYPE2).toURI();
//        URI uriNew= getClass().getClassLoader().getResource(PDF_DIR_NEW).toURI();
        URI uriAll= getClass().getClassLoader().getResource(PDF_DIR_ALL).toURI();
        String path = Paths.get(uri).toString();
        String pathType2 = Paths.get(uriType2).toString();
//        String pathNew = Paths.get(uriNew).toString();
        String pathAll = Paths.get(uriAll).toString();
        files= Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());

        filesType2= Files.walk(Paths.get(pathType2))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());

//        filesNew= Files.walk(Paths.get(pathNew))
//                .filter(Files::isRegularFile)
//                .map(Path::toString)
//                .collect(Collectors.toList());
        filesAll= Files.walk(Paths.get(pathAll))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());

        precessCustomMrn = new ProcessCustomMrn();
    }

    @Test
    public void readBruttoString() throws Exception {
        for (String filePath:files) {
            System.out.println( filePath);
            InputStream inputStream = new FileInputStream(new File(filePath));
            precessCustomMrn.init(inputStream);
            BigDecimal brutto =precessCustomMrn.readBrutto();
            Assert.assertNotNull(precessCustomMrn.getInvoiceModel().getGrossWeight());
            Assert.assertEquals("type1",precessCustomMrn.getDocType());
            System.out.println( brutto);
        }
    }
    @Test
    public void readBruttoStringType2() throws Exception {
        for (String filePath:filesType2) {
            System.out.println( filePath);
            InputStream inputStream = new FileInputStream(new File(filePath));
            precessCustomMrn.init(inputStream);
            BigDecimal brutto =precessCustomMrn.readBrutto();
            Assert.assertNotNull(precessCustomMrn.getInvoiceModel().getGrossWeight());
            Assert.assertEquals("type2",precessCustomMrn.getDocType());
            System.out.println( brutto);
        }
    }

    @Test
    public void readPositionsStringTest() throws Exception {
        for (String filePath:files) {
            System.out.println( filePath);
            InputStream inputStream = new FileInputStream(new File(filePath));
            precessCustomMrn.init(inputStream);
            precessCustomMrn.readBrutto();
            Integer poss =precessCustomMrn.readPositionsCount();
            System.out.println( poss);
            Assert.assertNotNull(poss);
            Assert.assertTrue(poss>0);
        }
    }
    @Test
    public void readPositionsStringTestType2() throws Exception {
        for (String filePath:filesType2) {
            System.out.println( filePath);
            InputStream inputStream = new FileInputStream(new File(filePath));
            precessCustomMrn.init(inputStream);
            precessCustomMrn.readBrutto();
            Integer poss =precessCustomMrn.readPositionsCount();
            System.out.println( poss);
            Assert.assertNotNull(poss);
            Assert.assertTrue(poss>0);
        }
    }
    @Test
    public void processPagePositionsTest() throws Exception {
        for (String filePath:files) {
            System.out.println( filePath);
            InputStream inputStream = new FileInputStream(new File(filePath));
            precessCustomMrn.init(inputStream);
            precessCustomMrn.readBrutto();
            precessCustomMrn.readPositionsCount();
            precessCustomMrn.processPositionsType1();

            System.out.println( "---"+precessCustomMrn.getInvoiceModel().getPositionsCount()+"---"+precessCustomMrn.getInvoiceModel().getPositions().size());
            Assert.assertEquals(precessCustomMrn.getInvoiceModel().getPositionsCount().intValue(),precessCustomMrn.getInvoiceModel().getPositions().size());
            for (CimSmgsInvoiceGruz gruz:precessCustomMrn.getInvoiceModel().getPositions()) {
                System.out.println(gruz.getTnved()+":"+gruz.getMnet());
                Assert.assertNotNull(gruz.getMnet());
                Assert.assertNotNull(gruz.getTnved());
            }
        }
    }
//    @Test
//    public void processPagePositionsType2Test() throws Exception {
//        for (String filePath:filesType2) {
//            System.out.println( filePath);
//            InputStream inputStream = new FileInputStream(new File(filePath));
//            precessCustomMrn.init(inputStream);
//            precessCustomMrn.readBrutto();
//            precessCustomMrn.readPositionsCount();
//            precessCustomMrn.processPositionsType2();
//
//            System.out.println( "---"+precessCustomMrn.getInvoiceModel().getPositionsCount()+"---"+precessCustomMrn.getInvoiceModel().getPositions().size());
//            Assert.assertEquals(precessCustomMrn.getInvoiceModel().getPositionsCount().intValue(),precessCustomMrn.getInvoiceModel().getPositions().size());
//            for (CimSmgsInvoiceGruz gruz:precessCustomMrn.getInvoiceModel().getPositions()) {
//                Assert.assertNotNull(gruz.getMnet());
//                Assert.assertNotNull(gruz.getTnved());
//            }
//        }
//    }
//
//    @Test
//    public void allFilesProcessTest() throws Exception {
//        List<String> allFiles= new ArrayList<>(files);
//        allFiles.addAll(filesType2);
//        allFiles.addAll(filesAll);
//
//        for (String filePath:allFiles) {
//            System.out.println(filePath);
//            InputStream inputStream = new FileInputStream(new File(filePath));
//            precessCustomMrn.init(inputStream);
//            precessCustomMrn.processFile();
//            for (CimSmgsInvoiceGruz gruz:precessCustomMrn.getInvoiceModel().getPositions()) {
//                Assert.assertNotNull(gruz.getMnet());
//                Assert.assertNotNull(gruz.getTnved());
//                Assert.assertTrue(gruz.getMnet().doubleValue()>0);
//            }
//            precessCustomMrn.generateXLS();
//
//        }
//    }

//    @Test
//    public void newFilesProcessTest() throws Exception {
//        List<String> allFiles= new ArrayList<>(files);
//        allFiles.addAll(filesNew);
//
//        for (String filePath:allFiles) {
//            System.out.println(filePath);
//            InputStream inputStream = new FileInputStream(new File(filePath));
//            precessCustomMrn.init(inputStream);
//            precessCustomMrn.processFile();
//            for (CimSmgsInvoiceGruz gruz:precessCustomMrn.getInvoiceModel().getPositions()) {
//                Assert.assertNotNull(gruz.getMnet());
//                Assert.assertNotNull(gruz.getTnved());
//            }
//        }
//    }

//    @Test
//    public void oneFileDirProccess() throws Exception {
//        String fileDir = "pdf/CustomMrn/oneFileDir";
//        URI uri= getClass().getClassLoader().getResource(fileDir).toURI();
//        String path = Paths.get(uri).toString();
//        List<String> file= Files.walk(Paths.get(path))
//                .filter(Files::isRegularFile)
//                .map(Path::toString)
//                .collect(Collectors.toList());
//
//        for (String filePath:file) {
//            System.out.println(filePath);
//            InputStream inputStream = new FileInputStream(new File(filePath));
//            precessCustomMrn.init(inputStream);
//            precessCustomMrn.processFile();
//            for (CimSmgsInvoiceGruz gruz:precessCustomMrn.getInvoiceModel().getPositions()) {
//                Assert.assertNotNull(gruz.getMnet());
//                Assert.assertNotNull(gruz.getTnved());
//                Assert.assertTrue(gruz.getMnet().doubleValue()>0);
//            }
//        }
//    }
}
