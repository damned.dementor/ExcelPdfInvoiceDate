package Ti.DataProcessing.excel;

import Ti.model.Volvo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class VolvoExcelProcessTest {

    private InputStream volvoFile;
    HashMap<String,String> checks= new HashMap<>();

    @Before
    public void testSetUp() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("volvo/testVolvo.xlsx").getFile());
        volvoFile=new FileInputStream(file);

        checks.put("A2","TRAIN");
    }

    @Test
    public void test1() {
        VolvoExcelProcess volvoExcelProcess = new VolvoExcelProcess();
        Assert.assertTrue(volvoExcelProcess.init(volvoFile,checks));
        ArrayList<Volvo> volvos= (ArrayList<Volvo>) volvoExcelProcess.processSheet();
        System.out.println(volvos);
        Assert.assertEquals(volvos.size(),90);
    }

}