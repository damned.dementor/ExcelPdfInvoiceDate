package Ti.DataProcessing.excel.stuffingMerge;

import Ti.model.excel.stuffing.ItogoCurModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class MergeStuffingXLSTest {
    private String itogoDir= "/stuffingXls/itogoFiles";
    private String itogoDir2= "/stuffingXls/itogoFiles2";
    private String stuffFile= "/stuffingXls/stuffingFile/STUFFING.xls";


    private List<String> itogoFiles;
    private List<String> itogoFiles2;
    private MergeStuffingXLS mergeStuffingXLS;


    @Before
    public void testSetUp() throws URISyntaxException, IOException {
        mergeStuffingXLS = new MergeStuffingXLS();

        URI uri= getClass().getResource(itogoDir).toURI();

        itogoFiles = Files.walk(Paths.get(uri))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());
        uri= getClass().getResource(itogoDir2).toURI();

        itogoFiles2 = Files.walk(Paths.get(uri))
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());
    }

    @Test
    public void processItogoXlsTest() throws Exception {

        for (String s:itogoFiles) {
            System.out.println(s);
            ItogoCurModel model=mergeStuffingXLS.processItogoXls(s);
            System.out.println(model);
        }
    }

    @Test
    public void processStuffingFile() throws Exception {
        Map<String,ItogoCurModel> curModels = new TreeMap<>();
        for (String s:itogoFiles) {
            System.out.println(s);
            ItogoCurModel curModel=mergeStuffingXLS.processItogoXls(s);
            curModels.put(curModel.getUtiN(),curModel);
        }
        URI uri= getClass().getResource(stuffFile).toURI();
        List<String> didntFind= mergeStuffingXLS.processStuffingFile(Paths.get(uri).toString(),curModels);
        Assert.assertEquals(38,didntFind.size());

    }
    @Test
    public void processStuffingFile2() throws Exception {
        Map<String,ItogoCurModel> curModels = new TreeMap<>();
        for (String s:itogoFiles2) {
            System.out.println(s);
            ItogoCurModel curModel=mergeStuffingXLS.processItogoXls(s);
            curModels.put(curModel.getUtiN(),curModel);
        }

        for (ItogoCurModel model:curModels.values()) {
            Assert.assertNotNull(model.getUtiN());
            Assert.assertNotNull(model.getCur());
            Assert.assertNotNull(model.getItogo());
        }

        System.out.println(curModels.size());
    }
}
