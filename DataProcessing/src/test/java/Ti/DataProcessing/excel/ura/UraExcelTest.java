package Ti.DataProcessing.excel.ura;

import Ti.DataProcessing.Utils;
import Ti.model.ura.ClientWorks;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class UraExcelTest {
    private InputStream uraIput;

    @Before
    public void testSetUp() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file1 = new File(classLoader.getResource("ura.xlsx").getFile());
        uraIput=new FileInputStream(file1);
    }
    @Test
    public void uraTest1()
    {
        UraExcel uraExcel = new UraExcel();
        uraExcel.init(uraIput,null);

        ArrayList<ClientWorks> clientWorks= (ArrayList<ClientWorks>) uraExcel.processSheet();
        for (ClientWorks strings : clientWorks){
            System.out.println(strings);
        }
        Assert.assertTrue(clientWorks.size()>0);
        for(int i=0;i<5;i++)
        {
            int clientNum= Utils.generateRandomIntIntRange(0, clientWorks.size()-1);
            String clientName=clientWorks.get(clientNum).getClientName();
            List<String> works=clientWorks.get(clientNum).getWorks();
            ArrayList<Integer> actionsDone = new ArrayList<>();
            ThreadLocalRandom.current().ints(0, works.size()).distinct().limit(Utils.generateRandomIntIntRange(1, Math.min(works.size(), 3)))
                    .forEach(actionsDone::add);
            actionsDone.sort(Integer::compareTo);
            System.out.println(actionsDone);
        }


    }
    @Test
    public void test2()
    {
        ArrayList<Integer> ints= new ArrayList<>();
        ThreadLocalRandom.current().ints(0, 40).distinct().limit(30).forEach(ints::add);
        ints.sort(Integer::compareTo);
        System.out.println(ints);
    }
}