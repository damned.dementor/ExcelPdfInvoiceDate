package Ti.DataProcessing.excel.porsche;

import Ti.model.porsche.PorscheSpec;
import Ti.model.porsche.PorscheSpecInvoice;
import Ti.model.porsche.PorscheSpecRecord;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class PorscheExcelProcess1Test {

    private InputStream porsche;
    private InputStream porsche2;
    private InputStream porsche2_2;
    private InputStream porsche3;
    private InputStream porschetest2_2;
    private HashMap<String,String> checks= new HashMap<>();
    private HashMap<String,String> checks2= new HashMap<>();

    private File folder;
    @Before
    public void testSetUp() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file1 = new File(classLoader.getResource("porsche/1/in1-CICU9928985.xls").getFile());
        porsche=new FileInputStream(file1);

        File file2 = new File(classLoader.getResource("porsche/1/in1-XHCU5036486.xls").getFile());
        porsche2=new FileInputStream(file2);

        File file2_2 = new File(classLoader.getResource("porsche/1/in1-XHCU5036487.xls").getFile());
        porsche2_2=new FileInputStream(file2_2);

        File file3 = new File(classLoader.getResource("porsche/2/in2-listv2.xlsx").getFile());
        porsche3=new FileInputStream(file3);

        folder = new File(classLoader.getResource("porsche/test2/1").getFile());

        File test2_2 = new File(classLoader.getResource("porsche/test2/test2_2.xlsx").getFile());
        porschetest2_2=new FileInputStream(test2_2);

        checks.put("A1","SPECIFICATION");
        checks2.put("A1","INVOICE- SPECIFICATION");
    }

    @Test
    public void test1()
    {
        PorscheExcelProcess1 excelProcess1 = new PorscheExcelProcess1();
        excelProcess1.init(porsche,checks);
        ArrayList<PorscheSpec> porscheSpecs= (ArrayList<PorscheSpec>) excelProcess1.processSheet();
        print(porscheSpecs.get(0));
        Assert.assertNotNull(porscheSpecs);
        Assert.assertEquals(porscheSpecs.size(),1);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().size(),2);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getGrossWeight(),2100,0);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getGrossWeight(),2100,0);

        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getNetWeight(),2100,0);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getNetWeight(),2100,0);

        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getPricePcsCny(),new BigDecimal("366004.00"));
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getPricePcsCny(),new BigDecimal("401209.00"));
    }

    @Test
    public void test2()
    {
        PorscheExcelProcess1 excelProcess1 = new PorscheExcelProcess1();
        excelProcess1.init(porsche2,checks);
        ArrayList<PorscheSpec> porscheSpecs= (ArrayList<PorscheSpec>) excelProcess1.processSheet();
        print(porscheSpecs.get(0));
        Assert.assertNotNull(porscheSpecs);
        Assert.assertEquals(porscheSpecs.size(),1);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().size(),2);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getGrossWeight(),2400,0);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getGrossWeight(),2101,0);

        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getNetWeight(),2400,0);
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getNetWeight(),2101,0);

        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(0).getPricePcsCny(),new BigDecimal("724722.00"));
        Assert.assertEquals(porscheSpecs.get(0).getRecords().get(1).getPricePcsCny(),new BigDecimal("687694.00"));
    }
    @Test
    public void test2_2()
    {
        PorscheExcelProcess1 excelProcess1 = new PorscheExcelProcess1();
        excelProcess1.init(porsche2_2,checks);
        ArrayList<PorscheSpec> porscheSpecs= (ArrayList<PorscheSpec>) excelProcess1.processSheet();
        print(porscheSpecs.get(0));
    }

    @Test
    public void generateXLStest() throws IOException {
        PorscheExcelProcess1 excelProcess1 = new PorscheExcelProcess1();
        PorscheExcelProcess2 excelProcess2 = new PorscheExcelProcess2();

        FileOutputStream outputStream =new FileOutputStream( getClass().getClassLoader().getResource("porsche/").getPath()+ File.separator +"out.xls");
        excelProcess1.init(porsche,checks);
        ArrayList<PorscheSpec> porscheSpecs= (ArrayList<PorscheSpec>) excelProcess1.processSheet();

        excelProcess1.init(porsche2,checks);
        porscheSpecs.addAll((ArrayList<PorscheSpec>) excelProcess1.processSheet());

        excelProcess1.init(porsche2_2,checks);
        porscheSpecs.addAll((ArrayList<PorscheSpec>) excelProcess1.processSheet());

        excelProcess2.init(porsche3,checks2);
        ArrayList<PorscheSpecInvoice> porscheSpecInvoices= (ArrayList<PorscheSpecInvoice>) excelProcess2.processSheet();

        excelProcess1.generateXls(porscheSpecs,porscheSpecInvoices.get(0)).write(outputStream);
        outputStream.close();
    }

    @Test
    public void test3() throws FileNotFoundException {
        String[] Flist = folder.list();
        ArrayList<PorscheSpec> porscheSpecs = new ArrayList<>();
        PorscheSpecInvoice porscheSpecInvoice;
        PorscheExcelProcess1 porscheExcelProcess1 = new PorscheExcelProcess1();
        PorscheExcelProcess2 porscheExcelProcess2 = new PorscheExcelProcess2();
        for (String S : Flist) {
            System.out.println(S);
            porscheExcelProcess1.init(new FileInputStream(new File(folder.getPath()+File.separator+S)),checks);
            porscheSpecs.addAll((Collection<? extends PorscheSpec>) porscheExcelProcess1.processSheet());
        }
        porscheExcelProcess2.init(porschetest2_2,checks2);
        porscheSpecInvoice= (PorscheSpecInvoice) porscheExcelProcess2.processSheet().get(0);

        Workbook wb= porscheExcelProcess1.generateXls(porscheSpecs,porscheSpecInvoice);
        Assert.assertNotNull(wb);
        Assert.assertEquals(wb.getNumberOfSheets(),1);

        Sheet sheet= wb.getSheetAt(0);
        Assert.assertEquals(sheet.getLastRowNum(),58);
        Assert.assertEquals(sheet.getRow(53).getCell(2).getCellStyle().getFillForegroundColor(), HSSFColor.RED.index);
    }

    private void print(PorscheSpec porscheSpec)
    {
        System.out.println(porscheSpec.getContNo());
        System.out.println(porscheSpec.getInvoice1());
        System.out.println(porscheSpec.getInvoice2());
        System.out.println(porscheSpec.getSealNo());
        for (PorscheSpecRecord record: porscheSpec.getRecords() ) {
            System.out.println(record);
        }
    }


}