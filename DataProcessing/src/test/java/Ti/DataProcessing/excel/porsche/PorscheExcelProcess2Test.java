package Ti.DataProcessing.excel.porsche;

import Ti.model.porsche.PorscheSpecInvoice;
import Ti.model.porsche.PorscheSpecInvoiceRecord;
import Ti.model.porsche.PorscheSpecInvoiceRecordElement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class PorscheExcelProcess2Test {

    private InputStream porsche,porsche2;
    private HashMap<String,String> checks= new HashMap<>();

    @Before
    public void testSetUp() throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("porsche/2/in2-list.xlsx").getFile());
        porsche = new FileInputStream(file);

        File file2 = new File(classLoader.getResource("porsche/2/in2-listv2.xlsx").getFile());
        porsche2 = new FileInputStream(file2);

        checks.put("A1","INVOICE- SPECIFICATION");
    }

    @Test
    public void test1()
    {
        PorscheExcelProcess2  process2 = new PorscheExcelProcess2();
        process2.init(porsche,checks);
        ArrayList<PorscheSpecInvoice> porscheSpecInvoices = (ArrayList<PorscheSpecInvoice>) process2.processSheet();
        Assert.assertEquals(porscheSpecInvoices.size(),1);
        Assert.assertEquals(porscheSpecInvoices.get(0).getRecords().size(),29);
        printRes(porscheSpecInvoices);

    }
    @Test
    public void test2()
    {
        PorscheExcelProcess2  process2 = new PorscheExcelProcess2();
        process2.init(porsche2,checks);
        ArrayList<PorscheSpecInvoice> porscheSpecInvoices = (ArrayList<PorscheSpecInvoice>) process2.processSheet();
        Assert.assertEquals(porscheSpecInvoices.size(),1);
        Assert.assertEquals(porscheSpecInvoices.get(0).getRecords().size(),29);
        Assert.assertEquals(porscheSpecInvoices.get(0).getRecords().get(0).getElements().size(),3);
        printRes(porscheSpecInvoices);
    }

    private void printRes(ArrayList<PorscheSpecInvoice> porscheSpecInvoices)
    {
        for (PorscheSpecInvoice invoice:porscheSpecInvoices ) {
            System.out.println(invoice.getInvSpec());
            for (PorscheSpecInvoiceRecord record: invoice.getRecords()) {
                System.out.println(record.getContNo());
                for (PorscheSpecInvoiceRecordElement element:record.getElements() ) {
                    System.out.println(element);
                }
            }
        }
    }
}
