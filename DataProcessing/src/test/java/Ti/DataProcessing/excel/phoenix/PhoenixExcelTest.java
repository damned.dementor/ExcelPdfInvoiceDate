package Ti.DataProcessing.excel.phoenix;

import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

public class PhoenixExcelTest {


    @Test
    public void initDictTest() throws IOException {
        PhoenixExcel phoenixExcel= new PhoenixExcel();
        phoenixExcel.initDict(getClass().getClassLoader().getResourceAsStream("phoenix/PHOENIX.xlsx"));
        Assert.assertTrue(!phoenixExcel.getDict().isEmpty());
        assertEquals(1005, phoenixExcel.getDict().size());
        phoenixExcel.readWb(getClass().getClassLoader().getResourceAsStream("phoenix/TCLU17281590079978.xls"));
        Workbook workbook= phoenixExcel.processSheet();
        FileOutputStream outputStream =new FileOutputStream( getClass().getClassLoader().getResource("phoenix/").getPath()+ File.separator +"out.xls");
        workbook.write(outputStream);
        outputStream.close();
    }
    @Test
    public void initDictTest2() throws IOException {
        PhoenixExcel phoenixExcel= new PhoenixExcel();
        phoenixExcel.initDict(getClass().getClassLoader().getResourceAsStream("phoenix/PHOENIX.xlsx"));
        Assert.assertTrue(!phoenixExcel.getDict().isEmpty());
        assertEquals(1005, phoenixExcel.getDict().size());
        phoenixExcel.readWb(getClass().getClassLoader().getResourceAsStream("phoenix/TCKU9032777PLIV.xls"));
        Workbook workbook= phoenixExcel.processSheet();
        FileOutputStream outputStream =new FileOutputStream( getClass().getClassLoader().getResource("phoenix/").getPath()+ File.separator +"out.xls");
        workbook.write(outputStream);
        outputStream.close();
    }
}
