package Ti.app.phoenix;

import Ti.DataProcessing.excel.phoenix.PhoenixExcel;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.file.Paths;

public class PhoenixFrame extends JFrame implements WindowListener, ActionListener {

    private JPanel panel;
    private JLabel xls1;
    private JButton xlsOpenButton;
    private FileNameExtensionFilter filter;
    private JFileChooser dirChooser;

    PhoenixExcel phoenixExcel = new PhoenixExcel();


    public PhoenixFrame() throws HeadlessException {
        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");
        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));
        xls1 = new JLabel("");
        xlsOpenButton = new JButton("Открыть XLS файл");
        dirChooser = new JFileChooser();
        xlsOpenButton.addActionListener(this);


        panel.add(xls1);
        panel.add(xlsOpenButton);
        add(panel, BorderLayout.NORTH);
        setTitle("Phoenix app");
        addWindowListener(this);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(xlsOpenButton)) {
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String file = dirChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                if (ext.toLowerCase().equals("xls") || ext.toLowerCase().equals("xlsx")) {
                    try {
                        phoenixExcel.readWb(new FileInputStream(new File(file)));
                        Workbook wb= phoenixExcel.processSheet();
                        if (dirChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {

                            String fileName = FilenameUtils.removeExtension(dirChooser.getSelectedFile().getName());
                            OutputStream outputStream = new FileOutputStream(new File(Paths.get(dirChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls"));
                            wb.write(outputStream);
                            outputStream.close();
                            JOptionPane.showMessageDialog(null,"Файл:" +dirChooser.getSelectedFile().getName()+ "\nсоздан в каталоге:\n"+Paths.get(dirChooser.getSelectedFile().getPath()).getParent().toString(),"Внимание", JOptionPane.INFORMATION_MESSAGE);
                            xls1.setText("");
                        }
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(null, "Ошибка обработки XLS файла", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }

        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
        File dictFile = new File(Paths.get(".").toAbsolutePath().normalize().toString()+File.separator+"PHOENIX.xlsx");

        try {
            phoenixExcel.initDict(new FileInputStream(dictFile));
        } catch (FileNotFoundException e1) {
            xls1.setText("Ошибка открытия файла словаря");
            JOptionPane.showMessageDialog(null, "Ошибка открытия файла словаря", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
            xls1.setText("СЛОВАРЬ НЕ НАЙДЕН!!!");
            return;
        }

        xls1.setText("Словарь открыт успешно");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
