package Ti.app.StuffingMerge;

import Ti.DataProcessing.excel.stuffingMerge.MergeStuffingXLS;
import Ti.model.excel.stuffing.ItogoCurModel;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class StuffingMergeFrame extends JFrame implements WindowListener, ActionListener {
    private MergeStuffingXLS stuffingXLS;
    private String stuffingFile;
    private JPanel panel;
    private JLabel jLabel;
    private JLabel jLabel2;
    private JLabel jLabel3;

    private JButton button1;
    private JButton button2;
    private JButton button3;

    private List list;
    private JFileChooser dirChooser;
    private FileNameExtensionFilter filter;

    public StuffingMergeFrame() throws HeadlessException {
        stuffingXLS = new MergeStuffingXLS();
        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        jLabel = new JLabel("Выбрать Stuffing XLS/XLSX");
        jLabel2 = new JLabel("Открыть папку с XLS/XLSX файлами");
        jLabel3 = new JLabel("Обработать");

        button1 = new JButton("Открыть");
        button2 = new JButton("Открыть");
        button3 = new JButton("Обработать");
        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button2.setEnabled(false);
        button3.setEnabled(false);

        list = new List(20);
        list.setVisible(true);
        dirChooser = new JFileChooser();

        panel.add(jLabel);
        panel.add(button1);
        panel.add(jLabel2);
        panel.add(button2);
        panel.add(jLabel3);
        panel.add(button3);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Stuffing XLS");
        addWindowListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(button1)){
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String file = dirChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                if (ext.toLowerCase().equals("xls") || ext.toLowerCase().equals("xlsx")) {

                    stuffingFile= dirChooser.getSelectedFile().getPath();
                    button2.setEnabled(true);
                    button3.setEnabled(false);
                    list.removeAll();
                }
            }
        }
        if(e.getSource().equals(button2)){
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                setTitle(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                for (String S : Flist) {
                    if (FilenameUtils.getExtension(S).toLowerCase().equals("xls") || FilenameUtils.getExtension(S).toLowerCase().equals("xlsx")) {
                        list.add(S);
                    }
                }
                if (list.getItems().length > 0) {
                    button3.setEnabled(true);
                } else
                    button3.setEnabled(false);
            }
        }
        if(e.getSource().equals(button3)){
            int count = list.getItemCount();
            Map<String,ItogoCurModel> itogoCurModels = new TreeMap<>();
            for (int index = 0; index < count; index++) {
                try  {
                    ItogoCurModel model = stuffingXLS.processItogoXls(getTitle() + File.separator + list.getItem(0));
                    itogoCurModels.put(model.getUtiN(),model);
                    list.remove(0);
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(null,  list.getItem(0) + "\n Обработался с ошибкой\n" + exc.getMessage(), "Информация", JOptionPane.INFORMATION_MESSAGE);
                    list.remove(0);
                    exc.printStackTrace();
                }
            }
            try {
                JTextPane jtp = new JTextPane();

                Document doc = jtp.getDocument();
                java.util.List<String> unusedUtins= stuffingXLS.processStuffingFile(stuffingFile,itogoCurModels);
                java.util.List<String> unusedItogo= itogoCurModels.values().stream().filter(itogoCurModel -> {return !itogoCurModel.isUsed();}).map(ItogoCurModel::getUtiN).collect(Collectors.toList());
                String res="Данные сохранены\n"+"Не найдены контейнеры из штуфинг листа:"+unusedUtins.stream().map(s -> s+"").collect(Collectors.joining(", "));
                doc.insertString(doc.getLength(),res, new SimpleAttributeSet());

                res="\n\nНе использованы контейнеры из XLS файлов:"+unusedItogo.stream().map(s -> s+" ").collect(Collectors.joining(", "));
                doc.insertString(doc.getLength(),res, new SimpleAttributeSet());
                jtp.setSize(new Dimension(480, 10));
                jtp.setPreferredSize(new Dimension(480, jtp.getPreferredSize().height));

                JOptionPane.showMessageDialog(null,
                        jtp,"Информация", JOptionPane.INFORMATION_MESSAGE);

                button3.setEnabled(false);
                button2.setEnabled(false);
                setTitle("Stuffing XLS");

            } catch (IOException | InvalidFormatException | BadLocationException exc) {
                JOptionPane.showMessageDialog(null,  stuffingFile + "\n Обработался с ошибкой\n" + exc.getMessage(), "Информация", JOptionPane.INFORMATION_MESSAGE);
                exc.printStackTrace();
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
