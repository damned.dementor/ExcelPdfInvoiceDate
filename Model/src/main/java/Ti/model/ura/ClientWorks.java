package Ti.model.ura;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientWorks {
    private String clientName;
    private List<String> works = new ArrayList<>();

}
