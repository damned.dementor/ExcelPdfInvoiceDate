package Ti.model;

import java.math.BigDecimal;

public class MrnModel {
    private int position;
    private String commodityCode_2;
    private String invoiceNumber;
    private BigDecimal brutto;
    private BigDecimal netto;
    private boolean isUsed=false;

    public MrnModel(int position, String commodityCode_2, String invoiceNumber, BigDecimal brutto, BigDecimal netto) {
        this.position = position;
        this.commodityCode_2 = commodityCode_2;
        this.invoiceNumber = invoiceNumber;
        this.brutto = brutto;
        this.netto = netto;
    }

    public MrnModel() {
    }

    public int getPosition() {
        return position;
    }

    public String getCommodityCode_2() {
        return commodityCode_2;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public BigDecimal getBrutto() {
        return brutto;
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setCommodityCode_2(String commodityCode_2) {
        this.commodityCode_2 = commodityCode_2;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public void setBrutto(BigDecimal brutto) {
        this.brutto = brutto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    @Override
    public String toString() {
        return "MrnModel{" +
                "position=" + position +
                ", commodityCode_2='" + commodityCode_2 + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", brutto=" + brutto +
                ", netto=" + netto +
                '}';
    }
}
