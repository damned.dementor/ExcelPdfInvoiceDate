package Ti.model;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public class PDFmodelInvoice {

    private static Logger LOGGER = Logger.getLogger(PDFmodelInvoice.class);
    private String docNumber;
    private Date docDate;
    private BigDecimal totalAmount;
    private BigDecimal totalNetAmount;
    private String curency;
    private ArrayList<CargoModel> cargoList = new ArrayList<>();

    public String getDocNumber() {
        return docNumber;
    }

    public Date getDocDate() {
        return docDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public String getCurency() {
        return curency;
    }

    public ArrayList<CargoModel> getCargoList() {
        return cargoList;
    }

    public BigDecimal getTotalNetAmount() {
        return totalNetAmount;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setCurency(String curency) {
        this.curency = curency;
    }


    public void setCargoList(ArrayList<CargoModel> cargoList) {
        this.cargoList = cargoList;
    }

    public void setTotalNetAmount(BigDecimal totalNetAmount) {
        this.totalNetAmount = totalNetAmount;
    }

    public PDFmodelInvoice() {
    }

    public PDFmodelInvoice(String docNumber, Date docDate, BigDecimal totalAmount, BigDecimal totalNetAmount, String curency, ArrayList<CargoModel> cargoList) {
        this.docNumber = docNumber;
        this.docDate = docDate;
        this.totalAmount = totalAmount;
        this.totalNetAmount = totalNetAmount;
        this.curency = curency;
        this.cargoList = cargoList;
    }

    public boolean checkControlSum()
    {
        BigDecimal amount= new BigDecimal(0);

        for (CargoModel cargoModel:cargoList) {
            amount=amount.add(cargoModel.getTotalPrice());
        }
        LOGGER.debug("Expected size:"+totalNetAmount);
        LOGGER.debug("Real size:"+amount);
        return amount.equals(totalNetAmount);
    }

    @Override
    public String toString() {
        StringBuilder cargo= new StringBuilder();
        for (CargoModel model:getCargoList()) {
            cargo.append(model).append("\n");
        }

        return "PDFmodelInvoice{" +
                "docNumber='" + docNumber + '\'' +
                ", docDate=" + docDate +
                ", totalAmount=" + totalAmount +
                ", totalNetAmount=" + totalNetAmount +
                ", curency='" + curency + '\'' +
                ", cargoList=\n" + cargo.toString() +
                '}';
    }
}
