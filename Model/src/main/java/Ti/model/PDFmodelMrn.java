package Ti.model;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PDFmodelMrn {
    private static Logger LOGGER = Logger.getLogger(PDFmodelInvoice.class);
    private  int totalDes;
    private BigDecimal masBrutto;
    private ArrayList<MrnModel> mrnList = new ArrayList<>();
    private boolean isUsed = false;

    public int getTotalDes() {
        return totalDes;
    }

    public BigDecimal getMasBrutto() {
        return masBrutto;
    }

    public ArrayList<MrnModel> getMrnList() {
        return mrnList;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setTotalDes(int totalDes) {
        this.totalDes = totalDes;
    }

    public void setMasBrutto(BigDecimal masBrutto) {
        this.masBrutto = masBrutto;
    }

    public void setMrnList(ArrayList<MrnModel> mrnList) {
        this.mrnList = mrnList;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    @Override
    public String toString() {

        StringBuilder mrns= new StringBuilder();
        for (MrnModel model:getMrnList()) {
            mrns.append(model).append("\n");
        }

        return "PDFmodelMrn{" +
                "totalDes=" + totalDes +
                ", masBrutto=" + masBrutto +
                ", mrnList=\n" + mrns +
                '}';
    }

    public boolean checkControlSum()
    {
        BigDecimal amount= new BigDecimal(0);
        for (MrnModel model:mrnList) {
            amount=amount.add(model.getBrutto());
        }
        LOGGER.debug("Expected size:"+masBrutto);
        LOGGER.debug("Real size:"+amount);
        return amount.equals(masBrutto);
    }
}
