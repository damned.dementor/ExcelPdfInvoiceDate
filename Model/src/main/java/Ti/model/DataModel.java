package Ti.model;

import java.util.Date;

public class DataModel {
    private int row;
    private Date date;

    public DataModel(int row) {
        this.row = row;
        this.date = null;
    }

    public DataModel() {
    }

    public int getRow() {
        return row;
    }

    public Date getDate() {
        return date;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
