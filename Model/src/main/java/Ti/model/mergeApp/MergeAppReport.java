package Ti.model.mergeApp;

import Ti.model.ExcelCell;

import java.util.ArrayList;
import java.util.List;

public class MergeAppReport {
    private String fileName;
    private List<ExcelCell> mrnReadErrors= new ArrayList<>();
    private List<ExcelCell> contNumReadErrors= new ArrayList<>();

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<ExcelCell> getMrnReadErrors() {
        return mrnReadErrors;
    }

    public void setMrnReadErrors(List<ExcelCell> mrnReadErrors) {
        this.mrnReadErrors = mrnReadErrors;
    }

    public List<ExcelCell> getContNumReadErrors() {
        return contNumReadErrors;
    }

    public void setContNumReadErrors(List<ExcelCell> contNumReadErrors) {
        this.contNumReadErrors = contNumReadErrors;
    }

    @Override
    public String toString() {
        return "MergeAppReport{" +
                "fileName='" + fileName + '\'' +
                ", mrnReadErrors=" + mrnReadErrors +
                ", contNumReadErrors=" + contNumReadErrors +
                '}';
    }
}
