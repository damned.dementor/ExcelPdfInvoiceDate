package Ti.model.mrn;

import java.math.BigDecimal;

public class CimSmgsInvoiceGruz {
    private String tnved;
    private BigDecimal mnet;
    private BigDecimal mbrt;
    private BigDecimal mtara;

    public String getTnved() {
        return tnved;
    }

    public void setTnved(String tnved) {
        this.tnved = tnved;
    }

    public BigDecimal getMnet() {
        return mnet;
    }

    public void setMnet(BigDecimal mnet) {
        this.mnet = mnet;
    }

    public BigDecimal getMbrt() {
        return mbrt;
    }

    public void setMbrt(BigDecimal mbrt) {
        this.mbrt = mbrt;
    }

    public BigDecimal getMtara() {
        return mtara;
    }

    public void setMtara(BigDecimal mtara) {
        this.mtara = mtara;
    }
}
