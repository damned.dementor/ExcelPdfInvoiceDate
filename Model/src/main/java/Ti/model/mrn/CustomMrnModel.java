package Ti.model.mrn;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CustomMrnModel {
    private BigDecimal grossWeight;
    private BigDecimal netWeight;
    private Integer positionsCount;
    private List<CimSmgsInvoiceGruz> positions= new ArrayList<>();
    private Map<String,CimSmgsInvoiceGruz> positionsMap= new TreeMap<>();

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public List<CimSmgsInvoiceGruz> getPositions() {
        return positions;
    }

    public void setPositions(List<CimSmgsInvoiceGruz> positions) {
        this.positions = positions;
    }

    public Integer getPositionsCount() {
        return positionsCount;
    }

    public void setPositionsCount(Integer positionsCount) {
        this.positionsCount = positionsCount;
    }

    public Map<String, CimSmgsInvoiceGruz> getPositionsMap() {
        return positionsMap;
    }

    public void setPositionsMap(Map<String, CimSmgsInvoiceGruz> positionsMap) {
        this.positionsMap = positionsMap;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }
}
