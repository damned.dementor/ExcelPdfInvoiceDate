package Ti.model.Wago;

import java.math.BigDecimal;
import java.util.Date;

public class WagoInvoiceModel {

    private String invoice;
    private BigDecimal grossWeight;
    private String NumCoils;
    private Date dated;

    public String getInvoice() {
        return invoice;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public String getNumCoils() {
        return NumCoils;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public void setNumCoils(String numCoils) {
        NumCoils = numCoils;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public WagoInvoiceModel(String invoice, BigDecimal grossWeight, String numCoils, Date dated) {
        this.invoice = invoice;
        this.grossWeight = grossWeight;
        NumCoils = numCoils;
        this.dated = dated;
    }

    public WagoInvoiceModel() {
    }

    @Override
    public String toString() {
        return "WagoInvoiceModel{" +
                "invoice='" + invoice + '\'' +
                ", grossWeight=" + grossWeight +
                ", NumCoils='" + NumCoils + '\'' +
                ", dated=" + dated +
                '}';
    }
}
