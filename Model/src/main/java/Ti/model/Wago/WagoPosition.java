package Ti.model.Wago;

import java.math.BigDecimal;

public class WagoPosition implements Comparable<WagoPosition> {
    //номер позиции в PDF файле
    private String pos;
    // наименование позиции
    private String name;
    // номер тарифа
    private String tarifNo;
    //количество
    private BigDecimal quntity;
    // идиницы измерения
    private String units;
    // стоимость позиции
    private BigDecimal posValue;
    private BigDecimal netto;
    private BigDecimal brutto;

    public String getName() {
        return name;
    }

    public String getTarifNo() {
        return tarifNo;
    }

    public BigDecimal getQuntity() {
        return quntity;
    }

    public String getUnits() {
        return units;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTarifNo(String tarifNo) {
        this.tarifNo = tarifNo;
    }

    public void setQuntity(BigDecimal quntity) {
        this.quntity = quntity;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public BigDecimal getPosValue() {
        return posValue;
    }

    public void setPosValue(BigDecimal posValue) {
        this.posValue = posValue;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public WagoPosition() {
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public BigDecimal getBrutto() {
        return brutto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    public void setBrutto(BigDecimal brutto) {
        this.brutto = brutto;
    }

    public WagoPosition(String pos, String name, String tarifNo, BigDecimal quntity, String units, BigDecimal posValue) {
        this.pos = pos;
        this.name = name;
        this.tarifNo = tarifNo;
        this.quntity = quntity;
        this.units = units;
        this.posValue = posValue;
    }

    @Override
    public String toString() {
        return "WagoPosition{" +
                "pos='" + pos + '\'' +
                ", name='" + name + '\'' +
                ", tarifNo='" + tarifNo + '\'' +
                ", quntity=" + quntity +
                ", units='" + units + '\'' +
                ", posValue=" + posValue +
                ", netto=" + netto +
                ", brutto=" + brutto +
                '}';
    }

    @Override
    public int compareTo(WagoPosition o) {
        return this.getTarifNo().compareTo(o.getTarifNo());
    }
}
