package Ti.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Volvo {
    String train;
    String vin;
}
