package Ti.model.excel.stuffing;

import lombok.Data;

@Data
public class ItogoCurModel {

    private String itogo;
    private String cur;
    private String utiN;
    private boolean used=false;
}
