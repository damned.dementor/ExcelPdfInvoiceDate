package Ti.model;

import java.math.BigDecimal;

public class CargoModel {

    private String POItem;
    private String description;
    private String commodityCode;
    private Long qty;
    private BigDecimal totalPrice;
    private String currency;

    public String getDescription() {
        return description;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public Long getQty() {
        return qty;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPOItem() {
        return POItem;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public void setTotalPrice(BigDecimal totalPrica) {
        this.totalPrice = totalPrica;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setPOItem(String POItem) {
        this.POItem = POItem;
    }

    public CargoModel(String description, String commodityCode, Long qty, BigDecimal totalPrice, String currency, String POItem) {
        this.description = description;
        this.commodityCode = commodityCode;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.currency = currency;
        this.POItem = POItem;
    }

    public CargoModel() {
    }

    @Override
    public String toString() {
        StringBuilder cargo = new StringBuilder();
        return "CargoModel{" +
                String.format("%-15s","POItem='" + POItem + '\'') +
                String.format("%-15s",", commodityCode='" + commodityCode + '\'') +
                String.format("%-10s",", qty=" + qty) +
                String.format("%-23s",", totalPrice=" + totalPrice) +
                ", currency='" + currency + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
