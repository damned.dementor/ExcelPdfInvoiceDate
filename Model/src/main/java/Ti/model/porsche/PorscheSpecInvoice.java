package Ti.model.porsche;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class PorscheSpecInvoice {

    private String invSpec;
    private ArrayList<PorscheSpecInvoiceRecord> records;
}
