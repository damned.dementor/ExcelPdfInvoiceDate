package Ti.model.porsche;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class PorscheSpecInvoiceRecord {

    private String contNo;
    private ArrayList<PorscheSpecInvoiceRecordElement> elements;

}
