package Ti.model.porsche;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PorscheSpecRecord {

    private String hsCode;
    private String description;
    private int qtyPacks;
    private int qtyPcs;
    private double netWeight;
    private double grossWeight;
    private BigDecimal pricePcsCny;
    private String vinNo;
}
