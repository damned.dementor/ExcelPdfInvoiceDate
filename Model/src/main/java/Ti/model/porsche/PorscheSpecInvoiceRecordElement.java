package Ti.model.porsche;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class PorscheSpecInvoiceRecordElement {

    private String hsCode;
    private String description;
    private int qtyPcs;
    private double grossWeight;
    private BigDecimal pricePcsEur;
}
