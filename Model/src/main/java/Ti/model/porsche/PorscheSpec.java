package Ti.model.porsche;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class PorscheSpec {

    private String invoice1;
    private String invoice2;
    private String contNo;
    private String sealNo;
    private ArrayList<PorscheSpecRecord> records;
}
