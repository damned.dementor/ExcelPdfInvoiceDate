package Ti.app.SCHNEIDER;

import Ti.DataProcessing.pdf.MrnInvoiceProcess;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.charset.Charset;

public class SCHNEIDERframe extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(SCHNEIDERframe.class);
    private JPanel panel;
    private JLabel pdfFolderOpenLabel;
    private JLabel pdfProcessLabel;
    private JButton pdfFolderOpenButton;
    private JButton pdfProcessButton;
    private List list;
    private JFileChooser dirChooser;

    private MrnInvoiceProcess mrnInvoiceProcess;
    private FileNameExtensionFilter filterPDF;


    public SCHNEIDERframe()
    {
        filterPDF = new FileNameExtensionFilter(
                "PDF", "pdf");

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        pdfFolderOpenLabel = new JLabel("Открыть папку с PDF файлами");
        pdfProcessLabel = new JLabel("Обработать PDF файлы");
        pdfFolderOpenButton = new JButton("Открыть папку PDF");
        pdfProcessButton = new JButton("Обработать PDF");
        list = new List(20);
        dirChooser = new JFileChooser();

        pdfFolderOpenButton.addActionListener(this);
        pdfProcessButton.addActionListener(this);

        pdfProcessButton.setEnabled(false);
        list.setVisible(true);

        panel.add(pdfFolderOpenLabel);
        panel.add(pdfFolderOpenButton);
        panel.add(pdfProcessLabel);
        panel.add(pdfProcessButton);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Ti Excel PDF processing app");
        addWindowListener(this);
        setVisible(true);
    }


    public void actionPerformed(ActionEvent e) {

        // open an Excel file button pressed.
        if (e.getSource().equals(pdfFolderOpenButton)) {
            dirChooser.removeChoosableFileFilter(null);
            dirChooser.setDialogTitle("Открыть папку с PDF");
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            dirChooser.setCurrentDirectory(new File("."));
            if(dirChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                list.removeAll();
                setTitle(dirChooser.getSelectedFile().getPath());
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                if(Flist!=null) {
                    for (String S : Flist) {
                        int index = S.lastIndexOf('.');
                        if (index != -1) {
                            String ext = S.substring(index);
                            if (ext.toLowerCase().equals(".pdf"))
                                list.add(S);
                        }
                    }
                    if(list.getItemCount()>0)
                        pdfProcessButton.setEnabled(true);
                    else
                        pdfProcessButton.setEnabled(false);
                }
//                if(list.getItemCount()>0) {
//                    list.setVisible(true);
//                    this.add(list);
//                    pdfProcessButton.setEnabled(true);
//                }
                else
                    JOptionPane.showMessageDialog(null,"Выбранная папаке не содержит PDF файлов","Информация", JOptionPane.INFORMATION_MESSAGE);
            }

        }
        if (e.getSource().equals(pdfProcessButton)) {
            mrnInvoiceProcess= new MrnInvoiceProcess();
            while(list.getItemCount()>0) {
                int index=0;
                LOGGER.debug("---------------------------------------------------------------------------------------------------------------");
                LOGGER.debug("File N:" + (index + 1));
                LOGGER.debug("File processing:" + getTitle() + File.separator + list.getItem(0));
                try {
                    FileInputStream pdfFile = new FileInputStream(new File(getTitle() + File.separator + list.getItem(index)));
                    if(!mrnInvoiceProcess.processPDF(pdfFile))
                    {
                        JOptionPane.showMessageDialog(null,"Ошибка при обработке файла :"+list.getItem(index),"Ошибка", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    else
                        pdfFile.close();
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(null,"Ошибка при обработке файла :"+list.getItem(index),"Ошибка", JOptionPane.ERROR_MESSAGE);
                    LOGGER.error("Error processing PDF file:"+getTitle() + File.separator + list.getItem(index));
                    exc.printStackTrace();
                    return;
                }
                list.remove(0);
                index++;
            }

            try {
                FileOutputStream outputStream =new FileOutputStream(getTitle() + File.separator +"out.xls");
                new OutputStreamWriter(outputStream, Charset.forName("CP1251"));
                mrnInvoiceProcess.generateExcel().write(outputStream);
                outputStream.close();
                JOptionPane.showMessageDialog(null,"Файл out.xls создан в каталоге:\n"+getTitle(),"Внимание", JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(null,"Ошибка при формировании Excel докуента","Ошибка", JOptionPane.ERROR_MESSAGE);
                e1.printStackTrace();
            }
        }
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }
}
