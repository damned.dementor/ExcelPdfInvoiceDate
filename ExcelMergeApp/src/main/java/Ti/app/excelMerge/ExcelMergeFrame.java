package Ti.app.excelMerge;

import Ti.DataProcessing.excel.merge.MergeExcel;
import Ti.model.ExcelCell;
import Ti.model.mergeApp.MergeAppReport;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ExcelMergeFrame extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(ExcelMergeFrame.class);
    private FileNameExtensionFilter filterXLS;
    private JPanel panel;
    private JLabel jLabel;
    private JLabel jLabel2;

    private JButton button1;
    private JButton button2;

    private List list;
    private JFileChooser dirChooser;
    private MergeExcel mergeExcel;


    public ExcelMergeFrame() throws HeadlessException {

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        jLabel = new JLabel("Открыть папку с XLS/XLSX файлами");
        jLabel2 = new JLabel("Обработать");
        mergeExcel = new MergeExcel();


        button1 = new JButton("Открыть");
        button2 = new JButton("Обработать");


        list = new List(20);
        dirChooser = new JFileChooser();

        button1.addActionListener(this);
        button2.addActionListener(this);

        button2.setEnabled(false);
        jLabel2.setVisible(false);

        list.setVisible(true);

        panel.add(jLabel);
        panel.add(button1);
        panel.add(jLabel2);
        panel.add(button2);
//        panel.add(jLabel4);
//        panel.add(button4);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Ti Excel processing app");
        addWindowListener(this);
        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(button1)) {
            dirChooser.removeChoosableFileFilter(filterXLS);
            dirChooser.setDialogTitle("Открыть папку с XLS");
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                list.removeAll();
                setTitle(dirChooser.getSelectedFile().getPath());
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                if (Flist != null)
                    for (String S : Flist) {
                        int index = S.lastIndexOf('.');
                        if (index != -1) {
                            String ext = S.substring(index).toLowerCase();
                            if (ext.equals(".xls") || ext.equals(".xlsx"))
                                list.add(S);
                        }
                    }
                if (list.getItemCount() > 0) {
                    button2.setEnabled(true);
                } else {
                    button2.setEnabled(false);
                    JOptionPane.showMessageDialog(null, "Выбранная папаке не содержит XLS файлов", "Информация", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
        if (e.getSource().equals(button2)) {
            try {
                int count = list.getItemCount();
                ArrayList<Sheet> sheets = new ArrayList();
                ArrayList<String> fnames = new ArrayList<>();
                for (int index = 0; index < count; index++) {
                    try (FileInputStream xlsFile = new FileInputStream(new File(getTitle() + File.separator + list.getItem(0)))) {
                        fnames.add(list.getItem(0));
                        Sheet sheet = mergeExcel.readSheet(xlsFile);
                        sheets.add(sheet);
                        list.remove(0);
                        if (list.getItemCount() < 1) {
                            button2.setEnabled(false);
                        }

                    } catch (IOException exc) {
                        JOptionPane.showMessageDialog(null, exc.getMessage(), "Информация", JOptionPane.INFORMATION_MESSAGE);
                        LOGGER.error("Error processing XLS file:" + getTitle() + File.separator + list.getItem(0));
                        list.remove(0);
                        exc.printStackTrace();
                    }
                }

                Workbook workbook = mergeExcel.mergeSheets(sheets, fnames);
                FileOutputStream fileOutputStream = new FileOutputStream(getTitle() + File.separator + "out.xlsx");
                workbook.write(fileOutputStream);
                fileOutputStream.close();

                StringBuilder errors = new StringBuilder();
                int processedFiles=0;
                for (MergeAppReport report : mergeExcel.getExcelMergeReport()) {
                    StringBuilder error= new StringBuilder();

                    if (report.getContNumReadErrors().size() > 0) {
                        error.append("Kонетйнеры с пустыми номерами: ");
                        error.append(report.getContNumReadErrors().stream().map(ExcelCell::toString).collect(Collectors.joining(", ")));
                        error.append("\n");
                    }
                    if (report.getMrnReadErrors().size() > 0) {
                        error.append("Мрн с пустыми номерами: ");
                        error.append(report.getMrnReadErrors().stream().map(ExcelCell::toString).collect(Collectors.joining(", ")));
                        error.append("\n");
                    }else
                        processedFiles++;
//                    if (report.getMrnReadErrors().size() == 0 )
//                    {
//                        processedFiles++;
//                    }
//                        errors.append("Нет ошибок").append("\n");
                    if(error.length()>0) {
                        errors.append(report.getFileName()).append(":\n");
                        errors.append(error);
                        errors.append("\n");
                    }
                }

                JOptionPane.showMessageDialog(null, "Файл успешно сохранен в выбранной папке\n" +
                        "Обработано файлов:"+mergeExcel.getExcelMergeReport().size()+
                        ".\nКоличество необработанных файлов:"+(mergeExcel.getExcelMergeReport().size()-processedFiles)+"\n" +
                        "\nОшибки:\n" + errors, "Информация", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Информация", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
