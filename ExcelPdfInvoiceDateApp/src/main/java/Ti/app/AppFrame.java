package Ti.app;

import Ti.DataProcessing.excel.ProcessEXCELfile;
import Ti.DataProcessing.pdf.ProcessInvoicePDFfile;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AppFrame   extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(AppFrame.class);
    private JPanel panel;
    private JLabel jLabel;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private List list;
    private JFileChooser dirChooser;
    private ProcessEXCELfile processEXCELfile;
    private ProcessInvoicePDFfile processPDFfile;

    private FileNameExtensionFilter filter;
    private FileNameExtensionFilter filterPDF;

    public AppFrame()
    {
        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");

        filterPDF = new FileNameExtensionFilter(
                "PDF", "pdf");

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        jLabel = new JLabel("Открыть XLS/XLSX файл");
        jLabel2 = new JLabel("Открыть папку с PDF файлами");
        jLabel3 = new JLabel("Обработать PDF файлы");
        jLabel4 = new JLabel("Бла бла МРН");
        button1 = new JButton("Открыть EXCEL");
        button2 = new JButton("Открыть папку PDF");
        button3 = new JButton("Обработать PDF");
        button4 = new JButton("Открыть PDF");
        list = new List(20);
        dirChooser = new JFileChooser();

        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button4.addActionListener(this);

        button2.setEnabled(false);
        button3.setEnabled(false);
        list.setVisible(true);

        panel.add(jLabel);
        panel.add(button1);
        panel.add(jLabel2);
        panel.add(button2);
        panel.add(jLabel3);
        panel.add(button3);
//        panel.add(jLabel4);
//        panel.add(button4);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Ti Excel PDF processing app");
        addWindowListener(this);
        setVisible(true);
    }



    public void actionPerformed(ActionEvent e) {

        // open an Excel file button pressed.
        if (e.getSource().equals(button1)) {

            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);

            dirChooser.setDialogTitle("Открыть excel файл");

            if(dirChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                String file=dirChooser.getSelectedFile().getPath();
                String ext= FilenameUtils.getExtension(file);
                if(ext.toLowerCase().equals("xls")||ext.toLowerCase().equals("xlsx")) {
                    processEXCELfile = new ProcessEXCELfile(file);
                    if (processEXCELfile.init()) {
                        button2.setEnabled(true);
                    }
                }
                else
                    JOptionPane.showMessageDialog(null,"Выбран не Excel файл","Информация", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        if (e.getSource().equals(button2)) {
            dirChooser.removeChoosableFileFilter(filter);
            dirChooser.setDialogTitle("Открыть папку с PDF");
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if(dirChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                list.removeAll();
                setTitle(dirChooser.getSelectedFile().getPath());
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                if(Flist!=null)
                 for (String S : Flist) {
                        int index = S.lastIndexOf('.');
                        if (index != -1) {
                            String ext = S.substring(index);
                            if (ext.equals(".pdf"))
                                list.add(S);
                        }
                    }
                if(list.getItemCount()>0) {
                    list.setVisible(true);
                    this.add(list);
                    button3.setEnabled(true);
                }
                else
                    JOptionPane.showMessageDialog(null,"Выбранная папаке не содержит PDF файлов","Информация", JOptionPane.INFORMATION_MESSAGE);

            }
        }
        if (e.getSource().equals(button3))
        {
            int count = list.getItemCount();
            for (int index = 0; index < count; index++) {
                LOGGER.debug("---------------------------------------------------------------------------------------------------------------");
                LOGGER.debug("File N:" + (index + 1));
                LOGGER.debug("File processing:" + getTitle() + File.separator + list.getItem(0));
                try (FileInputStream pdfFile = new FileInputStream(new File(getTitle() + File.separator + list.getItem(0)))) {
                    processEXCELfile.processPDF(pdfFile);

                    list.remove(0);
                    if (list.getItemCount() < 1) {
                        if(processEXCELfile.getDateCount()!=0)
                            button2.setEnabled(false);
                        button3.setEnabled(false);
                    }

                } catch (IOException exc) {
                    LOGGER.error("Error processing PDF file:"+getTitle() + File.separator + list.getItem(0));
                    list.remove(0);

                    exc.printStackTrace();
                }
            }


            if(processEXCELfile.getDateCount()==0)
            {
                JOptionPane.showMessageDialog(null,
                        "В указаных PDF файлах не было обнаружено ни одной записи инвойс с датой!",
                        "Ошибка", JOptionPane.ERROR_MESSAGE);
                LOGGER.error(Thread.currentThread().getStackTrace()[1].getMethodName() +"Wrong pdf files in dir. No invoice-dates found");
            }
            else
            if(processEXCELfile.saveEXCEL())
                JOptionPane.showMessageDialog(null,"Выходной файл успешно создан","Информация", JOptionPane.INFORMATION_MESSAGE);

        }
        if (e.getSource().equals(button4)) {
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filterPDF);
            dirChooser.setCurrentDirectory(new File("."));
            dirChooser.setDialogTitle("Открыть PDF");
            if(dirChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                String file=dirChooser.getSelectedFile().getPath();
                String ext= FilenameUtils.getExtension(file);
                if(ext.toLowerCase().equals("pdf")) {
                    processPDFfile = new ProcessInvoicePDFfile();
                    try {
                        if (processPDFfile.init(new FileInputStream(file))) {
                            try {
                                if(processPDFfile.readFirstPage()) {
                                    if(processPDFfile.getPdFmodel().getTotalAmount()==null)
                                        processPDFfile.readFirstPageTable();

    //                                if(!processPDFfile.isFinished())
                                        processPDFfile.readTable();
                                    processPDFfile.parseTable();
    //                                System.out.println(processPDFfile.getPdFmodel());
                                }
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
                else
                    JOptionPane.showMessageDialog(null,"Выбран не Excel файл","Информация", JOptionPane.INFORMATION_MESSAGE);
            }

        }
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }
}
