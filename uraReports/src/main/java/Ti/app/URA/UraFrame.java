package Ti.app.URA;

import Ti.DataProcessing.excel.ura.UraExcel;
import Ti.model.ura.ClientWorks;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.file.Paths;
import java.time.Year;
import java.util.ArrayList;

public class UraFrame extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(UraFrame.class);

    private JPanel panel;
    private JLabel xls1;
    private JLabel year;
    private JTextField year_edit;
    private JLabel additional_text= new JLabel("Дополнительный текст");
    private JTextField additional_text_edit= new JTextField("согласно договора №29/12 от 29.12.2017");
    private JLabel additional_text_sub= new JLabel("Дополнительный текст внизу");
    private JTextField additional_text_edit_sub= new JTextField("Отчет составил: Бовбель Ю.А.");
    private JButton xls1OpenButton;
    private JButton processButton;

    private FileNameExtensionFilter filter;
    private JFileChooser dirChooser;

    UraExcel uraExcel=null;
    ArrayList<ClientWorks> uraWorks;

    public UraFrame() throws HeadlessException {

        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");
        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(5, 2));

        dirChooser = new JFileChooser();
        year= new JLabel("Год");
        year_edit = new JTextField(String.valueOf(Year.now().getValue()));
        xls1 = new JLabel("");
        xls1OpenButton = new JButton("Открыть XLS файл");
        xls1OpenButton.addActionListener(this);
        processButton = new JButton("Обработать");
        processButton.addActionListener(this);
        processButton.setEnabled(false);
        panel.add(xls1);
        panel.add(xls1OpenButton);
        panel.add(year);
        panel.add(year_edit);
        panel.add(additional_text);
        panel.add(additional_text_edit);
        panel.add(additional_text_sub);
        panel.add(additional_text_edit_sub);
        panel.add(new JLabel(""));
        panel.add(processButton);
        add(panel, BorderLayout.NORTH);
        setTitle("Ura report app");
        addWindowListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //октрываем EXCEL
        if (e.getSource().equals(xls1OpenButton)) {
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String file = dirChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                if (ext.toLowerCase().equals("xls") || ext.toLowerCase().equals("xlsx")) {
                    xls1.setText(dirChooser.getSelectedFile().getName());
                    uraExcel= new UraExcel();
                    try {
                        if (uraExcel.init(new FileInputStream(new File(file)), null)) {
                            uraWorks= (ArrayList<ClientWorks>) uraExcel.processSheet();
                            processButton.setEnabled(true);
                        }
                    }catch (FileNotFoundException e1) {
                        JOptionPane.showMessageDialog(null, "", "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                        xls1.setText("");
                    }
                }
                else {
                    JOptionPane.showMessageDialog(null, "Выбран не Excel файл", "Информация", JOptionPane.INFORMATION_MESSAGE);
                    xls1.setText("");
                    processButton.setEnabled(false);
                }
            }
        }
        //Генерируем выходной файл
        if (e.getSource().equals(processButton)) {
            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {

                UraExcel uraExcel= new UraExcel();
                Workbook wb = uraExcel.generateXls(uraWorks,Integer.parseInt(year_edit.getText()),additional_text_edit.getText(),additional_text_edit_sub.getText());
                String fileName = FilenameUtils.removeExtension(dirChooser.getSelectedFile().getName());
                File xlsFile = new File(dirChooser.getSelectedFile().toString());
                if (xlsFile.exists()) {
                    int response = JOptionPane.showConfirmDialog(null, //
                            "Перезаписать файл?", //
                            "Подтверждение", JOptionPane.YES_NO_OPTION, //
                            JOptionPane.QUESTION_MESSAGE);
                    if (response != JOptionPane.YES_OPTION) {
                        return;
                    }
                }
                try {
                    OutputStream outputStream = new FileOutputStream(new File(Paths.get(dirChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls"));
                    wb.write(outputStream);
                    outputStream.close();
                    xls1.setText("");
                    JOptionPane.showMessageDialog(null, Paths.get(dirChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls", "Файл сохранен", JOptionPane.INFORMATION_MESSAGE);

                }catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(null, "", "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
