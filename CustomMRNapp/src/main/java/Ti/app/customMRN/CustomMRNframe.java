package Ti.app.customMRN;

import Ti.DataProcessing.pdf.custom.mrn.ProcessCustomMrn;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.nio.file.Paths;

public class CustomMRNframe extends JFrame implements WindowListener, ActionListener {
    private static Logger LOGGER = Logger.getLogger(CustomMRNframe.class);
    private FileNameExtensionFilter filterPDF;
    private FileNameExtensionFilter filterXLS;
    private JPanel panel;
    private JLabel pdfOpenLabel;
    private JLabel fileLabel;
    private JButton pdfOpenButton;
    private JButton processButton;
    private JButton exitButton;
    private JFileChooser fileChooser;
    private String choosenFile;
    private String currentDir;
    private ProcessCustomMrn processCustomMrn = new ProcessCustomMrn();


    public CustomMRNframe() throws HeadlessException {
        filterPDF = new FileNameExtensionFilter(
                "PDF файл", "pdf");
        filterXLS = new FileNameExtensionFilter(
                "Excel файл", "xls,xlsx");
        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        pdfOpenLabel = new JLabel("Выберити PDF файл");
        fileLabel = new JLabel("");
        pdfOpenButton = new JButton("Открыть");
        exitButton = new JButton("Выход");
        processButton = new JButton("Обработать");
        fileChooser = new JFileChooser();

        pdfOpenButton.addActionListener(this);
        exitButton.addActionListener(this);
        processButton.addActionListener(this);
        processButton.setEnabled(false);

        panel.add(pdfOpenLabel);
        panel.add(pdfOpenButton);
        panel.add(fileLabel);
        panel.add(processButton);
        panel.add(new JLabel(""));
        panel.add(new JLabel(""));
        panel.add(new JLabel(""));
        panel.add(exitButton);


        add(panel, BorderLayout.NORTH);
        setTitle("MRN app");
        addWindowListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(exitButton)) {
            System.exit(0);
        }

        if (e.getSource().equals(pdfOpenButton)) {
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setFileFilter(filterPDF);

            if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String file = fileChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                currentDir=FilenameUtils.getPath(file);
                if (ext.toLowerCase().equals("pdf")) {
                    choosenFile = fileChooser.getSelectedFile().getPath();
                    fileLabel.setText(FilenameUtils.getName(file));
                    processButton.setEnabled(true);
                }
            }
        }

        if (e.getSource().equals(processButton)) {
            if (e.getSource().equals(processButton)) {
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.setCurrentDirectory(new File(currentDir));
                fileChooser.setFileFilter(filterXLS);
                if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    currentDir=FilenameUtils.getPath(fileChooser.getSelectedFile().toString());
                    try {
                        processCustomMrn.init(new FileInputStream(new File(choosenFile)));
                        try {
                            processCustomMrn.processFile();
                            Workbook wb = processCustomMrn.generateXLS();
                            String fileName = FilenameUtils.removeExtension(fileChooser.getSelectedFile().getName());
                            File xlsFile = new File(
                                    FilenameUtils.getFullPath(fileChooser.getSelectedFile().toString())+File.separator+
                                            fileName+".xls");
                            if (xlsFile.exists()) {
                                int response = JOptionPane.showConfirmDialog(null, //
                                        "Перезаписать файл?", //
                                        "Подтверждение", JOptionPane.YES_NO_OPTION, //
                                        JOptionPane.QUESTION_MESSAGE);
                                if (response != JOptionPane.YES_OPTION) {
                                    return;
                                }
                            }
                            try {
                                OutputStream outputStream = new FileOutputStream(new File(Paths.get(fileChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls"));
                                wb.write(outputStream);
                                outputStream.close();
                                fileLabel.setText("");
                                processButton.setEnabled(false);
                                JOptionPane.showMessageDialog(null, Paths.get(fileChooser.getSelectedFile().getPath()).getParent().toString() + File.separator + fileName + ".xls", "Файл сохранен", JOptionPane.INFORMATION_MESSAGE);

                            } catch (FileNotFoundException e1) {
                                JOptionPane.showMessageDialog(null, "", "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }

                        } catch (Exception e1) {
                            JOptionPane.showMessageDialog(null, "Не могу обработать данный файл (", "Информация", JOptionPane.INFORMATION_MESSAGE);
                            processButton.setEnabled(false);
                            fileLabel.setText("");
                            processButton.setEnabled(false);
                        }


                    } catch (FileNotFoundException e1) {
                        JOptionPane.showMessageDialog(null, "Ошибка открытия файла", "Информация", JOptionPane.INFORMATION_MESSAGE);
                        processButton.setEnabled(false);
                        fileLabel.setText("");
                    }
                }
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
