package Ti.app.Volvo;

import Ti.DataProcessing.excel.VolvoExcelProcess;
import Ti.model.Volvo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class VolvoFrame extends JFrame implements WindowListener, ActionListener {

    private static Logger LOGGER = Logger.getLogger(VolvoFrame.class);
    private JPanel panel;
    private JLabel xlsLabel;
    private JLabel pdfLabel;
    private JLabel emptyLabel;
    private JButton pdfFolderOpenButton;
    private JButton xlsOpenButton;
    private JButton processButton;
    private List list;
    private JFileChooser dirChooser;
    private FileNameExtensionFilter filter;

    private ArrayList<Volvo> volvos;
    private VolvoExcelProcess volvoExcelProcess;

    public VolvoFrame() throws HeadlessException {

        filter = new FileNameExtensionFilter(
                "XLS/XLSX", "xls", "xlsx");

        setLayout(new BorderLayout());
        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        xlsLabel = new JLabel("");
        pdfLabel = new JLabel("");
        emptyLabel = new JLabel("");
        pdfFolderOpenButton = new JButton("Открыть папку с PDF");
        xlsOpenButton = new JButton("Открыть Excel файл");
        processButton = new JButton("Обработать");

        list = new List(20);
        dirChooser = new JFileChooser();

        pdfFolderOpenButton.addActionListener(this);
        xlsOpenButton.addActionListener(this);
        processButton.addActionListener(this);
        processButton.setEnabled(false);
        list.setVisible(true);

        panel.add(xlsLabel);
        panel.add(xlsOpenButton);
        panel.add(pdfLabel);
        panel.add(pdfFolderOpenButton);
        panel.add(emptyLabel);
        panel.add(processButton);
        add(list);

        add(panel, BorderLayout.NORTH);
        setTitle("Volvo app");
        addWindowListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(xlsOpenButton)) {
            HashMap<String,String> checks = new HashMap<>();
            checks.put("A2","TRAIN");

            dirChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            dirChooser.setFileFilter(filter);
            if (dirChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String file = dirChooser.getSelectedFile().getPath();
                String ext = FilenameUtils.getExtension(file);
                if (ext.toLowerCase().equals("xls") || ext.toLowerCase().equals("xlsx")) {
                    volvoExcelProcess = new VolvoExcelProcess();
                    try {
                        if(volvoExcelProcess.init(new FileInputStream(new File(file)), checks))
                        {
                            volvos= (ArrayList<Volvo>) volvoExcelProcess.processSheet();
                            if(volvos!=null)
                            {
                                JOptionPane.showMessageDialog(null, "Найдено:"+volvos.size()+" записей", "Успешно", JOptionPane.INFORMATION_MESSAGE);
                                xlsLabel.setText(FilenameUtils.getBaseName(file));
                                if(!xlsLabel.getText().isEmpty()&& !pdfLabel.getText().isEmpty())
                                    processButton.setEnabled(true);
                                return;
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(null, "Не найдено записей", "Ошибка", JOptionPane.INFORMATION_MESSAGE);
                                xlsLabel.setText("");
                            }
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, volvoExcelProcess.getErrors(), "Не верный EXCEL файл", JOptionPane.INFORMATION_MESSAGE);
                            xlsLabel.setText("");
                        }
                        processButton.setEnabled(false);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                        xlsLabel.setText("");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Выбран не Excel файл", "Информация", JOptionPane.INFORMATION_MESSAGE);
                    xlsLabel.setText("");
                    processButton.setEnabled(false);
                }
            }
        }
        if (e.getSource().equals(pdfFolderOpenButton)) {
            dirChooser.removeChoosableFileFilter(filter);
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if(dirChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                list.removeAll();
                File choosenDir = new File(dirChooser.getSelectedFile().getPath());
                String[] Flist = choosenDir.list();
                for (String S : Flist) {
                    if(FilenameUtils.getExtension(S).toLowerCase().equals("pdf"))
                    {
                        list.add(S);
                    }
                }
                JOptionPane.showMessageDialog(null, "Найдено:"+list.getItemCount()+" PDF файлов", "Успешно", JOptionPane.INFORMATION_MESSAGE);
                pdfLabel.setText(dirChooser.getSelectedFile().getPath());
                if(!xlsLabel.getText().isEmpty()&& !pdfLabel.getText().isEmpty())
                    processButton.setEnabled(true);
                return;
            }
        }
        if (e.getSource().equals(processButton)) {

            String outputPath=pdfLabel.getText()+File.separator+"out";
            try {
                FileUtils.forceMkdir(new File(outputPath));
            } catch (IOException e1) {
                e1.printStackTrace();
                return;
            }
            String[] fnames= list.getItems().clone();
            for (String fname:fnames) {
                for (Volvo volvo:volvos) {
                    if((volvo.getVin()+".pdf").toLowerCase().equals(fname.toLowerCase()))
                    {
                        try {
                            volvoExcelProcess.copyAndRename(fname,pdfLabel.getText(),volvo.getTrain());
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        list.remove(fname);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
